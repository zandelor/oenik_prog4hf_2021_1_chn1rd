﻿#nullable disable

namespace BiblioDatCore.Data.Models
{
    using System;
    using System.Collections.Generic;

    /// <summary>
    /// Entity class for type for author elements.
    /// </summary>
    public partial class Author
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Author"/> class.
        /// </summary>
        public Author()
        {
            this.Books = new HashSet<Book>();
        }

        /// <summary>
        /// Gets or sets Id of Author objects.
        /// </summary>
        public decimal Authorid { get; set; }

        /// <summary>
        /// Gets or sets first name of Author objects.
        /// </summary>
        public string Firstname { get; set; }

        /// <summary>
        /// Gets or sets last name of Author objects.
        /// </summary>
        public string Lastname { get; set; }

        /// <summary>
        /// Gets or sets gender of Author objects.
        /// </summary>
        public string Gender { get; set; }

        /// <summary>
        /// Gets or sets date of birth of Author objects.
        /// </summary>
        public DateTime? Dateofbirth { get; set; }

        /// <summary>
        /// Gets or sets date of death of Author objects.
        /// </summary>
        public DateTime? Dateofdeath { get; set; }

        /// <summary>
        /// Gets or sets Id of Author objects.
        /// </summary>
        public virtual ICollection<Book> Books { get; set; }
    }
}
