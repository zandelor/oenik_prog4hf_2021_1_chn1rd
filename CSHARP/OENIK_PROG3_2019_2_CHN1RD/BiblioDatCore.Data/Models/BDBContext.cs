﻿#nullable disable

using System;

[assembly: CLSCompliant(false)]

namespace BiblioDatCore.Data.Models
{
    using System;
    using Microsoft.EntityFrameworkCore;
    using Microsoft.EntityFrameworkCore.Metadata;

    /// <summary>
    /// Class for database context.
    /// </summary>
    public partial class BDBContext : DbContext
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="BDBContext"/> class.
        /// </summary>
        public BDBContext()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="BDBContext"/> class.
        /// </summary>
        /// <param name="options">Parameter for options.</param>
        public BDBContext(DbContextOptions<BDBContext> options)
            : base(options)
        {
        }

        /// <summary>
        /// Gets or sets Authors DbSet.
        /// </summary>
        public virtual DbSet<Author> Authors { get; set; }

        /// <summary>
        /// Gets or sets Books DbSet.
        /// </summary>
        public virtual DbSet<Book> Books { get; set; }

        /// <summary>
        /// Gets or sets Checkouts DbSet.
        /// </summary>
        public virtual DbSet<Checkout> Checkouts { get; set; }

        /// <summary>
        /// Gets or sets Students DbSet.
        /// </summary>
        public virtual DbSet<Student> Students { get; set; }

        /// <summary>
        /// Configures the database (and other options) to be used for this context (from MS docs).
        /// </summary>
        /// <param name="optionsBuilder">A builder used to create or modify options for this context (from MS docs).</param>
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                optionsBuilder.UseSqlServer("Data Source=(LocalDB)\\MSSQLLocalDB;AttachDbFilename=|DataDirectory|\\BDBC.mdf;Integrated Security=True");
            }
        }

        /// <summary>
        /// This method is called when the model for a derived context has been initialized, but before the model has been locked down and used to initialize the context (from MS docs).
        /// </summary>
        /// <param name="modelBuilder">The builder that defines the model for the context being created (from MS docs).</param>
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasAnnotation("Relational:Collation", "SQL_Latin1_General_CP1_CI_AS");

            modelBuilder.Entity<Author>(entity =>
            {
                entity.ToTable("AUTHOR");

                entity.Property(e => e.Authorid)
                    .HasColumnType("numeric(4, 0)")
                    .HasColumnName("AUTHORID");

                entity.Property(e => e.Dateofbirth)
                    .HasColumnType("datetime")
                    .HasColumnName("DATEOFBIRTH");

                entity.Property(e => e.Dateofdeath)
                    .HasColumnType("datetime")
                    .HasColumnName("DATEOFDEATH");

                entity.Property(e => e.Firstname)
                    .IsRequired()
                    .HasMaxLength(10)
                    .IsUnicode(false)
                    .HasColumnName("FIRSTNAME");

                entity.Property(e => e.Gender)
                    .HasMaxLength(10)
                    .IsUnicode(false)
                    .HasColumnName("GENDER");

                entity.Property(e => e.Lastname)
                    .HasMaxLength(10)
                    .IsUnicode(false)
                    .HasColumnName("LASTNAME");
            });

            modelBuilder.Entity<Book>(entity =>
            {
                entity.ToTable("BOOK");

                entity.Property(e => e.Bookid)
                    .HasColumnType("numeric(6, 0)")
                    .HasColumnName("BOOKID");

                entity.Property(e => e.Authorid)
                    .HasColumnType("numeric(4, 0)")
                    .HasColumnName("AUTHORID");

                entity.Property(e => e.Edition)
                    .HasColumnType("numeric(2, 0)")
                    .HasColumnName("EDITION");

                entity.Property(e => e.Genre)
                    .IsRequired()
                    .HasMaxLength(10)
                    .IsUnicode(false)
                    .HasColumnName("GENRE");

                entity.Property(e => e.Pages)
                    .HasColumnType("numeric(5, 0)")
                    .HasColumnName("PAGES");

                entity.Property(e => e.Releasedate)
                    .HasColumnType("datetime")
                    .HasColumnName("RELEASEDATE");

                entity.Property(e => e.Title)
                    .IsRequired()
                    .HasMaxLength(30)
                    .IsUnicode(false)
                    .HasColumnName("TITLE");

                entity.HasOne(d => d.Author)
                    .WithMany(p => p.Books)
                    .HasForeignKey(d => d.Authorid)
                    .HasConstraintName("BOOK_FOREIGN_KEY");
            });

            modelBuilder.Entity<Checkout>(entity =>
            {
                entity.ToTable("CHECKOUT");

                entity.Property(e => e.Checkoutid)
                    .HasColumnType("numeric(4, 0)")
                    .HasColumnName("CHECKOUTID");

                entity.Property(e => e.Bookid)
                    .HasColumnType("numeric(6, 0)")
                    .HasColumnName("BOOKID");

                entity.Property(e => e.Borrowdate)
                    .HasColumnType("datetime")
                    .HasColumnName("BORROWDATE");

                entity.Property(e => e.Returndate)
                    .HasColumnType("datetime")
                    .HasColumnName("RETURNDATE");

                entity.Property(e => e.Studentid)
                    .HasColumnType("numeric(5, 0)")
                    .HasColumnName("STUDENTID");

                entity.HasOne(d => d.Book)
                    .WithMany(p => p.Checkouts)
                    .HasForeignKey(d => d.Bookid)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("CHECKOUT_FOREIGN_KEY_BOOK");

                entity.HasOne(d => d.Student)
                    .WithMany(p => p.Checkouts)
                    .HasForeignKey(d => d.Studentid)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("CHECKOUT_FOREIGN_KEY_STUDENT");
            });

            modelBuilder.Entity<Student>(entity =>
            {
                entity.ToTable("STUDENT");

                entity.Property(e => e.Studentid)
                    .HasColumnType("numeric(5, 0)")
                    .HasColumnName("STUDENTID");

                entity.Property(e => e.Classyear)
                    .HasColumnType("numeric(2, 0)")
                    .HasColumnName("CLASSYEAR");

                entity.Property(e => e.Dateofbirth)
                    .HasColumnType("datetime")
                    .HasColumnName("DATEOFBIRTH");

                entity.Property(e => e.Firstname)
                    .IsRequired()
                    .HasMaxLength(10)
                    .IsUnicode(false)
                    .HasColumnName("FIRSTNAME");

                entity.Property(e => e.Gender)
                    .HasMaxLength(10)
                    .IsUnicode(false)
                    .HasColumnName("GENDER");

                entity.Property(e => e.Lastname)
                    .IsRequired()
                    .HasMaxLength(10)
                    .IsUnicode(false)
                    .HasColumnName("LASTNAME");
            });

            this.OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
