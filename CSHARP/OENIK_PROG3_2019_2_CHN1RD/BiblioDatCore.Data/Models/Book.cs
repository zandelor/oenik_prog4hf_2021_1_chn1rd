﻿#nullable disable

namespace BiblioDatCore.Data.Models
{
    using System;
    using System.Collections.Generic;

    /// <summary>
    /// Class for Book entries.
    /// </summary>
    public partial class Book
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Book"/> class.
        /// </summary>
        public Book()
        {
            this.Checkouts = new HashSet<Checkout>();
        }

        /// <summary>
        /// Gets or sets ID of book.
        /// </summary>
        public decimal Bookid { get; set; }

        /// <summary>
        /// Gets or sets author's id of book.
        /// </summary>
        public decimal? Authorid { get; set; }

        /// <summary>
        /// Gets or sets title of book.
        /// </summary>
        public string Title { get; set; }

        /// <summary>
        /// Gets or sets genre of book.
        /// </summary>
        public string Genre { get; set; }

        /// <summary>
        /// Gets or sets release date of book.
        /// </summary>
        public DateTime? Releasedate { get; set; }

        /// <summary>
        /// Gets or sets number of pages of book.
        /// </summary>
        public decimal Pages { get; set; }

        /// <summary>
        /// Gets or sets edition of book.
        /// </summary>
        public decimal? Edition { get; set; }

        /// <summary>
        /// Gets or sets author of book.
        /// </summary>
        public virtual Author Author { get; set; }

        /// <summary>
        /// Gets or sets checkouts of book.
        /// </summary>
        public virtual ICollection<Checkout> Checkouts { get; set; }
    }
}
