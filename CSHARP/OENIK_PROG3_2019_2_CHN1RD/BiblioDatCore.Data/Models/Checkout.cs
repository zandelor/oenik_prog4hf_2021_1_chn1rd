﻿#nullable disable

namespace BiblioDatCore.Data.Models
{
    using System;
    using System.Collections.Generic;

    /// <summary>
    /// Class for library checkouts.
    /// </summary>
    public partial class Checkout
    {
        /// <summary>
        /// Gets or sets the id of checkout.
        /// </summary>
        public decimal Checkoutid { get; set; }

        /// <summary>
        /// Gets or sets the student id of checkout.
        /// </summary>
        public decimal Studentid { get; set; }

        /// <summary>
        /// Gets or sets the book id of checkout.
        /// </summary>
        public decimal Bookid { get; set; }

        /// <summary>
        /// Gets or sets the borrow date of checkout.
        /// </summary>
        public DateTime Borrowdate { get; set; }

        /// <summary>
        /// Gets or sets the return date of checkout.
        /// </summary>
        public DateTime? Returndate { get; set; }

        /// <summary>
        /// Gets or sets the book of checkout.
        /// </summary>
        public virtual Book Book { get; set; }

        /// <summary>
        /// Gets or sets the student of checkout.
        /// </summary>
        public virtual Student Student { get; set; }
    }
}
