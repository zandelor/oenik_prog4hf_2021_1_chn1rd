﻿#nullable disable

namespace BiblioDatCore.Data.Models
{
    using System;
    using System.Collections.Generic;

    /// <summary>
    /// Class for representing student entries.
    /// </summary>
    public partial class Student
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Student"/> class.
        /// </summary>
        public Student()
        {
            this.Checkouts = new HashSet<Checkout>();
        }

        /// <summary>
        /// Gets or sets Id of student.
        /// </summary>
        public decimal Studentid { get; set; }

        /// <summary>
        /// Gets or sets first name of student.
        /// </summary>
        public string Firstname { get; set; }

        /// <summary>
        /// Gets or sets last name of student.
        /// </summary>
        public string Lastname { get; set; }

        /// <summary>
        /// Gets or sets gender of student.
        /// </summary>
        public string Gender { get; set; }

        /// <summary>
        /// Gets or sets class year of student.
        /// </summary>
        public decimal Classyear { get; set; }

        /// <summary>
        /// Gets or sets date of birth of student.
        /// </summary>
        public DateTime Dateofbirth { get; set; }

        /// <summary>
        /// Gets or sets checkouts by student.
        /// </summary>
        public virtual ICollection<Checkout> Checkouts { get; set; }
    }
}
