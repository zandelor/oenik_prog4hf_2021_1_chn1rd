﻿// This file is used by Code Analysis to maintain SuppressMessage
// attributes that are applied to this project.
// Project-level suppressions either have no target or are given
// a specific target and scoped to a namespace, type, member, etc.

using System.Diagnostics.CodeAnalysis;

[assembly: SuppressMessage("Globalization", "CA1305:Specify IFormatProvider", Justification = "<Program will not be accessed from different location.>", Scope = "member", Target = "~M:BiblioDatCore.Logic.Tests.LogicTest.Init")]
[assembly: SuppressMessage("Globalization", "CA1305:Specify IFormatProvider", Justification = "<Program will not be accessed from different location.>", Scope = "member", Target = "~M:BiblioDatCore.Logic.Tests.LogicTest.TestInsertStudent")]
[assembly: SuppressMessage("Globalization", "CA1305:Specify IFormatProvider", Justification = "<Program will not be accessed from different location.>", Scope = "member", Target = "~M:BiblioDatCore.Logic.Tests.LogicTest.TestChangeBookThrowsIdAlreadyInCollectionException")]
[assembly: SuppressMessage("Globalization", "CA1305:Specify IFormatProvider", Justification = "<Program will not be accessed from different location.>", Scope = "member", Target = "~M:BiblioDatCore.Logic.Tests.LogicTest.TestChangeCheckout")]
[assembly: SuppressMessage("Globalization", "CA1305:Specify IFormatProvider", Justification = "<Program will not be accessed from different location.>", Scope = "member", Target = "~M:BiblioDatCore.Logic.Tests.LogicTest.TestChangeCheckoutThrowsIdNotInCollectionException")]
[assembly: SuppressMessage("Globalization", "CA1305:Specify IFormatProvider", Justification = "<Program will not be accessed from different location.>", Scope = "member", Target = "~M:BiblioDatCore.Logic.Tests.LogicTest.TestInsertAuthorThrowsIdAlreadyInCollectionException")]
[assembly: SuppressMessage("StyleCop.CSharp.DocumentationRules", "SA1633:File should have header", Justification = "<No header is used throughout solution.>")]
[assembly: SuppressMessage("StyleCop.CSharp.DocumentationRules", "SA1633:File should have header", Justification = "<No header is used throughout solution.>", Scope = "namespace", Target = "~N:BiblioDatCore.Logic.Tests")]
