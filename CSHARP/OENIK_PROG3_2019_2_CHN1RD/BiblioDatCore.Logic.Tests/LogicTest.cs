﻿using System;

[assembly: CLSCompliant(false)]

namespace BiblioDatCore.Logic.Tests
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using BiblioDatCore.Data.Models;
    using BiblioDatCore.Repository;
    using Moq;
    using NUnit.Framework;

    /// <summary>
    /// Class created for Moq tests.
    /// </summary>
    [TestFixture]
    public class LogicTest
    {
        private Mock<IRepository> mockRepository;
        private LogicClass logic;

        /// <summary>
        /// Initializes mock repository database for testing.
        /// </summary>
        [SetUp]
        public void Init()
            {
            this.mockRepository = new Mock<IRepository>();

            this.mockRepository.Setup(x => x.ReadAllAuthor()).Returns(new List<Author>()
            {
                new Author() { Authorid = 1234, Firstname = "JOHN", Lastname = "SMITH", Gender = "MALE", Dateofbirth = DateTime.Parse("30-SEP-1972"), Dateofdeath = null },
                new Author() { Authorid = 2572, Firstname = "ARLENE", Lastname = "KOWALSKI", Gender = "FEMALE", Dateofbirth = DateTime.Parse("04-MAY-1974"), Dateofdeath = null },
                new Author() { Authorid = 4112, Firstname = "ANDREASSA", Lastname = "DELACRUZ", Gender = "FEMALE", Dateofbirth = DateTime.Parse("17-JUL-1929"), Dateofdeath = DateTime.Parse("11-JAN-2015") },
                new Author() { Authorid = 1111, Firstname = "RAJHIT", Lastname = "ADHMAR", Gender = "MALE", Dateofbirth = DateTime.Parse("23-JUL-1992"), Dateofdeath = null },
            });

            this.mockRepository.Setup(x => x.ReadAllBook()).Returns(new List<Book>()
            {
                new Book() { Bookid = 491257, Authorid = 1234, Title = "On Top of the Whale", Genre = "COMEDY", Releasedate = DateTime.Parse("12-APR-1992"), Pages = 513, Edition = 1 },
                new Book() { Bookid = 481114, Authorid = 1234, Title = "Lathe of Heaven", Genre = "SCIENCE", Releasedate = DateTime.Parse("17-AUG-2005"), Pages = 2010, Edition = 1 },
                new Book() { Bookid = 644558, Authorid = 1111, Title = "Raajneeti", Genre = "CRIME", Releasedate = DateTime.Parse("28-FEB-2018"), Pages = 8121, Edition = 1 },
                new Book() { Bookid = 481945, Authorid = 2572, Title = "Keeping the Faith", Genre = "ROMANTIC", Releasedate = DateTime.Parse("08-MAY-1996"), Pages = 1212, Edition = 3 },
            });

            this.mockRepository.Setup(x => x.ReadAllStudent()).Returns(new List<Student>()
            {
                new Student() { Studentid = 29503, Firstname = "ASHTON", Lastname = "FABBRI", Gender = "MALE", Classyear = 5, Dateofbirth = DateTime.Parse("19-OCT-1941") },
                new Student() { Studentid = 28872, Firstname = "MARIA", Lastname = "MCKEADY", Gender = "FEMALE", Classyear = 13, Dateofbirth = DateTime.Parse("20-MAY-1962") },
                new Student() { Studentid = 60455, Firstname = "TWILA", Lastname = "EDMANS", Gender = "FEMALE", Classyear = 1, Dateofbirth = DateTime.Parse("11-JUL-1974") },
            });

            this.mockRepository.Setup(x => x.ReadAllCheckout()).Returns(new List<Checkout>()
            {
                new Checkout() { Checkoutid = 8859, Studentid = 29503, Bookid = 491257, Borrowdate = DateTime.Parse("09-FEB-2010"), Returndate = DateTime.Parse("10-FEB-2010") },
                new Checkout() { Checkoutid = 9440, Studentid = 28872, Bookid = 481114, Borrowdate = DateTime.Parse("06-OCT-2011"), Returndate = DateTime.Parse("07-OCT-2011") },
                new Checkout() { Checkoutid = 5438, Studentid = 28872, Bookid = 644558, Borrowdate = DateTime.Parse("13-NOV-2010"), Returndate = DateTime.Parse("19-NOV-2010") },
                new Checkout() { Checkoutid = 9315, Studentid = 28872, Bookid = 644558, Borrowdate = DateTime.Parse("09-DEC-2015"), Returndate = DateTime.Parse("23-DEC-2015") },
            });

            this.logic = new LogicClass(this.mockRepository.Object);
        }

        /// <summary>
        /// Tests the InsertStudent method with correct input.
        /// Verifies that the correct repository method is called with the correct parameters.
        /// </summary>
        [Test]
        public void TestInsertStudent()
        {
            Student student = new Student()
            {
                Studentid = 12345,
                Firstname = "EDGAER",
                Lastname = "MITTENS",
                Gender = "MALE",
                Classyear = 2,
                Dateofbirth = DateTime.Parse("06-JUL-1966"),
            };
            this.logic.InsertStudent(student);

            this.mockRepository.Verify(x => x.CreateStudent(It.IsAny<Student>()), Times.Once());
        }

        /// <summary>
        /// Tests the InsertAuthor method with incorrect input: ID of item is already in the database.
        /// Verifies that the correct exception is thrown.
        /// </summary>
        [Test]
        public void TestInsertAuthorThrowsIdAlreadyInCollectionException()
        {
            Author author = new Author()
            {
                Authorid = 1234,
                Firstname = "JOHN",
                Lastname = "SMITH",
                Gender = "MALE",
                Dateofbirth = DateTime.Parse("30-SEP-1972"),
                Dateofdeath = null,
            };
            Assert.Throws<IdAlreadyInCollectionException>(() => this.logic.InsertAuthor(author));
        }

        /// <summary>
        /// Tests the ListAllBook method with correct input.
        /// Verifies that the correct repository method is called.
        /// </summary>
        [Test]
        public void TestListAllBook()
        {
            IList<Book> list = this.logic.ListAllBook();
            this.mockRepository.Verify(x => x.ReadAllBook(), Times.Once());
        }

        /// <summary>
        /// Tests the GetOneAuthor method with correct input.
        /// Verifies that the correct repository method is called with the correct parameters.
        /// </summary>
        [Test]
        public void TestGetOneAuthor()
        {
            Author author = this.logic.GetOneAuthor(1111);
            this.mockRepository.Verify(x => x.GetOneAuthor(It.IsAny<int>()), Times.Once());
        }

        /// <summary>
        /// Tests the GetOneBook method with incorrect input: ID of item is not in the database.
        /// Verifies that the correct exception is thrown.
        /// </summary>
        [Test]
        public void TestGetOneBookWithException()
        {
            Assert.Throws<IdNotInCollectionException>(() => this.logic.GetOneBook(0));
        }

        /// <summary>
        /// Tests the ChangeChekout method with correct input.
        /// Verifies that the correct repository method is called with the correct parameters.
        /// </summary>
        [Test]
        public void TestChangeCheckout()
        {
            Checkout checkout = new Checkout()
            {
                Checkoutid = 8859,
                Studentid = 29503,
                Bookid = 491257,
                Borrowdate = DateTime.Parse("09-FEB-2010"),
                Returndate = DateTime.Parse("10-FEB-2010"),
            };
            this.logic.ChangeCheckout(8859, checkout);
            this.mockRepository.Verify(x => x.UpdateCheckout(It.IsAny<int>(), It.IsAny<Checkout>()), Times.Once());
        }

        /// <summary>
        /// Tests the GetOneAuthor method with incorrect input: Author item to be inserted is null.
        /// Verifies that the correct exception is thrown.
        /// </summary>
        [Test]
        public void TestChangeAuthorWithNull()
        {
            Assert.Throws<NullReferenceException>(() => this.logic.ChangeAuthor(0, null));
        }

        /// <summary>
        /// Tests the ChangeCheckout method with incorrect input: ID of item to be changed is not in the database.
        /// Verifies that the correct exception is thrown.
        /// </summary>
        [Test]
        public void TestChangeCheckoutThrowsIdNotInCollectionException()
        {
            Checkout checkout = new Checkout()
            {
                Checkoutid = 8859,
                Studentid = 29503,
                Bookid = 491257,
                Borrowdate = DateTime.Parse("09-FEB-2010"),
                Returndate = DateTime.Parse("10-FEB-2010"),
            };
            Assert.Throws<IdNotInCollectionException>(() => this.logic.ChangeCheckout(0, checkout));
        }

        /// <summary>
        /// Tests the ChangeBook method with incorrect input: new ID of item is already in the database.
        /// Verifies that the correct exception is thrown.
        /// </summary>
        [Test]
        public void TestChangeBookThrowsIdAlreadyInCollectionException()
        {
            Book book = new Book()
            {
                Bookid = 491257,
                Authorid = 1234,
                Title = "On Top of the Whale",
                Genre = "COMEDY",
                Releasedate = DateTime.Parse("12-APR-1992"),
                Pages = 513,
                Edition = 1,
            };
            Assert.Throws<IdAlreadyInCollectionException>(() => this.logic.ChangeBook(481945, book));
        }

        /// <summary>
        /// Tests the RemoveCheckout method with correct input.
        /// Verifies that the correct repository method is called with the correct parameters.
        /// </summary>
        [Test]
        public void TestRemoveCheckout()
        {
            this.logic.RemoveCheckout(8859);
            this.mockRepository.Verify(x => x.DeleteCheckout(It.IsAny<int>()), Times.Once);
        }

        /// <summary>
        /// Tests the RemoveAuthor method with correct input.
        /// Verifies that the correct repository method is called with the correct parameters.
        /// </summary>
        [Test]
        public void TestRemoveAuthor()
        {
            this.logic.RemoveAuthor(4112);
            this.mockRepository.Verify(x => x.DeleteAuthor(It.IsAny<int>()), Times.Once);
        }

        /// <summary>
        /// Tests the BooksNeverBorrowed method.
        /// Verifies that the correct items are returned.
        /// </summary>
        [Test]
        public void TestBooksNeverBorrowed()
        {
            IList<Book> list = this.logic.BooksNeverBorrowed();
            Assert.That(list[0].Bookid == 481945);
            this.mockRepository.Verify(x => x.ReadAllBook(), Times.Once());
            Assert.That(list, Has.Exactly(1).Items);
        }

        /// <summary>
        /// Tests the BorrowsByGenres method.
        /// Verifies that the correct number of items are returned.
        /// Verifies that the method correctly logs Genres even when there has been no Checkouts of said Genre in the database.
        /// Verifies that the correct repository methods were called with the correct parameters.
        /// </summary>
        [Test]
        public void TestBorrowsByGenres()
        {
            IList<GenresAndNumberOfBorrows> list = this.logic.BorrowsByGenres();
            Assert.That(list, Has.Exactly(4).Items);
            Assert.That(list[0].Genre.Equals("COMEDY", StringComparison.Ordinal));
            Assert.That(list[3].NumberOfBorrows == 0);
            this.mockRepository.Verify(x => x.ReadAllCheckout(), Times.Once());
            this.mockRepository.Verify(x => x.ReadAllBook(), Times.Exactly(2));
        }

        /// <summary>
        /// Tests the Test_AvgDayOfBorrowsByAuthors method.
        /// Verifies that the correct number of items are returned.
        /// Verifies that the method correctly logs Authors even when there has been no Checkouts of Books of that Author.
        /// Verifies that the correct repository methods were called with the correct parameters.
        /// </summary>
        [Test]
        public void TestAvgDayOfBorrowsByAuthors()
        {
            IList<AverageDaysOfBorrows> list = this.logic.AvgDayOfBorrowsByAuthors();
            Assert.That(list, Has.Exactly(4).Items);
            Assert.That(list[0].AVERAGE == 1);
            Assert.That(list[3].AVERAGE == 0);
            this.mockRepository.Verify(x => x.ReadAllCheckout(), Times.Once());
            this.mockRepository.Verify(x => x.ReadAllBook(), Times.Once());
            this.mockRepository.Verify(x => x.ReadAllAuthor(), Times.Exactly(2));
        }
    }
}
