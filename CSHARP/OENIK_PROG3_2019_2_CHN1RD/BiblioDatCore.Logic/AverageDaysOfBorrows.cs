﻿namespace BiblioDatCore.Logic
{
    /// <summary>
    /// Supplementary class for storing authors and a number of how many days on average do people borrow the books of said authors.
    /// </summary>
    public class AverageDaysOfBorrows
    {
        /// <summary>
        /// Gets or sets the name of the author.
        /// </summary>
        public string AUTHORNAME { get; set; }

        /// <summary>
        /// Gets or sets the value of how many days on average do people borrow the books of the author.
        /// </summary>
        public double AVERAGE { get; set; }
    }
}
