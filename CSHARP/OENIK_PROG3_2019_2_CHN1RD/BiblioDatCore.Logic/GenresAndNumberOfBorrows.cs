﻿namespace BiblioDatCore.Logic
{
    /// <summary>
    /// Supplementary class for storing book genres and the number of checkouts there have been of said genres.
    /// </summary>
    public class GenresAndNumberOfBorrows
    {
        /// <summary>
        /// Gets or sets book genre.
        /// </summary>
        public string Genre { get; set; }

        /// <summary>
        /// Gets or sets the number of checkouts there have been of the genre.
        /// </summary>
        public int NumberOfBorrows { get; set; }
    }
}
