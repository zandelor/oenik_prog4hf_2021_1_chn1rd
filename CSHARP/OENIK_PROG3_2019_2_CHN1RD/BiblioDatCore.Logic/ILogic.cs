﻿namespace BiblioDatCore.Logic
{
    using System.Collections.Generic;
    using BiblioDatCore.Data.Models;

    /// <summary>
    /// Interface for Logic level methods.
    /// </summary>
    public interface ILogic
    {
        /// <summary>
        /// Method for inserting new author item into database.
        /// </summary>
        /// <param name="author">author item to be inserted into database.</param>
        void InsertAuthor(Author author);

        /// <summary>
        /// Method for inserting new book item into database.
        /// </summary>
        /// <param name="book">book item to be inserted into database.</param>
        void InsertBook(Book book);

        /// <summary>
        /// Method for inserting new student item into database.
        /// </summary>
        /// <param name="student">student item to be inserted into database.</param>
        void InsertStudent(Student student);

        /// <summary>
        /// Method for inserting new checkout item into database.
        /// </summary>
        /// <param name="checkout">checkout item to be inserted into database.</param>
        void InsertCheckout(Checkout checkout);

        /// <summary>
        /// Method for reading all author items from database.
        /// </summary>
        /// <returns>a list of all authors in database.</returns>
        IList<Author> ListAllAuthor();

        /// <summary>
        /// Method for reading all book items from database.
        /// </summary>
        /// <returns>a list of all books in database.</returns>
        IList<Book> ListAllBook();

        /// <summary>
        /// Method for reading all student items from database.
        /// </summary>
        /// <returns>a list of all students in database.</returns>
        IList<Student> ListAllStudent();

        /// <summary>
        /// Method for reading all checkout items from database.
        /// </summary>
        /// <returns>a list of all checkouts in database.</returns>
        IList<Checkout> ListAllCheckout();

        /// <summary>
        /// Method for reading a single author item from database.
        /// </summary>
        /// <param name="id">id of item to be read from database.</param>
        /// <returns>a single specified author from database.</returns>
        Author GetOneAuthor(int id);

        /// <summary>
        /// Method for reading a single book item from database.
        /// </summary>
        /// <param name="id">id of item to be read from database.</param>
        /// <returns>a single specified book from database.</returns>
        Book GetOneBook(int id);

        /// <summary>
        /// Method for reading a single student item from database.
        /// </summary>
        /// <param name="id">id of item to be read from database.</param>
        /// <returns>a single specified student from database.</returns>
        Student GetOneStudent(int id);

        /// <summary>
        /// Method for reading a single checkout item from database.
        /// </summary>
        /// <param name="id">id of item to be read from database.</param>
        /// <returns>a single specified checkout from database.</returns>
        Checkout GetOneCheckout(int id);

        /// <summary>
        /// Method for replacing a single specified author item in the database with a new author item.
        /// </summary>
        /// <param name="id">id of item to be updated in the database.</param>
        /// <param name="newAuthor">id of new item to be inserted into the database.</param>
        void ChangeAuthor(int id, Author newAuthor);

        /// <summary>
        /// Method for replacing a single specified book item in the database with a new author item.
        /// </summary>
        /// <param name="id">id of item to be updated in the database.</param>
        /// <param name="newBook">id of new item to be inserted into the database.</param>
        void ChangeBook(int id, Book newBook);

        /// <summary>
        /// Method for replacing a single specified student item in the database with a new author item.
        /// </summary>
        /// <param name="id">id of item to be updated in the database.</param>
        /// <param name="newStudent">id of new item to be inserted into the database.</param>
        void ChangeStudent(int id, Student newStudent);

        /// <summary>
        /// Method for replacing a single specified checkout item in the database with a new author item.
        /// </summary>
        /// <param name="id">id of item to be updated in the database.</param>
        /// <param name="newCheckout">id of new item to be inserted into the database.</param>
        void ChangeCheckout(int id, Checkout newCheckout);

        /// <summary>
        /// Method for removing a single specified author item from the database.
        /// </summary>
        /// <param name="id">id of item to be removed from the database.</param>
        void RemoveAuthor(int id);

        /// <summary>
        /// Method for removing a single specified book item from the database.
        /// </summary>
        /// <param name="id">id of item to be removed from the database.</param>
        void RemoveBook(int id);

        /// <summary>
        /// Method for removing a single specified student item from the database.
        /// </summary>
        /// <param name="id">id of item to be removed from the database.</param>
        void RemoveStudent(int id);

        /// <summary>
        /// Method for removing a single specified checkout item from the database.
        /// </summary>
        /// <param name="id">id of item to be removed from the database.</param>
        void RemoveCheckout(int id);

        /// <summary>
        /// Method for creating a list of stored books that have never been borrowed.
        /// </summary>
        /// <returns>a list of books that have never been borrowed.</returns>
        IList<Book> BooksNeverBorrowed();

        /// <summary>
        /// Method for creating a list of book genres and the number of checkouts there have been of said genres.
        /// </summary>
        /// <returns>a list of genres and the number of checkouts there have been of said genres.</returns>
        IList<GenresAndNumberOfBorrows> BorrowsByGenres();

        /// <summary>
        /// Method for creating a list of authors and a number of how many days on average do people borrow the books of said authors.
        /// </summary>
        /// <returns>a list of authors and a number of how many days on average do people borrow the books of said authors.</returns>
        IList<AverageDaysOfBorrows> AvgDayOfBorrowsByAuthors();

        /// <summary>
        /// Method for generating an advertizing voucher for a certain book item.
        /// </summary>
        /// <returns>a voucher item containing the title of book to be advertized, the discount value, and a list of people to send the advertisment to.</returns>
        /// <param name="id">id of book to be generated a voucher for.</param>
        VoucherClass Voucher(int id);
    }
}
