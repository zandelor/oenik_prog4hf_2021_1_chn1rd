﻿namespace BiblioDatCore.Logic
{
    using System;

    /// <summary>
    /// Exception to be used whenever we are trying to insert an item into the database with an ID that is already in the database.
    /// </summary>
    public class IdAlreadyInCollectionException : Exception
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="IdAlreadyInCollectionException"/> class.
        /// </summary>
        public IdAlreadyInCollectionException()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="IdAlreadyInCollectionException"/> class.
        /// </summary>
        /// <param name="msg">Error message of exception.</param>
        public IdAlreadyInCollectionException(string msg)
            : base(msg)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="IdAlreadyInCollectionException"/> class.
        /// </summary>
        /// <param name="message">Error message of exception.</param>
        /// <param name="innerException">Exception that may have cause the current exception.</param>
        public IdAlreadyInCollectionException(string message, Exception innerException)
            : base(message, innerException)
        {
        }
    }
}
