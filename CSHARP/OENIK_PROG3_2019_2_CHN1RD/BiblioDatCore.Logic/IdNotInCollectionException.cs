﻿namespace BiblioDatCore.Logic
{
    using System;

    /// <summary>
    /// Exception to be used whenever we are trying to read an item that is not in the database.
    /// </summary>
    public class IdNotInCollectionException : Exception
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="IdNotInCollectionException"/> class.
        /// </summary>
        /// <param name="msg">Error message of exception.</param>
        public IdNotInCollectionException(string msg)
            : base(msg)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="IdNotInCollectionException"/> class.
        /// </summary>
        public IdNotInCollectionException()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="IdNotInCollectionException"/> class.
        /// </summary>
        /// <param name="message">Error message of exception.</param>
        /// <param name="innerException">Exception that may have caused this exception.</param>
        public IdNotInCollectionException(string message, Exception innerException)
            : base(message, innerException)
        {
        }
    }
}
