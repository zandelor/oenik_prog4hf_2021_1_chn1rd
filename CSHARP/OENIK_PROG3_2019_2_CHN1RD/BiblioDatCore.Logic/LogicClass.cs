﻿using System;

[assembly: CLSCompliant(false)]

namespace BiblioDatCore.Logic
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.Linq;
    using System.Xml.Linq;
    using BiblioDatCore.Data.Models;
    using BiblioDatCore.Repository;
    using GalaSoft.MvvmLight.Ioc;

    /// <summary>
    /// Class for the implementation of the database repository.
    /// </summary>
    public class LogicClass : ILogic
    {
        private readonly IRepository repo;

        /// <summary>
        /// Initializes a new instance of the <see cref="LogicClass"/> class.
        /// </summary>
        [PreferredConstructor]
        public LogicClass()
        {
            this.repo = new RepoClass(new BDBContext());
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="LogicClass"/> class.
        /// This constructor is to be used with Moq testing.
        /// </summary>
        /// <param name="repo">repository object to be used for the initialization of the logic controller for testing purposes.</param>
        public LogicClass(IRepository repo)
        {
            this.repo = repo;
        }

        /// <inheritdoc/>
        public void ChangeAuthor(int id, Author newAuthor)
        {
            if (newAuthor == null)
            {
                throw new ArgumentNullException(nameof(newAuthor));
            }

            Change(
                id,
                this.ListAllAuthor().Select(x => (int)x.Authorid).ToList(),
                newAuthor,
                (int)newAuthor.Authorid,
                this.repo.UpdateAuthor);
        }

        /// <inheritdoc/>
        public void ChangeBook(int id, Book newBook)
        {
            if (newBook == null)
            {
                throw new ArgumentNullException(nameof(newBook));
            }

            Change(
                id,
                this.ListAllBook().Select(x => (int)x.Bookid).ToList(),
                newBook,
                (int)newBook.Bookid,
                this.repo.UpdateBook);
        }

        /// <inheritdoc/>
        public void ChangeStudent(int id, Student newStudent)
        {
            if (newStudent == null)
            {
                throw new ArgumentNullException(nameof(newStudent));
            }

            Change(
                id,
                this.ListAllStudent().Select(x => (int)x.Studentid).ToList(),
                newStudent,
                (int)newStudent.Studentid,
                this.repo.UpdateStudent);
        }

        /// <inheritdoc/>
        public void ChangeCheckout(int id, Checkout newCheckout)
        {
            if (newCheckout == null)
            {
                throw new ArgumentNullException(nameof(newCheckout));
            }

            Change(
                id,
                this.ListAllCheckout().Select(x => (int)x.Checkoutid).ToList(),
                newCheckout,
                (int)newCheckout.Checkoutid,
                this.repo.UpdateCheckout);
        }

        /// <inheritdoc/>
        public void InsertAuthor(Author author)
        {
            if (author == null)
            {
                throw new ArgumentNullException(nameof(author));
            }

            InsertItem(
                author,
                (int)author.Authorid,
                this.ListAllAuthor().Select(x => (int)x.Authorid).ToList(),
                this.repo.CreateAuthor);
        }

        /// <inheritdoc/>
        public void InsertBook(Book book)
        {
            if (book == null)
            {
                throw new ArgumentNullException(nameof(book));
            }

            InsertItem(
                book,
                (int)book.Bookid,
                this.ListAllBook().Select(x => (int)x.Bookid).ToList(),
                this.repo.CreateBook);
        }

        /// <inheritdoc/>
        public void InsertCheckout(Checkout checkout)
        {
            if (checkout == null)
            {
                throw new ArgumentNullException(nameof(checkout));
            }

            InsertItem(
                checkout,
                (int)checkout.Checkoutid,
                this.ListAllCheckout().Select(x => (int)x.Checkoutid).ToList(),
                this.repo.CreateCheckout);
        }

        /// <inheritdoc/>
        public void InsertStudent(Student student)
        {
            if (student == null)
            {
                throw new ArgumentNullException(nameof(student));
            }

            InsertItem(
                student,
                (int)student.Studentid,
                this.ListAllStudent().Select(x => (int)x.Studentid).ToList(),
                this.repo.CreateStudent);
        }

        /// <inheritdoc/>
        public IList<Author> ListAllAuthor()
        {
            return this.repo.ReadAllAuthor();
        }

        /// <inheritdoc/>
        public IList<Book> ListAllBook()
        {
            return this.repo.ReadAllBook();
        }

        /// <inheritdoc/>
        public IList<Checkout> ListAllCheckout()
        {
            return this.repo.ReadAllCheckout();
        }

        /// <inheritdoc/>
        public IList<Student> ListAllStudent()
        {
            return this.repo.ReadAllStudent();
        }

        /// <inheritdoc/>
        public Author GetOneAuthor(int id)
        {
            if (CollectionContainsId(id, this.ListAllAuthor().Select(x => (int)x.Authorid).ToList()))
            {
                return this.repo.GetOneAuthor(id);
            }

            throw new IdNotInCollectionException("Invalid author ID");
        }

        /// <inheritdoc/>
        public Book GetOneBook(int id)
        {
            if (CollectionContainsId(id, this.ListAllBook().Select(x => (int)x.Bookid).ToList()))
            {
                return this.repo.GetOneBook(id);
            }

            throw new IdNotInCollectionException("Invalid book ID");
        }

        /// <inheritdoc/>
        public Student GetOneStudent(int id)
        {
            if (CollectionContainsId(id, this.ListAllStudent().Select(x => (int)x.Studentid).ToList()))
            {
                return this.repo.GetOneStudent(id);
            }

            throw new IdNotInCollectionException("Invalid student ID");
        }

        /// <inheritdoc/>
        public Checkout GetOneCheckout(int id)
        {
            if (CollectionContainsId(id, this.ListAllCheckout().Select(x => (int)x.Checkoutid).ToList()))
            {
                return this.repo.GetOneCheckout(id);
            }

            throw new IdNotInCollectionException("Invalid checkout ID");
        }

        /// <inheritdoc/>
        public void RemoveAuthor(int id)
        {
            if (CollectionContainsId(id, this.ListAllAuthor().Select(x => (int)x.Authorid).ToList()))
            {
                this.repo.DeleteAuthor(id);
            }
            else
            {
                throw new IdNotInCollectionException("Invalid author ID");
            }
        }

        /// <inheritdoc/>
        public void RemoveBook(int id)
        {
            if (CollectionContainsId(id, this.ListAllBook().Select(x => (int)x.Bookid).ToList()))
            {
                this.repo.DeleteBook(id);
            }
            else
            {
                throw new IdNotInCollectionException("Invalid book ID");
            }
        }

        /// <inheritdoc/>
        public void RemoveCheckout(int id)
        {
            if (CollectionContainsId(id, this.ListAllCheckout().Select(x => (int)x.Checkoutid).ToList()))
            {
                this.repo.DeleteCheckout(id);
            }
            else
            {
                throw new IdNotInCollectionException("Invalid checkout ID");
            }
        }

        /// <inheritdoc/>
        public void RemoveStudent(int id)
        {
            if (CollectionContainsId(id, this.ListAllStudent().Select(x => (int)x.Studentid).ToList()))
            {
                this.repo.DeleteStudent(id);
            }
            else
            {
                throw new IdNotInCollectionException("Invalid student ID");
            }
        }

        /// <inheritdoc/>
        public IList<AverageDaysOfBorrows> AvgDayOfBorrowsByAuthors()
        {
            var daysOfBorrows = from x in this.ListAllCheckout()
                                join y in this.ListAllBook() on x.Bookid equals y.Bookid
                                join z in this.ListAllAuthor() on y.Authorid equals z.Authorid
                                select new
                                {
                                    AuthorId = z.Authorid,
                                    AuthorName = z.Firstname + " " + z.Lastname,
                                    Days = x.Returndate == null ? (DateTime.Now - x.Borrowdate).TotalDays : (x.Returndate - x.Borrowdate).GetValueOrDefault().TotalDays,
                                };

            var averageDays = (from x in daysOfBorrows
                    group x by x.AuthorName into g
                    select new AverageDaysOfBorrows
                    {
                        AUTHORNAME = g.Key,
                        AVERAGE = g.Average(y => y.Days),
                    }).ToList();

            foreach (var item in this.ListAllAuthor())
            {
                if (!daysOfBorrows.Any(x => x.AuthorId == item.Authorid))
                {
                    averageDays.Add(new AverageDaysOfBorrows()
                    {
                    AUTHORNAME = item.Firstname + " " + item.Lastname,
                    AVERAGE = 0,
                    });
                }
            }

            return averageDays;
        }

        /// <inheritdoc/>
        public IList<Book> BooksNeverBorrowed()
        {
            var booksNeverBorrowed = this.ListAllBook().Where(x => !this.ListAllCheckout().Any(y => y.Bookid == x.Bookid));
            return booksNeverBorrowed.ToList();
        }

        /// <inheritdoc/>
        public IList<GenresAndNumberOfBorrows> BorrowsByGenres()
        {
            var numberOfBorrowsByGenres = (from x in this.ListAllCheckout()
                                          join y in this.ListAllBook() on x.Bookid equals y.Bookid
                                          group y by y.Genre into g
                                          select new GenresAndNumberOfBorrows
                                          {
                                              Genre = g.Key,
                                              NumberOfBorrows = g.Count(),
                                          }).ToList();

            foreach (var item in this.ListAllBook())
            {
                if (!numberOfBorrowsByGenres.Any(x => x.Genre == item.Genre))
                {
                    numberOfBorrowsByGenres.Add(new GenresAndNumberOfBorrows()
                    {
                        Genre = item.Genre,
                        NumberOfBorrows = 0,
                    });
                }
            }

            return numberOfBorrowsByGenres;
        }

        /// <inheritdoc/>
        public VoucherClass Voucher(int id)
        {
            string title = this.GetOneBook(id).Title;
            int pages = Convert.ToInt32(this.GetOneBook(id).Pages);
            string url = $"http://localhost:8080/BiblioAppServlet/VoucherServlet?title={title}&pages={pages}";
            XDocument xDocument = XDocument.Load(url);

            VoucherClass voucher = new VoucherClass();
            voucher.Title = xDocument.Root.Element("bookTitle").Value;
            voucher.Value = Convert.ToInt32(xDocument.Root.Element("voucherValue").Value, new CultureInfo("en-US"));
            var v = from x in xDocument.Root.Descendants("person")
                    select new Person
                    {
                        Name = x.Element("name").Value,
                        Email = x.Element("email").Value,
                        Gender = x.Element("gender").Value,
                    };
            v.ToList().ForEach(x => voucher.People.Add(x));

            // voucher.People = v.ToList();
            return voucher;
        }

        /// <summary>
        /// Method for checking whether or not a list containing IDs of items contains a certain item with a given ID.
        /// </summary>
        /// <returns>a logic value stating whether or not a list of items contains a certain item with a given ID.</returns>
        /// <param name="id">id of item to search in list.</param>
        /// <param name="idList">list of IDs of items.</param>
        private static bool CollectionContainsId(int id, List<int> idList)
        {
            return idList.Contains(id);
        }

        /// <summary>
        /// Method with safety checks for implementing the insertion of items into the database.
        /// </summary>
        /// <param name="item">item to be inserted into the database.</param>
        /// <param name="id">id of item to be inserted into the database.</param>
        /// <param name="idList">list of IDs of items of the type to be inserted.</param>
        /// <param name="insert">insert method of the type of item to be inserted.</param>
        private static void InsertItem<T>(T item, int id, List<int> idList, Action<T> insert)
        {
            if (item != null)
            {
                if (!CollectionContainsId(id, idList))
                {
                    insert(item);
                }
                else
                {
                    throw new IdAlreadyInCollectionException("ID already exists in collection");
                }
            }
        }

        /// <summary>
        /// Method with safety checks for implementing the update of items in the database.
        /// </summary>
        /// <param name="id">id of item to be updated in the database.</param>
        /// <param name="idList">list of IDs of items of the type to be updated.</param>
        /// <param name="newItem">the updated version of the item that is to be updated.</param>
        /// <param name="newId">Id of updated item.</param>
        /// <param name="updateMethod">update method of the type of item to be updated.</param>
        private static void Change<T>(int id, List<int> idList, T newItem, int newId, Action<int, T> updateMethod)
        {
            if (newItem != null)
            {
                if (CollectionContainsId(id, idList))
                {
                    if (newId == id || !CollectionContainsId(newId, idList))
                    {
                        updateMethod(id, newItem);
                    }
                    else
                    {
                        throw new IdAlreadyInCollectionException("ID already exists");
                    }
                }
                else
                {
                    throw new IdNotInCollectionException("Invalid ID");
                }
            }
        }
    }
}
