﻿namespace BiblioDatCore.Logic
{
    /// <summary>
    /// Supplementary class for storing information of people.
    /// </summary>
    public class Person
    {
        /// <summary>
        /// Gets or sets the name of the person.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the email address of the person.
        /// </summary>
        public string Email { get; set; }

        /// <summary>
        /// Gets or sets the gender of the person.
        /// </summary>
        public string Gender { get; set; }
    }
}
