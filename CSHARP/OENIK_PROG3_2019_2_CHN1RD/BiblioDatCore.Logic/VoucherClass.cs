﻿namespace BiblioDatCore.Logic
{
    using System.Collections.Generic;

    /// <summary>
    /// Supplementary class for creating vouchers containing the title of book to be advertized, the discount value, and a list of people to send the advertisment to.
    /// </summary>
    public class VoucherClass
    {
        private IList<Person> people;

        /// <summary>
        /// Initializes a new instance of the <see cref="VoucherClass"/> class.
        /// </summary>
        public VoucherClass()
        {
            this.people = new List<Person>();
        }

        /// <summary>
        /// Gets or sets the title of the book to be advertized.
        /// </summary>
        public string Title { get; set; }

        /// <summary>
        /// Gets or sets the discount value.
        /// </summary>
        public int Value { get; set; }

        /// <summary>
        /// Gets the list of people to send the advertisment to.
        /// </summary>
        public IList<Person> People { get => this.people; }
    }
}
