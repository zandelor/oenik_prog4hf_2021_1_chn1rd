﻿// This file is used by Code Analysis to maintain SuppressMessage
// attributes that are applied to this project.
// Project-level suppressions either have no target or are given
// a specific target and scoped to a namespace, type, member, etc.

using System.Diagnostics.CodeAnalysis;

[assembly: SuppressMessage("Globalization", "CA1305:Specify IFormatProvider", Justification = "<User's default format is intended behaviour.>", Scope = "member", Target = "~M:BiblioDatCore.Program.Menu.MainMenu(System.Boolean@)")]
[assembly: SuppressMessage("Globalization", "CA1305:Specify IFormatProvider", Justification = "<User's default format is intended behaviour.>", Scope = "member", Target = "~M:BiblioDatCore.Program.Menu.UpdateProperty``1(``0,System.Reflection.PropertyInfo,System.String)")]
[assembly: SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification = "<Exception description happens through exception messaging.>", Scope = "member", Target = "~M:BiblioDatCore.Program.Menu.Start")]
[assembly: SuppressMessage("Performance", "CA1822:Mark members as static", Justification = "<No static is needed.>", Scope = "member", Target = "~M:BiblioDatCore.Program.Menu.IsNullable(System.Type)~System.Boolean")]
[assembly: SuppressMessage("Performance", "CA1822:Mark members as static", Justification = "<No static is needed.>", Scope = "member", Target = "~M:BiblioDatCore.Program.Menu.IsVirtual(System.Reflection.PropertyInfo)~System.Boolean")]
[assembly: SuppressMessage("Performance", "CA1822:Mark members as static", Justification = "<No static is needed.>", Scope = "member", Target = "~M:BiblioDatCore.Program.Menu.PrintCrudMenu")]
[assembly: SuppressMessage("Performance", "CA1822:Mark members as static", Justification = "<No static is needed.>", Scope = "member", Target = "~M:BiblioDatCore.Program.Menu.PrintMenu")]
[assembly: SuppressMessage("Globalization", "CA1303:Do not pass literals as localized parameters", Justification = "<No tables are used for literals.>", Scope = "member", Target = "~M:BiblioDatCore.Program.Menu.CrudMenu``1(System.String,System.Action{``0},System.Func{System.Collections.Generic.IList{``0}},System.Func{System.Int32,``0},System.Action{System.Int32,``0},System.Action{System.Int32})")]
[assembly: SuppressMessage("Globalization", "CA1303:Do not pass literals as localized parameters", Justification = "<No tables are used for literals.>", Scope = "member", Target = "~M:BiblioDatCore.Program.Menu.MainMenu(System.Boolean@)")]
[assembly: SuppressMessage("Globalization", "CA1303:Do not pass literals as localized parameters", Justification = "<No tables are used for literals.>", Scope = "member", Target = "~M:BiblioDatCore.Program.Menu.PrintCrudMenu")]
[assembly: SuppressMessage("Globalization", "CA1303:Do not pass literals as localized parameters", Justification = "<No tables are used for literals.>", Scope = "member", Target = "~M:BiblioDatCore.Program.Menu.PrintMenu")]
[assembly: SuppressMessage("Globalization", "CA1305:Specify IFormatProvider", Justification = "<User's default format is intended behaviour.>", Scope = "member", Target = "~M:BiblioDatCore.Program.Menu.CrudMenu``1(System.String,System.Action{``0},System.Func{System.Collections.Generic.IList{``0}},System.Func{System.Int32,``0},System.Action{System.Int32,``0},System.Action{System.Int32})")]
[assembly: SuppressMessage("StyleCop.CSharp.DocumentationRules", "SA1633:File should have header", Justification = "<No header is needed throughout solution.>")]
[assembly: SuppressMessage("StyleCop.CSharp.DocumentationRules", "SA1633:File should have header", Justification = "<No header is needed throughout solution.>", Scope = "namespace", Target = "~N:BiblioDatCore.Program")]
