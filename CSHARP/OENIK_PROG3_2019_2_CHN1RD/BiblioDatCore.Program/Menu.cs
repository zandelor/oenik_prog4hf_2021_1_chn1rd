﻿namespace BiblioDatCore.Program
{
    using System;
    using System.Collections.Generic;
    using System.Reflection;
    using BiblioDatCore.Logic;

    /// <summary>
    /// Class for the implementation of a menu system and the management of the database using the logic controller.
    /// </summary>
    internal class Menu
    {
        private ILogic db = new LogicClass();

        /// <summary>
        /// Initializes a new instance of the <see cref="Menu"/> class.
        /// </summary>
        /// <param name="db">Logic controller to initialize the menu with.</param>
        public Menu(ILogic db)
        {
            this.db = db;
        }

        /// <summary>
        /// Method for starting the menu system.
        /// </summary>
        public void Start()
        {
            bool exitMenu = false;

            do
            {
                this.PrintMenu();
                try
                {
                    this.MainMenu(ref exitMenu);
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message);
                }
            }
            while (!exitMenu);
        }

        /// <summary>
        /// Method for printing the main menu on the console.
        /// </summary>
        private void PrintMenu()
        {
            Console.WriteLine(
                "1. Authors\r\n" +
                "2. Books\r\n" +
                "3. Students\r\n" +
                "4. Checkouts/borrows\r\n" +
                "5. Books never borrowed\r\n" +
                "6. Borrows by genres\r\n" +
                "7. Avg day of borrows by authors\r\n" +
                "8. Voucher\r\n" +
                "9. Exit");
        }

        /// <summary>
        /// Method for printing the CRUD menu on the console.
        /// </summary>
        private void PrintCrudMenu()
        {
            Console.WriteLine(
                    "1. Insert\r\n" +
                    "2. List\r\n" +
                    "3. Modify\r\n" +
                    "4. Remove\r\n" +
                    "5. Go back");
        }

        /// <summary>
        /// Method for initializing the CRUD menu.
        /// </summary>
        /// <param name="name">The name of the type of the item to be worked with.</param>
        /// <param name="insert">The insert method of the type to be worked with.</param>
        /// <param name="listAll">The ListAll method of the type to be worked with.</param>
        /// <param name="getOne">The GetOne method of the type to be worked with..</param>
        /// <param name="change">The Change method of the type to be worked with..</param>
        /// <param name="remove">The Remove method of the type to be worked with..</param>
        private void CrudMenu<T>(string name, Action<T> insert, Func<IList<T>> listAll, Func<int, T> getOne, Action<int, T> change, Action<int> remove)
        {
            bool exitSubMenu = false;
            do
            {
                Console.WriteLine(name + " menu: ");
                this.PrintCrudMenu();
                switch (Console.ReadLine())
                {
                    case "1":
                        T itemToInsert = (T)this.CreateEntityObj(typeof(T));
                        insert(itemToInsert);
                        break;
                    case "2":
                        this.PrintList(listAll());
                        break;
                    case "3":
                        Console.WriteLine("ID of item to modify: ");
                        int idToModify = int.Parse(Console.ReadLine());
                        T newItem = this.GetNewObjForUpdate(getOne(idToModify));
                        change(idToModify, newItem);
                        break;
                    case "4":
                        Console.WriteLine("ID of item to remove: ");
                        int idToRemove = int.Parse(Console.ReadLine());
                        remove(idToRemove);
                        break;
                    case "5":
                        exitSubMenu = true;
                        break;
                    default:
                        Console.WriteLine("invalid input");
                        break;
                }
            }
            while (!exitSubMenu);
        }

        /// <summary>
        /// Method for initializing the main menu.
        /// </summary>
        /// <param name="exitMenu">The parameter for deciding whether or not the menu should exit.</param>
        private void MainMenu(ref bool exitMenu)
        {
            switch (Console.ReadLine())
            {
                case "1":
                    this.CrudMenu("Author", this.db.InsertAuthor, this.db.ListAllAuthor, this.db.GetOneAuthor, this.db.ChangeAuthor, this.db.RemoveAuthor);
                    break;
                case "2":
                    this.CrudMenu("Book", this.db.InsertBook, this.db.ListAllBook, this.db.GetOneBook, this.db.ChangeBook, this.db.RemoveBook);
                    break;
                case "3":
                    this.CrudMenu("Student", this.db.InsertStudent, this.db.ListAllStudent, this.db.GetOneStudent, this.db.ChangeStudent, this.db.RemoveStudent);
                    break;
                case "4":
                    this.CrudMenu("Checkout", this.db.InsertCheckout, this.db.ListAllCheckout, this.db.GetOneCheckout, this.db.ChangeCheckout, this.db.RemoveCheckout);
                    break;
                case "5":
                    this.PrintList(this.db.BooksNeverBorrowed());
                    break;
                case "6":
                    this.PrintList(this.db.BorrowsByGenres());
                    break;
                case "7":
                    this.PrintList(this.db.AvgDayOfBorrowsByAuthors());
                    break;
                case "8":
                    Console.WriteLine("ID of book to create voucher for: ");
                    VoucherClass voucher = this.db.Voucher(Convert.ToInt32(Console.ReadLine()));
                    Console.WriteLine("Title: " + voucher.Title + ", value: " + voucher.Value);
                    this.PrintList(voucher.People);
                    break;
                case "9":
                    exitMenu = true;
                    break;
                default:
                    Console.WriteLine("invalid input");
                    break;
            }
        }

        /// <summary>
        /// Method for creating database type items.
        /// </summary>
        /// <param name="type">The type of the item to be created.</param>
        private object CreateEntityObj(Type type)
        {
            object newObj = Activator.CreateInstance(type);
            foreach (PropertyInfo property in newObj.GetType().GetProperties())
            {
                if (!this.IsVirtual(property))
                {
                    Console.WriteLine(property.Name + ", type: " + property.PropertyType);
                    string setValue = Console.ReadLine();
                    this.UpdateProperty(newObj, property, setValue);
                }
            }

            return newObj;
        }

        /// <summary>
        /// Decides whether or not a given type is nullable.
        /// </summary>
        /// <param name="type">The type to be checked.</param>
        private bool IsNullable(Type type)
        {
            if (Nullable.GetUnderlyingType(type) != null)
            {
                return true;
            }

            return false;
        }

        /// <summary>
        /// Decides whether or not a given property is virtual.
        /// </summary>
        /// <param name="property">The property to be checked.</param>
        private bool IsVirtual(PropertyInfo property)
        {
            return property.GetAccessors()[0].IsVirtual;
        }

        /// <summary>
        /// Prints the values of a given list on the console.
        /// </summary>
        /// <param name="list">The list to be printed.</param>
        private void PrintList<T>(IList<T> list)
        {
            Console.WriteLine("Printing: " + list.GetType().GetGenericArguments()[0].Name);
            PropertyInfo[] listArgumentProperties = list.GetType().GetGenericArguments()[0].GetProperties();
            foreach (var item in listArgumentProperties)
            {
                if (!this.IsVirtual(item))
                {
                    Console.Write(item.Name + " ");
                }
            }

            Console.WriteLine();
            foreach (var item in list)
            {
                foreach (var item2 in listArgumentProperties)
                {
                    if (!this.IsVirtual(item2))
                    {
                        Console.Write(item2.GetValue(item) + " ");
                    }
                }

                Console.WriteLine();
            }
        }

        /// <summary>
        /// Creates an item to be used with the Update methods while also printing additional help on the procedure on the console.
        /// </summary>
        /// <param name="obj">The item to be updated.</param>
        private T GetNewObjForUpdate<T>(T obj)
        {
            PropertyInfo[] propertiesOfObj = obj.GetType().GetProperties();
            for (int i = 0; i < propertiesOfObj.Length; i++)
            {
                if (!this.IsVirtual(propertiesOfObj[i]))
                {
                    Console.WriteLine(i + ". " + propertiesOfObj[i].Name + ": " + propertiesOfObj[i].GetValue(obj));
                }
            }

            return (T)this.CreateEntityObj(obj.GetType());
        }

        /// <summary>
        /// Method for updating properties of a given object.
        /// </summary>
        /// <param name="obj">The object to be updated.</param>
        /// <param name="property">The property of the object to be updated.</param>
        /// <param name="newValue">The new value of the property.</param>
        private void UpdateProperty<T>(T obj, PropertyInfo property, string newValue)
        {
            if (!this.IsNullable(property.PropertyType))
            {
                if (property.PropertyType == typeof(decimal))
                {
                    property.SetValue(obj, Convert.ToDecimal(newValue), null);
                }
                else if (property.PropertyType == typeof(DateTime))
                {
                    property.SetValue(obj, Convert.ToDateTime(newValue), null);
                }
                else
                {
                    property.SetValue(obj, newValue, null);
                }
            }
            else
            {
                if (newValue == "null")
                {
                    property.SetValue(obj, null, null);
                }
                else if (Nullable.GetUnderlyingType(property.PropertyType) == typeof(decimal))
                {
                    property.SetValue(obj, Convert.ToDecimal(newValue), null);
                }
                else if (Nullable.GetUnderlyingType(property.PropertyType) == typeof(DateTime))
                {
                    property.SetValue(obj, Convert.ToDateTime(newValue), null);
                }
                else
                {
                    property.SetValue(obj, newValue, null);
                }
            }
        }
    }
}
