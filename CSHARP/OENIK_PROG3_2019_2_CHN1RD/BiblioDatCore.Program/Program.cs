﻿using System;

[assembly: CLSCompliant(false)]

namespace BiblioDatCore.Program
{
    using BiblioDatCore.Logic;

    /// <summary>
    /// The main program class containing the main method.
    /// </summary>
    internal class Program
    {
        /// <summary>
        /// The entry point of the program.
        /// </summary>
        private static void Main()
        {
            ILogic db = new LogicClass();
            Menu menu = new Menu(db);
            menu.Start();
        }
    }
}
