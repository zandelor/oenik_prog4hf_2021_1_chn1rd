﻿namespace BiblioDatCore.Repository
{
    using System.Collections.Generic;
    using BiblioDatCore.Data.Models;

    /// <summary>
    /// Interface for Repository methods.
    /// </summary>
    public interface IRepository
    {
        /// <summary>
        /// Method for inserting new author in database.
        /// </summary>
        /// <param name="author">author item to be inserted into database.</param>
        void CreateAuthor(Author author);

        /// <summary>
        /// Method for inserting new book in database.
        /// </summary>
        /// <param name="book">book item to be inserted into database.</param>
        void CreateBook(Book book);

        /// <summary>
        /// Method for inserting new student in database.
        /// </summary>
        /// <param name="student">student item to be inserted into database.</param>
        void CreateStudent(Student student);

        /// <summary>
        /// Method for inserting new checkout in database.
        /// </summary>
        /// <param name="checkout">checkout item to be inserted into database.</param>
        void CreateCheckout(Checkout checkout);

        /// <summary>
        /// Method for reading all author items from database.
        /// </summary>
        /// <returns>a list of all authors in database.</returns>
        IList<Author> ReadAllAuthor();

        /// <summary>
        /// Method for reading all book items from database.
        /// </summary>
        /// <returns>a list of all books in database.</returns>
        IList<Book> ReadAllBook();

        /// <summary>
        /// Method for reading all student items from database.
        /// </summary>
        /// <returns>a list of all students in database.</returns>
        IList<Student> ReadAllStudent();

        /// <summary>
        /// Method for reading all checkout items from database.
        /// </summary>
        /// <returns>a list of all checkouts in database.</returns>
        IList<Checkout> ReadAllCheckout();

        /// <summary>
        /// Method for reading a single author item from database.
        /// </summary>
        /// <param name="id">id of item to be read from database.</param>
        /// <returns>a single specified author from database.</returns>
        Author GetOneAuthor(int id);

        /// <summary>
        /// Method for reading a single book item from database.
        /// </summary>
        /// <param name="id">id of item to be read from database.</param>
        /// <returns>a single specified book from database.</returns>
        Book GetOneBook(int id);

        /// <summary>
        /// Method for reading a single student item from database.
        /// </summary>
        /// <param name="id">id of item to be read from database.</param>
        /// <returns>a single specified student from database.</returns>
        Student GetOneStudent(int id);

        /// <summary>
        /// Method for reading a single checkout item from database.
        /// </summary>
        /// <param name="id">id of item to be read from database.</param>
        /// <returns>a single specified checkout from database.</returns>
        Checkout GetOneCheckout(int id);

        /// <summary>
        /// Method for replacing a single specified author item in the database with a new author item.
        /// </summary>
        /// <param name="id">id of item to be updated in the database.</param>
        /// <param name="newAuthor">id of new item to be inserted into the database.</param>
        void UpdateAuthor(int id, Author newAuthor);

        /// <summary>
        /// Method for replacing a single specified book item in the database with a new author item.
        /// </summary>
        /// <param name="id">id of item to be updated in the database.</param>
        /// <param name="newBook">id of new item to be inserted into the database.</param>
        void UpdateBook(int id, Book newBook);

        /// <summary>
        /// Method for replacing a single specified student item in the database with a new author item.
        /// </summary>
        /// <param name="id">id of item to be updated in the database.</param>
        /// <param name="newStudent">id of new item to be inserted into the database.</param>
        void UpdateStudent(int id, Student newStudent);

        /// <summary>
        /// Method for replacing a single specified checkout item in the database with a new author item.
        /// </summary>
        /// <param name="id">id of item to be updated in the database.</param>
        /// <param name="newCheckout">id of new item to be inserted into the database.</param>
        void UpdateCheckout(int id, Checkout newCheckout);

        /// <summary>
        /// Method for removing a single specified author item from the database.
        /// </summary>
        /// <param name="id">id of item to be removed from the database.</param>
        void DeleteAuthor(int id);

        /// <summary>
        /// Method for removing a single specified book item from the database.
        /// </summary>
        /// <param name="id">id of item to be removed from the database.</param>
        void DeleteBook(int id);

        /// <summary>
        /// Method for removing a single specified student item from the database.
        /// </summary>
        /// <param name="id">id of item to be removed from the database.</param>
        void DeleteStudent(int id);

        /// <summary>
        /// Method for removing a single specified checkout item from the database.
        /// </summary>
        /// <param name="id">id of item to be removed from the database.</param>
        void DeleteCheckout(int id);
    }
}
