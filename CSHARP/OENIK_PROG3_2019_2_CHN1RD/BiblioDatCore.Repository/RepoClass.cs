﻿using System;

[assembly: CLSCompliant(false)]

namespace BiblioDatCore.Repository
{
    using System.Collections.Generic;
    using System.Linq;
    using BiblioDatCore.Data.Models;

    /// <summary>
    /// Class for the implementation of the database repository.
    /// </summary>
    public class RepoClass : IRepository
    {
        private readonly BDBContext db;

        /// <summary>
        /// Initializes a new instance of the <see cref="RepoClass"/> class.
        /// </summary>
        /// <param name="db">Entity database object to create repository from.</param>
        public RepoClass(BDBContext db)
        {
            this.db = db;

            this.ReadAllCheckout().ToList().ForEach(x => this.DeleteCheckout((int)x.Checkoutid));
        }

        /// <inheritdoc/>
        public void CreateAuthor(Author author)
        {
            this.db.Authors.Add(author);
            this.db.SaveChanges();
        }

        /// <inheritdoc/>
        public void CreateBook(Book book)
        {
            this.db.Books.Add(book);
            this.db.SaveChanges();
        }

        /// <inheritdoc/>
        public void CreateCheckout(Checkout checkout)
        {
            this.db.Checkouts.Add(checkout);
            this.db.SaveChanges();
        }

        /// <inheritdoc/>
        public void CreateStudent(Student student)
        {
            this.db.Students.Add(student);
            this.db.SaveChanges();
        }

        /// <inheritdoc/>
        public void DeleteAuthor(int id)
        {
            this.db.Authors.Remove(this.GetOneAuthor(id));
            this.db.SaveChanges();
        }

        /// <inheritdoc/>
        public void DeleteBook(int id)
        {
            this.db.Books.Remove(this.GetOneBook(id));
            this.db.SaveChanges();
        }

        /// <inheritdoc/>
        public void DeleteCheckout(int id)
        {
            this.db.Checkouts.Remove(this.GetOneCheckout(id));
            this.db.SaveChanges();
        }

        /// <inheritdoc/>
        public void DeleteStudent(int id)
        {
            this.db.Students.Remove(this.GetOneStudent(id));
            this.db.SaveChanges();
        }

        /// <inheritdoc/>
        public Author GetOneAuthor(int id)
        {
            return this.ReadAllAuthor().Where(x => x.Authorid.Equals(id)).FirstOrDefault();
        }

        /// <inheritdoc/>
        public Book GetOneBook(int id)
        {
            return this.ReadAllBook().Where(x => x.Bookid == id).FirstOrDefault();
        }

        /// <inheritdoc/>
        public Checkout GetOneCheckout(int id)
        {
            return this.ReadAllCheckout().Where(x => x.Checkoutid == id).FirstOrDefault();
        }

        /// <inheritdoc/>
        public Student GetOneStudent(int id)
        {
            return this.ReadAllStudent().Where(x => x.Studentid == id).FirstOrDefault();
        }

        /// <inheritdoc/>
        public IList<Author> ReadAllAuthor()
        {
            return this.db.Authors.ToList();
        }

        /// <inheritdoc/>
        public IList<Book> ReadAllBook()
        {
            return this.db.Books.ToList();
        }

        /// <inheritdoc/>
        public IList<Checkout> ReadAllCheckout()
        {
            return this.db.Checkouts.ToList();
        }

        /// <inheritdoc/>
        public IList<Student> ReadAllStudent()
        {
            return this.db.Students.ToList();
        }

        /// <inheritdoc/>
        public void UpdateAuthor(int id, Author newAuthor)
        {
            this.DeleteAuthor(id);
            this.CreateAuthor(newAuthor);
            this.db.SaveChanges();
        }

        /// <inheritdoc/>
        public void UpdateBook(int id, Book newBook)
        {
            this.DeleteBook(id);
            this.CreateBook(newBook);
            this.db.SaveChanges();
        }

        /// <inheritdoc/>
        public void UpdateStudent(int id, Student newStudent)
        {
            this.DeleteStudent(id);
            this.CreateStudent(newStudent);
            this.db.SaveChanges();
        }

        /// <inheritdoc/>
        public void UpdateCheckout(int id, Checkout newCheckout)
        {
            this.DeleteCheckout(id);
            this.CreateCheckout(newCheckout);
            this.db.SaveChanges();
        }
    }
}
