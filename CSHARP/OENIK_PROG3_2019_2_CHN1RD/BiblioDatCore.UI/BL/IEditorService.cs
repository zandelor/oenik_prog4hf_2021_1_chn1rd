﻿namespace BiblioDatCore.UI.BL
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using BiblioDatCore.UI.Data;

    /// <summary>
    /// Interface describing methods necessary for editing student entries.
    /// </summary>
    public interface IEditorService
    {
        /// <summary>
        /// Method for editing student entries.
        /// </summary>
        /// <param name="student">Student to be edited.</param>
        /// <returns>Returns a boolean statement whether edit was successful.</returns>
        bool EditStudent(StudentWPF student);
    }
}
