﻿namespace BiblioDatCore.UI.BL
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using BiblioDatCore.UI.Data;

    /// <summary>
    /// Interface describing CRUD operations for student entries.
    /// </summary>
    public interface IStudentLogic
    {
        /// <summary>
        /// Method for adding student to list of students.
        /// </summary>
        /// <param name="list">List to be inserted into.</param>
        void AddStudent(IList<StudentWPF> list);

        /// <summary>
        /// Method for modifying student entries.
        /// </summary>
        /// <param name="studentToModify">Student entry to modify.</param>
        void ModStudent(StudentWPF studentToModify);

        /// <summary>
        /// Method for deleting student entry.
        /// </summary>
        /// <param name="list">List to be deleted from.</param>
        /// <param name="student">Student to be deleted.</param>
        void DelStudent(IList<StudentWPF> list, StudentWPF student);

        /// <summary>
        /// Method for grabbing all student entries from database.
        /// </summary>
        /// <returns>List of all students in database.</returns>
        IEnumerable<StudentWPF> GetAllStudentsFromDatabase();
    }
}
