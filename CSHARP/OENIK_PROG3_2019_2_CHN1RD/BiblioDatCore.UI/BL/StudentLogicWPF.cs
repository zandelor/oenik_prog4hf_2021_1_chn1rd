﻿namespace BiblioDatCore.UI.BL
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using BiblioDatCore.Logic;
    using BiblioDatCore.UI.Data;
    using GalaSoft.MvvmLight.Messaging;

    /// <summary>
    /// Class for implementing CRUD operations for WPF application.
    /// </summary>
    public class StudentLogicWPF : IStudentLogic
    {
        private ILogic db;

        private IEditorService editorService;
        private IMessenger messengerService;

        /// <summary>
        /// Initializes a new instance of the <see cref="StudentLogicWPF"/> class.
        /// </summary>
        /// <param name="editorService">Editor service to be used in class.</param>
        /// <param name="messengerService">Messenger service to be used in class.</param>
        /// <param name="db">Database to be used in class.</param>
        public StudentLogicWPF(IEditorService editorService, IMessenger messengerService, ILogic db)
        {
            this.db = db;
            this.editorService = editorService;
            this.messengerService = messengerService;
        }

        /// <inheritdoc/>
        public void AddStudent(IList<StudentWPF> list)
        {
            StudentWPF student = new StudentWPF();
            student.DateOfBirth = DateTime.Now;
            if (this.editorService.EditStudent(student))
            {
                try
                {
                    this.db.InsertStudent(student.ConvertToEntity());
                    list.Add(student);

                    this.messengerService.Send("ADD OK", "LogicResult");
                }
                catch (Exception e)
                {
                    this.messengerService.Send("ADD ERROR: " + e.Message, "LogicResult");
                }
            }
            else
            {
                this.messengerService.Send("ADD CANCELLED", "LogicResult");
            }
        }

        /// <inheritdoc/>
        public void DelStudent(IList<StudentWPF> list, StudentWPF student)
        {
            if (student != null && list != null && list.Contains(student))
            {
                try
                {
                    this.db.RemoveStudent(student.Id);
                    list.Remove(student);
                    this.messengerService.Send("DEL OK", "LogicResult");
                }
                catch (Exception e)
                {
                    this.messengerService.Send("DEL ERROR: " + e.Message, "LogicResult");
                }
            }
            else
            {
                this.messengerService.Send("DEL FAILED", "LogicResult");
            }
        }

        /// <inheritdoc/>
        public IEnumerable<StudentWPF> GetAllStudentsFromDatabase()
        {
            return this.db.ListAllStudent().Select(x => new StudentWPF(x));
        }

        /// <inheritdoc/>
        public void ModStudent(StudentWPF studentToModify)
        {
            if (studentToModify == null)
            {
                this.messengerService.Send("EDIT FAILED", "LogicResult");
                return;
            }

            StudentWPF clone = new StudentWPF();
            clone.CopyFrom(studentToModify);
            if (this.editorService.EditStudent(clone))
            {
                try
                {
                    this.db.ChangeStudent(studentToModify.Id, clone.ConvertToEntity());
                    studentToModify.CopyFrom(clone);
                    this.messengerService.Send("MOD OK", "LogicResult");
                }
                catch (Exception e)
                {
                    this.messengerService.Send("EDIT ERROR: " + e.Message, "LogicResult");
                }
            }
            else
            {
                this.messengerService.Send("EDIT CANCELLED", "LogicResult");
            }
        }
    }
}
