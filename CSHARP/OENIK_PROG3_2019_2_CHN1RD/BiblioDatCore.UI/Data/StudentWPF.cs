﻿namespace BiblioDatCore.UI.Data
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using BiblioDatCore.Data.Models;
    using GalaSoft.MvvmLight;

    /// <summary>
    /// Enum used for possible years of class.
    /// </summary>
    public enum ClassYear
    {
        /// <summary>
        /// Empty.
        /// </summary>
        None,

        /// <summary>
        /// Class year 1.
        /// </summary>
        One = 1,

        /// <summary>
        /// Class year 2.
        /// </summary>
        Two,

        /// <summary>
        /// Class year 3.
        /// </summary>
        Three,

        /// <summary>
        /// Class year 4.
        /// </summary>
        Four,

        /// <summary>
        /// Class year 5.
        /// </summary>
        Five,

        /// <summary>
        /// Class year 6.
        /// </summary>
        Six,

        /// <summary>
        /// Class year 7.
        /// </summary>
        Seven,

        /// <summary>
        /// Class year 8.
        /// </summary>
        Eight,

        /// <summary>
        /// Class year 9.
        /// </summary>
        Nine,

        /// <summary>
        /// Class year 10.
        /// </summary>
        Ten,

        /// <summary>
        /// Class year 11.
        /// </summary>
        Eleven,

        /// <summary>
        /// Class year 12.
        /// </summary>
        Twelve,

        /// <summary>
        /// Class year 13.
        /// </summary>
        Thirteen,

        /// <summary>
        /// Class year 14.
        /// </summary>
        Fourteen,
    }

    /// <summary>
    /// Data class representing Student entries in WPF project.
    /// </summary>
    public class StudentWPF : ObservableObject
    {
        private int id;
        private string firstName;
        private string lastName;
        private string gender;
        private ClassYear classYear;
        private DateTime dateOfBirth;

        /// <summary>
        /// Initializes a new instance of the <see cref="StudentWPF"/> class.
        /// </summary>
        public StudentWPF()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="StudentWPF"/> class.
        /// </summary>
        /// <param name="student">Student to be initilized mor operations.</param>
        public StudentWPF(Student student)
            : this()
        {
            this.CopyFrom(student);
        }

        /// <summary>
        /// Gets or sets id of student.
        /// </summary>
        public int Id { get => this.id; set => this.Set(ref this.id, value); }

        /// <summary>
        /// Gets or sets first name of student.
        /// </summary>
        public string FirstName { get => this.firstName; set => this.Set(ref this.firstName, value); }

        /// <summary>
        /// Gets or sets last name of student.
        /// </summary>
        public string LastName { get => this.lastName; set => this.Set(ref this.lastName, value); }

        /// <summary>
        /// Gets or sets gender of student.
        /// </summary>
        public string Gender { get => this.gender; set => this.Set(ref this.gender, value); }

        /// <summary>
        /// Gets or sets class year of student.
        /// </summary>
        public ClassYear ClassYear { get => this.classYear; set => this.Set(ref this.classYear, value); }

        /// <summary>
        /// Gets or sets date of birth of student.
        /// </summary>
        public DateTime DateOfBirth { get => this.dateOfBirth; set => this.Set(ref this.dateOfBirth, value); }

        /// <summary>
        /// Copies student from other WPF student object.
        /// </summary>
        /// <param name="student">Student entry to be copied from.</param>
        public void CopyFrom(StudentWPF student)
        {
            if (student == null)
            {
                throw new ArgumentNullException(nameof(student));
            }

            this.Id = student.Id;
            this.FirstName = student.FirstName;
            this.LastName = student.LastName;
            this.Gender = student.Gender;
            this.ClassYear = student.ClassYear;
            this.DateOfBirth = student.DateOfBirth;
        }

        /// <summary>
        /// Copies student from other student entity object.
        /// </summary>
        /// <param name="student">Student entry to be copied from.</param>
        public void CopyFrom(Student student)
        {
            if (student == null)
            {
                throw new ArgumentNullException(nameof(student));
            }

            this.Id = (int)student.Studentid;
            this.FirstName = student.Firstname;
            this.LastName = student.Lastname;
            this.Gender = student.Gender;
            this.ClassYear = (ClassYear)student.Classyear;
            this.DateOfBirth = student.Dateofbirth;
        }

        /// <summary>
        /// Converts wpf student object to entity student.
        /// </summary>
        /// <returns>Student entity object.</returns>
        public Student ConvertToEntity()
        {
            return new Student() { Studentid = this.Id, Firstname = this.FirstName, Lastname = this.LastName, Gender = this.Gender, Classyear = (int)this.ClassYear, Dateofbirth = this.DateOfBirth };
        }

        /// <summary>
        /// Convert student object to string.
        /// </summary>
        /// <returns>String representation of student.</returns>
        public override string ToString()
        {
            return this.FirstName + " " + this.LastName;
        }
    }
}
