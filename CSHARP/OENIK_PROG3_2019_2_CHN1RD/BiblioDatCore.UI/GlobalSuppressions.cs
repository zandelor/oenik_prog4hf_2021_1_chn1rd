﻿// This file is used by Code Analysis to maintain SuppressMessage
// attributes that are applied to this project.
// Project-level suppressions either have no target or are given
// a specific target and scoped to a namespace, type, member, etc.

using System.Diagnostics.CodeAnalysis;

[assembly: SuppressMessage("Globalization", "CA1305:Specify IFormatProvider", Justification = "<User default localization is intended behaviour.>", Scope = "member", Target = "~M:BiblioDatCore.UI.VM.EditorViewModel.#ctor")]
[assembly: SuppressMessage("Globalization", "CA1305:Specify IFormatProvider", Justification = "<User default localization is intended behaviour.>", Scope = "member", Target = "~M:BiblioDatCore.UI.VM.MainViewModel.#ctor(BiblioDatCore.UI.BL.IStudentLogic)")]
[assembly: SuppressMessage("StyleCop.CSharp.DocumentationRules", "SA1633:File should have header", Justification = "<No header is to be used throughout solution.>")]
[assembly: SuppressMessage("StyleCop.CSharp.DocumentationRules", "SA1633:File should have header", Justification = "<No header is to be used throughout solution.>", Scope = "namespace", Target = "~N:BiblioDatCore.UI.VM")]
[assembly: SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification = "<No special handling needed, message is sent through exception.>", Scope = "member", Target = "~M:BiblioDatCore.UI.BL.StudentLogicWPF.AddStudent(System.Collections.Generic.IList{BiblioDatCore.UI.Data.StudentWPF})")]
[assembly: SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification = "<No special handling needed, message is sent through exception.>", Scope = "member", Target = "~M:BiblioDatCore.UI.BL.StudentLogicWPF.DelStudent(System.Collections.Generic.IList{BiblioDatCore.UI.Data.StudentWPF},BiblioDatCore.UI.Data.StudentWPF)")]
[assembly: SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification = "<No special handling needed, message is sent through exception.>", Scope = "member", Target = "~M:BiblioDatCore.UI.BL.StudentLogicWPF.ModStudent(BiblioDatCore.UI.Data.StudentWPF)")]
[assembly: SuppressMessage("Design", "CA1062:Validate arguments of public methods", Justification = "<No special handling needed, message is sent through exception.>", Scope = "member", Target = "~M:BiblioDatCore.UI.BL.StudentLogicWPF.AddStudent(System.Collections.Generic.IList{BiblioDatCore.UI.Data.StudentWPF})")]
