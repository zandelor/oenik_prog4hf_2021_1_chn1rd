﻿namespace BiblioDatCore.UI
{
    using System;
    using System.Collections.Generic;
    using System.Configuration;
    using System.Data;
    using System.Linq;
    using System.Threading.Tasks;
    using System.Windows;
    using BiblioDatCore.Logic;
    using BiblioDatCore.UI.BL;
    using BiblioDatCore.UI.UserInterface;
    using CommonServiceLocator;
    using GalaSoft.MvvmLight.Ioc;
    using GalaSoft.MvvmLight.Messaging;

    /// <summary>
    /// Ioc class for fixing package error.
    /// </summary>
    public class MyIoc : SimpleIoc, IServiceLocator
    {
        /// <summary>
        /// Gets ioc instance to be worked with.
        /// </summary>
        public static MyIoc Instance { get; private set; } = new MyIoc();
    }
}
