﻿namespace BiblioDatCore.UI.UserInterface
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using BiblioDatCore.UI.BL;
    using BiblioDatCore.UI.Data;

    /// <summary>
    /// Class for editor implementation.
    /// </summary>
    public class EditorServiceViaWindow : IEditorService
    {
        /// <inheritdoc/>
        public bool EditStudent(StudentWPF student)
        {
            EditorWindow win = new EditorWindow(student);
            return win.ShowDialog() ?? false;
        }
    }
}
