﻿namespace BiblioDatCore.UI.UserInterface
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Data;
    using System.Windows.Documents;
    using System.Windows.Input;
    using System.Windows.Media;
    using System.Windows.Media.Imaging;
    using System.Windows.Shapes;
    using BiblioDatCore.UI.Data;
    using BiblioDatCore.UI.VM;

    /// <summary>
    /// Interaction logic for EditorWindow.xaml.
    /// </summary>
    public partial class EditorWindow : Window
    {
        private EditorViewModel vm;

        /// <summary>
        /// Initializes a new instance of the <see cref="EditorWindow"/> class.
        /// </summary>
        public EditorWindow()
        {
            this.InitializeComponent();
            this.vm = this.FindResource("VM") as EditorViewModel;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="EditorWindow"/> class.
        /// </summary>
        /// <param name="student">Student to be edited.</param>
        public EditorWindow(StudentWPF student)
            : this()
        {
            this.vm.Student = student;
        }

        /// <summary>
        /// Gets student to be edited.
        /// </summary>
        public StudentWPF Student { get => this.vm.Student; }

        /// <summary>
        /// Event for submitting edit.
        /// </summary>
        /// <param name="sender">Sender of event.</param>
        /// <param name="e">Args of event.</param>
        private void OkClick(object sender, RoutedEventArgs e)
        {
            this.DialogResult = true;
        }

        /// <summary>
        /// Event for cancelling edit.
        /// </summary>
        /// <param name="sender">Sender of event.</param>
        /// <param name="e">Args of event.</param>
        private void CancelClick(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }

        private void TextBox_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            e.Handled = !char.IsDigit(e.Text[0]);
        }
    }
}
