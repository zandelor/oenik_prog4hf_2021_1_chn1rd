﻿namespace BiblioDatCore.UI.VM
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using BiblioDatCore.UI.Data;
    using GalaSoft.MvvmLight;

    /// <summary>
    /// View model class of editor.
    /// </summary>
    public class EditorViewModel : ViewModelBase
    {
        private StudentWPF student;

        /// <summary>
        /// Initializes a new instance of the <see cref="EditorViewModel"/> class.
        /// </summary>
        public EditorViewModel()
        {
            this.student = new StudentWPF();
            if (this.IsInDesignMode)
            {
                this.student.FirstName = "First";
                this.student.LastName = "Last";
                this.student.Gender = "Male";
                this.student.Id = 19827;
                this.student.DateOfBirth = DateTime.Parse("Jan 1, 2009");
            }
        }

        /// <summary>
        /// Gets array for listing ClassYear enum elements.
        /// </summary>
        public static Array ClassYears
        {
            get => Enum.GetValues(typeof(ClassYear));
        }

        /// <summary>
        /// Gets or sets student to be edited.
        /// </summary>
        public StudentWPF Student { get => this.student; set => this.Set(ref this.student, value); }
    }
}
