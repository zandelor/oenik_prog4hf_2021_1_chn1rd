﻿namespace BiblioDatCore.UI.VM
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows.Input;
    using BiblioDatCore.UI.BL;
    using BiblioDatCore.UI.Data;
    using CommonServiceLocator;
    using GalaSoft.MvvmLight;
    using GalaSoft.MvvmLight.Command;

    /// <summary>
    /// Main view model of WPF application.
    /// </summary>
    public class MainViewModel : ViewModelBase
    {
        private IStudentLogic logic;
        private StudentWPF studentSelected;

        /// <summary>
        /// Initializes a new instance of the <see cref="MainViewModel"/> class.
        /// </summary>
        /// <param name="logic">Logic to be initialized with.</param>
        public MainViewModel(IStudentLogic logic)
        {
            this.logic = logic;

            this.Students = new ObservableCollection<StudentWPF>();

            if (this.IsInDesignMode)
            {
                this.Students.Add(new StudentWPF() { Id = 29503, FirstName = "ASHTON", LastName = "FABBRI", Gender = "MALE", ClassYear = (ClassYear)5, DateOfBirth = DateTime.Parse("19-OCT-1941") });
                this.Students.Add(new StudentWPF() { Id = 28872, FirstName = "MARIA", LastName = "MCKEADY", Gender = "FEMALE", ClassYear = (ClassYear)13, DateOfBirth = DateTime.Parse("20-MAY-1962") });
            }

            this.logic.GetAllStudentsFromDatabase().ToList().ForEach(x => this.Students.Add(x));
            this.AddCommand = new RelayCommand(() => this.logic.AddStudent(this.Students));
            this.ModifyCommand = new RelayCommand(() => this.logic.ModStudent(this.StudentSelected));
            this.DeleteCommand = new RelayCommand(() => this.logic.DelStudent(this.Students, this.StudentSelected));
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="MainViewModel"/> class.
        /// </summary>
        public MainViewModel()
            : this(IsInDesignModeStatic ? null : ServiceLocator.Current.GetInstance<IStudentLogic>())
        {
        }

        /// <summary>
        /// Gets list of students to be worked with.
        /// </summary>
        public ObservableCollection<StudentWPF> Students { get; private set; }

        /// <summary>
        /// Gets or sets selected student on UI.
        /// </summary>
        public StudentWPF StudentSelected { get => this.studentSelected; set => this.Set(ref this.studentSelected, value); }

        /// <summary>
        /// Gets command to add new student.
        /// </summary>
        public ICommand AddCommand { get; private set; }

        /// <summary>
        /// Gets command to modifystudent.
        /// </summary>Í
        public ICommand ModifyCommand { get; private set; }

        /// <summary>
        /// Gets command to deleted student.
        /// </summary>Í
        public ICommand DeleteCommand { get; private set; }
    }
}
