﻿namespace BiblioDatCore.Web.Controllers
{
    /// <summary>
    /// Class defining API result type for API result communication.
    /// </summary>
    public class ApiResult
    {
        /// <summary>
        /// Gets or sets a value indicating whether API operations were successful.
        /// </summary>
        public bool OperationResult { get; set; }
    }
}