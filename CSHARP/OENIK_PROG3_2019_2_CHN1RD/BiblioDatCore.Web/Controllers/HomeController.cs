﻿namespace BiblioDatCore.Web.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Linq;
    using System.Threading.Tasks;
    using BiblioDatCore.Web.Models;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.Extensions.Logging;

    /// <summary>
    /// Home page controler class.
    /// </summary>
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;

        /// <summary>
        /// Initializes a new instance of the <see cref="HomeController"/> class.
        /// </summary>
        /// <param name="logger">Logger object.</param>
        public HomeController(ILogger<HomeController> logger)
        {
            this._logger = logger;
        }

        /// <summary>
        /// Method to get Index view.
        /// </summary>
        /// <returns>Index view.</returns>
        public IActionResult Index()
        {
            return this.View();
        }

        /// <summary>
        /// Method to get Privacy view.
        /// </summary>
        /// <returns>Privacy page.</returns>
        public IActionResult Privacy()
        {
            return this.View();
        }

        /// <summary>
        /// Method to get Error view.
        /// </summary>
        /// <returns>Error view model.</returns>
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return this.View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? this.HttpContext.TraceIdentifier });
        }
    }
}
