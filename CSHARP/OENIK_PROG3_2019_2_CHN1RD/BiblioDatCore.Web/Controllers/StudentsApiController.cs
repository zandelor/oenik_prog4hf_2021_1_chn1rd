﻿namespace BiblioDatCore.Web.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using AutoMapper;
    using BiblioDatCore.Logic;
    using Microsoft.AspNetCore.Mvc;

    /// <summary>
    /// Class implementing API CRUD controller for student database.
    /// </summary>
    public class StudentsApiController : ControllerBase
    {
        private ILogic logic;
        private IMapper mapper;

        /// <summary>
        /// Initializes a new instance of the <see cref="StudentsApiController"/> class.
        /// </summary>
        /// <param name="logic">Logic to access database.</param>
        /// <param name="mapper">Mapper to convert to and from entities.</param>
        public StudentsApiController(ILogic logic, IMapper mapper)
        {
            this.logic = logic;
            this.mapper = mapper;
        }

        /// <summary>
        /// Gets list of students in json through API.
        /// </summary>
        /// <returns>List of students.</returns>
        [HttpGet]
        [ActionName("all")]
        public IEnumerable<Models.Student> GetAll()
        {
            var students = this.logic.ListAllStudent();
            return this.mapper.Map<IList<Data.Models.Student>, List<Models.Student>>(students);
        }

        /// <summary>
        /// Method for deleting one student from database through API.
        /// </summary>
        /// <param name="id">Id of student to delete.</param>
        /// <returns>Result of operation.</returns>
        [HttpGet]
        [ActionName("del")]
        public ApiResult DelOneStudent(int id)
        {
            bool result = true;
            try
            {
                this.logic.RemoveStudent(id);
            }
            catch
            {
                result = false;
            }

            return new ApiResult() { OperationResult = result };
        }

        /// <summary>
        /// Method for inserting student to database through API.
        /// </summary>
        /// <param name="student">Student to be insterted.</param>
        /// <returns>Result of operation.</returns>
        [HttpPost]
        [ActionName("add")]
        public ApiResult AddOneStudent(Models.Student student)
        {
            bool result = true;
            try
            {
                this.logic.InsertStudent(this.mapper.Map<Models.Student, Data.Models.Student>(student));
            }
            catch
            {
                result = false;
            }

            return new ApiResult() { OperationResult = result };
        }

        /// <summary>
        /// Method for modifying student entry through API.
        /// </summary>
        /// <param name="idToChange">Id of student to modify.</param>
        /// <param name="student">Modified student.</param>
        /// <returns>Result of opertaion.</returns>
        [HttpPost]
        [ActionName("mod")]
        public ApiResult ModOneStudent(int idToChange, Models.Student student)
        {
            bool result = true;
            try
            {
                this.logic.ChangeStudent(idToChange, this.mapper.Map<Models.Student, Data.Models.Student>(student));
            }
            catch
            {
                result = false;
            }

            return new ApiResult() { OperationResult = result };
        }
    }
}