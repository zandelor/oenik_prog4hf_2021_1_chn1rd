﻿namespace BiblioDatCore.Web.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using AutoMapper;
    using BiblioDatCore.Logic;
    using BiblioDatCore.Web.Models;
    using Microsoft.AspNetCore.Mvc;

    /// <summary>
    /// Class for Students Controller.
    /// </summary>
    public class StudentsController : Controller
    {
        private ILogic logic;
        private IMapper mapper;
        private StudentListViewModel vm;

        /// <summary>
        /// Initializes a new instance of the <see cref="StudentsController"/> class.
        /// </summary>
        /// <param name="logic">Logic object for database operations.</param>
        /// <param name="mapper">Mapper object for entity/model class conversions.</param>
        public StudentsController(ILogic logic, IMapper mapper)
        {
            this.logic = logic;
            this.mapper = mapper;

            this.vm = new StudentListViewModel();
            this.vm.EditedStudent = new Models.Student();

            IList<Data.Models.Student> students = logic.ListAllStudent();
            this.vm.ListOfStudents = mapper.Map<IList<Data.Models.Student>, List<Models.Student>>(students);
        }

        /// <summary>
        /// Method to get Index page.
        /// </summary>
        /// <returns>StudentsIndex view.</returns>
        public IActionResult Index()
        {
            this.ViewData["editAction"] = "AddNew";
            return this.View("StudentsIndex", this.vm);
        }

        /// <summary>
        /// Method to get Details page.
        /// </summary>
        /// <param name="id">Id of student to print.</param>
        /// <returns>StudentsDetail view.</returns>
        public IActionResult Details(int id)
        {
            return this.View("StudentsDetails", this.GetStudentModel(id));
        }

        /// <summary>
        /// Method to process remove actions.
        /// </summary>
        /// <param name="id">Id of student to remove.</param>
        /// <returns>Redirect action to Index.</returns>
        public IActionResult Remove(int id)
        {
            try
            {
                this.logic.RemoveStudent(id);
                this.TempData["editResult"] = "Delete successful";
            }
            catch (Exception e)
            {
                this.TempData["editResult"] = "Delete failed: " + e.Message;
            }

            return this.RedirectToAction(nameof(this.Index));
        }

        /// <summary>
        /// Method for processing edit get request.
        /// </summary>
        /// <param name="id">Id of student to be edited.</param>
        /// <returns>Index page with editor.</returns>
        public IActionResult Edit(int id)
        {
            this.ViewData["editAction"] = "Edit";
            this.vm.EditedStudent = this.GetStudentModel(id);
            return this.View("StudentsIndex", this.vm);
        }

        /// <summary>
        /// Method for processing Edit post request.
        /// </summary>
        /// <param name="student">Student edited.</param>
        /// <param name="editAction">Type of edit (add new or edit existing).</param>
        /// <returns>Redirect to index page.</returns>
        [HttpPost]
        public IActionResult Edit(Models.Student student, string editAction)
        {
            if (this.ModelState.IsValid && student != null)
            {
                this.TempData["editResult"] = "Edit successful.";
                try
                {
                    if (editAction == "AddNew")
                    {
                        this.logic.InsertStudent(this.ConvertStudentToEntity(student));
                    }
                    else
                    {
                        this.logic.ChangeStudent(student.Id, this.ConvertStudentToEntity(student));
                    }
                }
                catch (Exception e)
                {
                    this.TempData["editResult"] = "Edit failed: " + e.Message;
                }

                return this.RedirectToAction(nameof(this.Index));
            }
            else
            {
                this.ViewData["editAction"] = "Edit";
                this.vm.EditedStudent = student;
                return this.View("StudentsIndex", this.vm);
            }
        }

        private Models.Student GetStudentModel(int id)
        {
            Data.Models.Student oneStudent = this.logic.GetOneStudent(id);
            return this.mapper.Map<Data.Models.Student, Models.Student>(oneStudent);
        }

        private Data.Models.Student ConvertStudentToEntity(Models.Student student)
        {
            return this.mapper.Map<Models.Student, Data.Models.Student>(student);
        }
    }
}
