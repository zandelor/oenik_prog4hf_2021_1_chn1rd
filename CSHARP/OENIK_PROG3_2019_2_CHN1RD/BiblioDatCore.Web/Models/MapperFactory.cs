﻿namespace BiblioDatCore.Web.Models
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using AutoMapper;

    /// <summary>
    /// Class to create mapper.
    /// </summary>
    public class MapperFactory
    {
        /// <summary>
        /// Creates mapper for Student objects.
        /// </summary>
        /// <returns>Mapper for student objects.</returns>
        public static IMapper CreateMapper()
        {
            var config = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<Data.Models.Student, Web.Models.Student>()
                .ForMember(dest => dest.Id, map => map.MapFrom(src => src.Studentid))
                .ForMember(dest => dest.FirstName, map => map.MapFrom(src => src.Firstname))
                .ForMember(dest => dest.LastName, map => map.MapFrom(src => src.Lastname))
                .ForMember(dest => dest.Gender, map => map.MapFrom(src => src.Gender))
                .ForMember(dest => dest.ClassYear, map => map.MapFrom(src => src.Classyear))
                .ForMember(dest => dest.DateOfBirth, map => map.MapFrom(src => src.Dateofbirth))
                .ReverseMap();
            });
            return config.CreateMapper();
        }
    }
}
