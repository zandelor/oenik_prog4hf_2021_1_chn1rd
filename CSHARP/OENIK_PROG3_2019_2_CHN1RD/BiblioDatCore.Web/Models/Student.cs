﻿namespace BiblioDatCore.Web.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.Linq;
    using System.Threading.Tasks;

    /// <summary>
    /// Class to represent Student model objects.
    /// </summary>
    public class Student
    {
        /// <summary>
        /// Gets or sets Id of student.
        /// </summary>
        [Display(Name = "Student Id")]
        [Required]
        [Range(1, 99999)]
        public int Id { get; set; }

        /// <summary>
        /// Gets or sets first name of student.
        /// </summary>
        [Display(Name = "Student First Name")]
        [Required]
        public string FirstName { get; set; }

        /// <summary>
        /// Gets or sets last name of student.
        /// </summary>
        [Display(Name = "Student Last Name")]
        [Required]
        public string LastName { get; set; }

        /// <summary>
        /// Gets or sets gender of student.
        /// </summary>
        [Display(Name = "Student Gender")]
        [Required]
        public string Gender { get; set; }

        /// <summary>
        /// Gets or sets class year of student.
        /// </summary>
        [Range(1, 14)]
        [Display(Name = "Student Class Year")]
        [Required]
        public int ClassYear { get; set; }

        /// <summary>
        /// Gets or sets date of birth of student.
        /// </summary>
        [Display(Name = "Student Date of Birth")]
        [Range(typeof(DateTime), "1900-01-01", "2020-01-01", ErrorMessage = "Value for {0} must be between {1} and {2}")]
        [DisplayFormat(DataFormatString = "{0:yyyy/MM/dd}", ApplyFormatInEditMode = true)]
        [Required]
        public DateTime DateOfBirth { get; set; }
    }
}
