﻿namespace BiblioDatCore.Web.Models
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;

    /// <summary>
    /// Student list viewmodel class.
    /// </summary>
    public class StudentListViewModel
    {
        /// <summary>
        /// Gets or sets list of students.
        /// </summary>
        public List<Student> ListOfStudents { get; set; }

        /// <summary>
        /// Gets or sets student to be edited.
        /// </summary>
        public Student EditedStudent { get; set; }
    }
}
