﻿// This file is used by Code Analysis to maintain SuppressMessage
// attributes that are applied to this project.
// Project-level suppressions either have no target or are given
// a specific target and scoped to a namespace, type, member, etc.

using System.Diagnostics.CodeAnalysis;

[assembly: SuppressMessage("Design", "CA1001:Types that own disposable fields should be disposable", Justification = "No displosal manually set yet.", Scope = "type", Target = "~T:BiblioDatCore.WpfClient.MainLogic")]
[assembly: SuppressMessage("Performance", "CA1822:Mark members as static", Justification = "Situation does not need static.", Scope = "member", Target = "~M:BiblioDatCore.WpfClient.MainLogic.SendMessage(System.Boolean)")]
[assembly: SuppressMessage("Usage", "CA2234:Pass system uri objects instead of strings", Justification = "Theme of project required strings.", Scope = "member", Target = "~M:BiblioDatCore.WpfClient.MainLogic.ApiDelStudent(BiblioDatCore.WpfClient.StudentVM)")]
[assembly: SuppressMessage("Usage", "CA2234:Pass system uri objects instead of strings", Justification = "Theme of project required strings.", Scope = "member", Target = "~M:BiblioDatCore.WpfClient.MainLogic.ApiEditStudent(BiblioDatCore.WpfClient.StudentVM,System.Int32,System.Boolean)~System.Boolean")]
[assembly: SuppressMessage("Usage", "CA2234:Pass system uri objects instead of strings", Justification = "Theme of project required strings.", Scope = "member", Target = "~M:BiblioDatCore.WpfClient.MainLogic.ApiGetStudents~System.Collections.Generic.List{BiblioDatCore.WpfClient.StudentVM}")]
[assembly: SuppressMessage("Globalization", "CA1305:Specify IFormatProvider", Justification = "No globalization regarding current usage.", Scope = "member", Target = "~M:BiblioDatCore.WpfClient.MainLogic.ApiDelStudent(BiblioDatCore.WpfClient.StudentVM)")]
[assembly: SuppressMessage("Globalization", "CA1305:Specify IFormatProvider", Justification = "No globalization regarding current usage.", Scope = "member", Target = "~M:BiblioDatCore.WpfClient.MainLogic.ApiEditStudent(BiblioDatCore.WpfClient.StudentVM,System.Int32,System.Boolean)~System.Boolean")]
[assembly: SuppressMessage("Reliability", "CA2000:Dispose objects before losing scope", Justification = "No displosal manually set yet.", Scope = "member", Target = "~M:BiblioDatCore.WpfClient.MainLogic.ApiEditStudent(BiblioDatCore.WpfClient.StudentVM,System.Int32,System.Boolean)~System.Boolean")]
[assembly: SuppressMessage("StyleCop.CSharp.DocumentationRules", "SA1633:File should have header", Justification = "No header used throughout solution.")]
[assembly: SuppressMessage("StyleCop.CSharp.DocumentationRules", "SA1633:File should have header", Justification = "No header used throughout solution.", Scope = "namespace", Target = "~N:BiblioDatCore.WpfClient")]
[assembly: SuppressMessage("Usage", "CA2227:Collection properties should be read only", Justification = "Setter is used.", Scope = "member", Target = "~P:BiblioDatCore.WpfClient.MainVM.AllStudents")]
[assembly: SuppressMessage("Design", "CA1002:Do not expose generic lists", Justification = "List type is correct for end usage.", Scope = "member", Target = "~M:BiblioDatCore.WpfClient.IMainLogic.ApiGetStudents~System.Collections.Generic.List{BiblioDatCore.WpfClient.StudentVM}")]
