﻿namespace BiblioDatCore.WpfClient
{
    using System;
    using System.Collections.Generic;

    /// <summary>
    /// Interface describing public MainLogic methods.
    /// </summary>
    public interface IMainLogic
    {
        /// <summary>
        /// Method for deleting students through API request.
        /// </summary>
        /// <param name="student">Student to be deleted.</param>
        void ApiDelStudent(StudentVM student);

        /// <summary>
        /// Method for getting students through API request.
        /// </summary>
        /// <returns>List of students.</returns>
        List<StudentVM> ApiGetStudents();

        /// <summary>
        /// Method for editing students.
        /// </summary>
        /// <param name="student">Student to be edited.</param>
        /// <param name="editorFunc">Function to edit student with.</param>
        void EditStudent(StudentVM student, Func<StudentVM, bool> editorFunc);
    }
}