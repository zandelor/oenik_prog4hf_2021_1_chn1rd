﻿namespace BiblioDatCore.WpfClient
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Net.Http;
    using System.Text;
    using System.Text.Json;
    using System.Threading.Tasks;
    using GalaSoft.MvvmLight.Messaging;

    /// <summary>
    /// Class for defining logic layer implementation.
    /// </summary>
    public class MainLogic : IMainLogic
    {
        private string url = "http://localhost:64127/StudentsApi/";
        private HttpClient client = new HttpClient();
        private JsonSerializerOptions jsonOptions = new JsonSerializerOptions(JsonSerializerDefaults.Web);

        /// <inheritdoc/>
        public List<StudentVM> ApiGetStudents()
        {
            string json = this.client.GetStringAsync(this.url + "all").Result;
            var list = JsonSerializer.Deserialize<List<StudentVM>>(json, this.jsonOptions);
            return list;
        }

        /// <inheritdoc/>
        public void ApiDelStudent(StudentVM student)
        {
            bool success = false;
            if (student != null)
            {
                string json = this.client.GetStringAsync(this.url + "del/" + student.Id.ToString()).Result;
                JsonDocument doc = JsonDocument.Parse(json);
                success = doc.RootElement.EnumerateObject().First().Value.GetRawText() == "true";
            }

            this.SendMessage(success);
        }

        /// <inheritdoc/>
        public void EditStudent(StudentVM student, Func<StudentVM, bool> editorFunc)
        {
            StudentVM clone = new StudentVM();
            if (student != null)
            {
                clone.CopyFrom(student);
            }
            else
            {
                clone.DateOfBirth = DateTime.Now;
            }

            bool? success = editorFunc?.Invoke(clone);
            if (success == true)
            {
                if (student != null)
                {
                    success = this.ApiEditStudent(clone, student.Id, true);
                }
                else
                {
                    success = this.ApiEditStudent(clone, 0, false);
                }
            }

            this.SendMessage(success == true);
        }

        /// <summary>
        /// Method for calling API request for editing students.
        /// </summary>
        /// <param name="student">Edited student.</param>
        /// <param name="idToChange">Original ID of student.</param>
        /// <param name="isEditing">Signals whether it's an edit or add request.</param>
        /// <returns>Whether API was successful.</returns>
        private bool ApiEditStudent(StudentVM student, int idToChange, bool isEditing)
        {
            if (student == null)
            {
                return false;
            }

            string myUrl = this.url + (isEditing ? "mod" : "add");

            Dictionary<string, string> postData = new Dictionary<string, string>();
            if (isEditing)
            {
                postData.Add("idToChange", idToChange.ToString());
            }

            postData.Add("Id", student.Id.ToString());
            postData.Add("FirstName", student.FirstName);
            postData.Add("LastName", student.LastName);
            postData.Add("Gender", student.Gender);
            postData.Add("ClassYear", student.ClassYear.ToString());
            postData.Add("DateOfBirth", student.DateOfBirth.ToString());

            string json = this.client.PostAsync(myUrl, new FormUrlEncodedContent(postData))
                .Result
                .Content
                .ReadAsStringAsync().Result;
            JsonDocument doc = JsonDocument.Parse(json);
            return doc.RootElement.EnumerateObject().First().Value.GetRawText() == "true";
        }

        private void SendMessage(bool success)
        {
            string msg = success ? "Operation ok" : "Operation failed";
            Messenger.Default.Send(msg, "StudentResult");
        }
    }
}
