﻿namespace BiblioDatCore.WpfClient
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows.Input;
    using CommonServiceLocator;
    using GalaSoft.MvvmLight;
    using GalaSoft.MvvmLight.Command;

    /// <summary>
    /// Main view model for WpfClient.
    /// </summary>
    public class MainVM : ViewModelBase
    {
        private IMainLogic logic;
        private StudentVM selectedStudent;
        private ObservableCollection<StudentVM> allStudents;

        /// <summary>
        /// Initializes a new instance of the <see cref="MainVM"/> class.
        /// </summary>
        /// <param name="logic">Main logic dependency injection.</param>
        public MainVM(IMainLogic logic)
        {
            // this.logic = new MainLogic(); // todo
            this.logic = logic;

            this.LoadCmd = new RelayCommand(() => this.AllStudents = new ObservableCollection<StudentVM>(this.logic.ApiGetStudents()));
            this.DelCmd = new RelayCommand(() => this.logic.ApiDelStudent(this.SelectedStudent));
            this.ModCmd = new RelayCommand(() => this.logic.EditStudent(this.selectedStudent, this.EditorFunc));
            this.AddCmd = new RelayCommand(() => this.logic.EditStudent(null, this.EditorFunc));
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="MainVM"/> class.
        /// This contrsuctor initializes IMainLogic dependency through CommonServiceLocator.
        /// </summary>
        public MainVM()
            : this(ServiceLocator.Current.GetInstance<IMainLogic>())
        {
        }

        /// <summary>
        /// Gets or sets list of all students.
        /// </summary>
        public ObservableCollection<StudentVM> AllStudents
        {
            get => this.allStudents; set { this.Set(ref this.allStudents, value); }
        }

        /// <summary>
        /// Gets or sets the selected students.
        /// </summary>
        public StudentVM SelectedStudent
        {
            get => this.selectedStudent; set { this.Set(ref this.selectedStudent, value); }
        }

        /// <summary>
        /// Gets or sets function for editing student.
        /// </summary>
        public Func<StudentVM, bool> EditorFunc { get; set; }

        /// <summary>
        /// Gets command for adding students.
        /// </summary>
        public ICommand AddCmd { get; private set; }

        /// <summary>
        /// Gets command for modifying students.
        /// </summary>
        public ICommand ModCmd { get; private set; }

        /// <summary>
        /// Gets command for deleting students.
        /// </summary>
        public ICommand DelCmd { get; private set; }

        /// <summary>
        /// Gets command for loading database.
        /// </summary>
        public ICommand LoadCmd { get; private set; }
    }
}
