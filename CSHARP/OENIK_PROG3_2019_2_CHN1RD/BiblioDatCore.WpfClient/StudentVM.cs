﻿namespace BiblioDatCore.WpfClient
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using GalaSoft.MvvmLight;

    /// <summary>
    /// Student view model class.
    /// </summary>
    public class StudentVM : ObservableObject
    {
        private int id;
        private string firstName;
        private string lastName;
        private string gender;
        private int classYear;
        private DateTime dateOfBirth;

        /// <summary>
        /// Gets or sets ID of student.
        /// </summary>
        public int Id { get => this.id; set => this.Set(ref this.id, value); }

        /// <summary>
        /// Gets or sets first name of student.
        /// </summary>
        public string FirstName { get => this.firstName; set => this.Set(ref this.firstName, value); }

        /// <summary>
        /// Gets or sets last name of student.
        /// </summary>
        public string LastName { get => this.lastName; set => this.Set(ref this.lastName, value); }

        /// <summary>
        /// Gets or sets gender of student.
        /// </summary>
        public string Gender { get => this.gender; set => this.Set(ref this.gender, value); }

        /// <summary>
        /// Gets or sets class year of student.
        /// </summary>
        public int ClassYear { get => this.classYear; set => this.Set(ref this.classYear, value); }

        /// <summary>
        /// Gets or sets date of birth of student.
        /// </summary>
        public DateTime DateOfBirth { get => this.dateOfBirth; set => this.Set(ref this.dateOfBirth, value); }

        /// <summary>
        /// Copies from other StudentVM object to this one.
        /// </summary>
        /// <param name="student">Student to copy from.</param>
        public void CopyFrom(StudentVM student)
        {
            if (student == null)
            {
                throw new ArgumentNullException(nameof(student));
            }

            this.Id = student.Id;
            this.FirstName = student.FirstName;
            this.LastName = student.LastName;
            this.Gender = student.Gender;
            this.ClassYear = student.ClassYear;
            this.DateOfBirth = student.DateOfBirth;
        }
    }
}
