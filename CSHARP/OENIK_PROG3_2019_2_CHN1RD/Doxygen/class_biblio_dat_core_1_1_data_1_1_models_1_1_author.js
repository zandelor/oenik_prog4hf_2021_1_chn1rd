var class_biblio_dat_core_1_1_data_1_1_models_1_1_author =
[
    [ "Author", "class_biblio_dat_core_1_1_data_1_1_models_1_1_author.html#a67af31d057dca09cf4e5de460ae4c47a", null ],
    [ "Authorid", "class_biblio_dat_core_1_1_data_1_1_models_1_1_author.html#ad05b13b76e25da419aa842b176cf63ca", null ],
    [ "Books", "class_biblio_dat_core_1_1_data_1_1_models_1_1_author.html#af709528282dcad3717a70954126852a4", null ],
    [ "Dateofbirth", "class_biblio_dat_core_1_1_data_1_1_models_1_1_author.html#a5d9e1d615918d9cd58cc1730d25bfed7", null ],
    [ "Dateofdeath", "class_biblio_dat_core_1_1_data_1_1_models_1_1_author.html#ae5793b0571a5263942f62f3546601318", null ],
    [ "Firstname", "class_biblio_dat_core_1_1_data_1_1_models_1_1_author.html#a259fafe3d8f2462f149ab9e4aa34768f", null ],
    [ "Gender", "class_biblio_dat_core_1_1_data_1_1_models_1_1_author.html#a32b3a2596907b48816fa326c6b6b8a28", null ],
    [ "Lastname", "class_biblio_dat_core_1_1_data_1_1_models_1_1_author.html#af4852170d42aaa066faf1cd60fb4ffe1", null ]
];