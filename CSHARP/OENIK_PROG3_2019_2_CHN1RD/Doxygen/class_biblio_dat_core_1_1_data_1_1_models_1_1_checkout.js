var class_biblio_dat_core_1_1_data_1_1_models_1_1_checkout =
[
    [ "Book", "class_biblio_dat_core_1_1_data_1_1_models_1_1_checkout.html#a6147cfdc1d49f750f37840db6060c9fd", null ],
    [ "Bookid", "class_biblio_dat_core_1_1_data_1_1_models_1_1_checkout.html#a980fdb42ac8a06b06fe652286c0cbb7f", null ],
    [ "Borrowdate", "class_biblio_dat_core_1_1_data_1_1_models_1_1_checkout.html#a5a47407ba9724831a70f65dc1b51eebb", null ],
    [ "Checkoutid", "class_biblio_dat_core_1_1_data_1_1_models_1_1_checkout.html#a124094fdcca294306ae615258cf11957", null ],
    [ "Returndate", "class_biblio_dat_core_1_1_data_1_1_models_1_1_checkout.html#a3703409fda8f35c301769acb91e709b7", null ],
    [ "Student", "class_biblio_dat_core_1_1_data_1_1_models_1_1_checkout.html#a73a1764f78ad639a4c830509735d9561", null ],
    [ "Studentid", "class_biblio_dat_core_1_1_data_1_1_models_1_1_checkout.html#aaa78f782126abf5367ffcc05d920cc93", null ]
];