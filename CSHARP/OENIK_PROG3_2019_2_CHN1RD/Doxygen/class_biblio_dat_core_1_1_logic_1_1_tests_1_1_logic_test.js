var class_biblio_dat_core_1_1_logic_1_1_tests_1_1_logic_test =
[
    [ "Init", "class_biblio_dat_core_1_1_logic_1_1_tests_1_1_logic_test.html#a8ba43c0b54e90937e1093c3bdb471f5e", null ],
    [ "TestAvgDayOfBorrowsByAuthors", "class_biblio_dat_core_1_1_logic_1_1_tests_1_1_logic_test.html#acc9c483add131b493a171a6288f921ee", null ],
    [ "TestBooksNeverBorrowed", "class_biblio_dat_core_1_1_logic_1_1_tests_1_1_logic_test.html#a3847e8b93b02fd977f67eabd6b42b31e", null ],
    [ "TestBorrowsByGenres", "class_biblio_dat_core_1_1_logic_1_1_tests_1_1_logic_test.html#a19798c4000f297e825465bcd7b87419a", null ],
    [ "TestChangeAuthorWithNull", "class_biblio_dat_core_1_1_logic_1_1_tests_1_1_logic_test.html#afe512e5f6e6735469338850cc36562e9", null ],
    [ "TestChangeBookThrowsIdAlreadyInCollectionException", "class_biblio_dat_core_1_1_logic_1_1_tests_1_1_logic_test.html#a407d79e8db49d225faa03257c3968a19", null ],
    [ "TestChangeCheckout", "class_biblio_dat_core_1_1_logic_1_1_tests_1_1_logic_test.html#acaa3032c04ae550c6d4cc3bda1ad1bc4", null ],
    [ "TestChangeCheckoutThrowsIdNotInCollectionException", "class_biblio_dat_core_1_1_logic_1_1_tests_1_1_logic_test.html#a09993e4ab31cfdde1a7dbf467943fcfa", null ],
    [ "TestGetOneAuthor", "class_biblio_dat_core_1_1_logic_1_1_tests_1_1_logic_test.html#a6ad4815c7baa24f9a0bb23fc385fd93e", null ],
    [ "TestGetOneBookWithException", "class_biblio_dat_core_1_1_logic_1_1_tests_1_1_logic_test.html#a8bae6078b40cf750f853758aebd41df0", null ],
    [ "TestInsertAuthorThrowsIdAlreadyInCollectionException", "class_biblio_dat_core_1_1_logic_1_1_tests_1_1_logic_test.html#a021432992393ee677257f7bc02a1e850", null ],
    [ "TestInsertStudent", "class_biblio_dat_core_1_1_logic_1_1_tests_1_1_logic_test.html#a1ee7b8b6de133653c7bb0278ae7ce55d", null ],
    [ "TestListAllBook", "class_biblio_dat_core_1_1_logic_1_1_tests_1_1_logic_test.html#add2ec996e3f25813f8d2e5cda7a46e11", null ],
    [ "TestRemoveAuthor", "class_biblio_dat_core_1_1_logic_1_1_tests_1_1_logic_test.html#a54f6470b89635c04c8405479291c1ff2", null ],
    [ "TestRemoveCheckout", "class_biblio_dat_core_1_1_logic_1_1_tests_1_1_logic_test.html#ac383ddd68754553693fe3144578a520b", null ]
];