var class_biblio_dat_core_1_1_u_i_1_1_v_m_1_1_main_view_model =
[
    [ "MainViewModel", "class_biblio_dat_core_1_1_u_i_1_1_v_m_1_1_main_view_model.html#a782c8232886e8d95c63f7a54cf8b2469", null ],
    [ "MainViewModel", "class_biblio_dat_core_1_1_u_i_1_1_v_m_1_1_main_view_model.html#a791149a73296832396d56768a9bf0898", null ],
    [ "AddCommand", "class_biblio_dat_core_1_1_u_i_1_1_v_m_1_1_main_view_model.html#a6dc277aa2b89a80b67278a3235dc8d55", null ],
    [ "DeleteCommand", "class_biblio_dat_core_1_1_u_i_1_1_v_m_1_1_main_view_model.html#ab6897e36e40acaed355105f26ca83d20", null ],
    [ "ModifyCommand", "class_biblio_dat_core_1_1_u_i_1_1_v_m_1_1_main_view_model.html#a8e54c799d1bd7663ea3db3d44459b811", null ],
    [ "Students", "class_biblio_dat_core_1_1_u_i_1_1_v_m_1_1_main_view_model.html#ad2bb4705b8dc8e68eba0e9454fcb4b64", null ],
    [ "StudentSelected", "class_biblio_dat_core_1_1_u_i_1_1_v_m_1_1_main_view_model.html#a224f6bc894256a7ab9736267e135d88b", null ]
];