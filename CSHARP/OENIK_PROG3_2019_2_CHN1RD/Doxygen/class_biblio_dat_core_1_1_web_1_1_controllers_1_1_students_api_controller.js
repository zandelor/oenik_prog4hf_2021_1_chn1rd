var class_biblio_dat_core_1_1_web_1_1_controllers_1_1_students_api_controller =
[
    [ "StudentsApiController", "class_biblio_dat_core_1_1_web_1_1_controllers_1_1_students_api_controller.html#ad50579799240ad8dddac6a05d896501c", null ],
    [ "AddOneStudent", "class_biblio_dat_core_1_1_web_1_1_controllers_1_1_students_api_controller.html#ae5569c4a808bd037ef3e149f115d87a6", null ],
    [ "DelOneStudent", "class_biblio_dat_core_1_1_web_1_1_controllers_1_1_students_api_controller.html#a52582bb84966b8043322a1d6cc337309", null ],
    [ "GetAll", "class_biblio_dat_core_1_1_web_1_1_controllers_1_1_students_api_controller.html#acd3d682ebf8c2a9aa6bc784995223429", null ],
    [ "ModOneStudent", "class_biblio_dat_core_1_1_web_1_1_controllers_1_1_students_api_controller.html#aa08d229618a1cda3745844fac86b269f", null ]
];