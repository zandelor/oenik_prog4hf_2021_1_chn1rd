var class_biblio_dat_core_1_1_wpf_client_1_1_main_v_m =
[
    [ "MainVM", "class_biblio_dat_core_1_1_wpf_client_1_1_main_v_m.html#abe251f130f67442afb58ca16c82e586e", null ],
    [ "MainVM", "class_biblio_dat_core_1_1_wpf_client_1_1_main_v_m.html#ac8b8174ef85af885c6921cf03379c726", null ],
    [ "AddCmd", "class_biblio_dat_core_1_1_wpf_client_1_1_main_v_m.html#a36468aa80915f1858b1776a64b987664", null ],
    [ "AllStudents", "class_biblio_dat_core_1_1_wpf_client_1_1_main_v_m.html#ab99dc5ece909aff2ffa22339164e1a2f", null ],
    [ "DelCmd", "class_biblio_dat_core_1_1_wpf_client_1_1_main_v_m.html#a962c8fd07d835abd1029bf5e7eb05e77", null ],
    [ "EditorFunc", "class_biblio_dat_core_1_1_wpf_client_1_1_main_v_m.html#a5b2b43c23ef1e6c04088808c0605d912", null ],
    [ "LoadCmd", "class_biblio_dat_core_1_1_wpf_client_1_1_main_v_m.html#ae4e13852061a5cd81812ae4beb0a7c73", null ],
    [ "ModCmd", "class_biblio_dat_core_1_1_wpf_client_1_1_main_v_m.html#aecf8c1c87a229ddd4278f3d121364321", null ],
    [ "SelectedStudent", "class_biblio_dat_core_1_1_wpf_client_1_1_main_v_m.html#a20ca127affd372d2d02cce57138a39bc", null ]
];