var dir_14aa5b9bb0a9acfc81276f1d2c0e66e7 =
[
    [ "BiblioDatCore.Data", "dir_011c7a4b978f1562cd4e923938e03006.html", "dir_011c7a4b978f1562cd4e923938e03006" ],
    [ "BiblioDatCore.Logic", "dir_d4d7da3cae6bfadb78900d640a6c961d.html", "dir_d4d7da3cae6bfadb78900d640a6c961d" ],
    [ "BiblioDatCore.Logic.Tests", "dir_c032decdd8c39dc58973c88e8b756d8b.html", "dir_c032decdd8c39dc58973c88e8b756d8b" ],
    [ "BiblioDatCore.Program", "dir_98bc2166b1e4617fd432fb70e4fcda51.html", "dir_98bc2166b1e4617fd432fb70e4fcda51" ],
    [ "BiblioDatCore.Repository", "dir_b19ac75ba9672fee3ca2f340d213b99f.html", "dir_b19ac75ba9672fee3ca2f340d213b99f" ],
    [ "BiblioDatCore.UI", "dir_718bc890ba29b7dfb5dac134bb69fa65.html", "dir_718bc890ba29b7dfb5dac134bb69fa65" ],
    [ "BiblioDatCore.Web", "dir_7ec21f967a019be4b6800814512a7da9.html", "dir_7ec21f967a019be4b6800814512a7da9" ],
    [ "BiblioDatCore.WpfClient", "dir_8e81e162539a6c1d17ffb586e3d49679.html", "dir_8e81e162539a6c1d17ffb586e3d49679" ]
];