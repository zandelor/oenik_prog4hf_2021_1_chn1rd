var dir_d4d7da3cae6bfadb78900d640a6c961d =
[
    [ "obj", "dir_da50533c775e12249b8a7a9f50a4c624.html", "dir_da50533c775e12249b8a7a9f50a4c624" ],
    [ "AverageDaysOfBorrows.cs", "_average_days_of_borrows_8cs_source.html", null ],
    [ "GenresAndNumberOfBorrows.cs", "_genres_and_number_of_borrows_8cs_source.html", null ],
    [ "GlobalSuppressions.cs", "_biblio_dat_core_8_logic_2_global_suppressions_8cs_source.html", null ],
    [ "IdAlreadyInCollectionException.cs", "_id_already_in_collection_exception_8cs_source.html", null ],
    [ "IdNotInCollectionException.cs", "_id_not_in_collection_exception_8cs_source.html", null ],
    [ "ILogic.cs", "_i_logic_8cs_source.html", null ],
    [ "LogicClass.cs", "_logic_class_8cs_source.html", null ],
    [ "Person.cs", "_person_8cs_source.html", null ],
    [ "VoucherClass.cs", "_voucher_class_8cs_source.html", null ]
];