var interface_biblio_dat_core_1_1_repository_1_1_i_repository =
[
    [ "CreateAuthor", "interface_biblio_dat_core_1_1_repository_1_1_i_repository.html#af34ed9e7f23ba995172095d1b814161d", null ],
    [ "CreateBook", "interface_biblio_dat_core_1_1_repository_1_1_i_repository.html#a44f1e5232230f20851aa0d273ddae764", null ],
    [ "CreateCheckout", "interface_biblio_dat_core_1_1_repository_1_1_i_repository.html#a0fd536b86bcf75ad1602af57b8da31db", null ],
    [ "CreateStudent", "interface_biblio_dat_core_1_1_repository_1_1_i_repository.html#ac71dd186b78d36e727226d87b6c1f4a4", null ],
    [ "DeleteAuthor", "interface_biblio_dat_core_1_1_repository_1_1_i_repository.html#ab45ec4dc49696c8138111ba614d0e792", null ],
    [ "DeleteBook", "interface_biblio_dat_core_1_1_repository_1_1_i_repository.html#a345cbabe6b38d16d4a8b23a3a5784afa", null ],
    [ "DeleteCheckout", "interface_biblio_dat_core_1_1_repository_1_1_i_repository.html#a16cb2cf01bfbf9cb7041c0f2b28264bf", null ],
    [ "DeleteStudent", "interface_biblio_dat_core_1_1_repository_1_1_i_repository.html#ac6936a469badbfe21f9f7bee3a590042", null ],
    [ "GetOneAuthor", "interface_biblio_dat_core_1_1_repository_1_1_i_repository.html#ac9a5c334423734db59113b21b35e97d2", null ],
    [ "GetOneBook", "interface_biblio_dat_core_1_1_repository_1_1_i_repository.html#aaa6010c15a984e76715fd5e750a60bd1", null ],
    [ "GetOneCheckout", "interface_biblio_dat_core_1_1_repository_1_1_i_repository.html#a810ba95a4d6cff68d9e6046cbea6e98a", null ],
    [ "GetOneStudent", "interface_biblio_dat_core_1_1_repository_1_1_i_repository.html#a73e492381b47cb79175c765720030cce", null ],
    [ "ReadAllAuthor", "interface_biblio_dat_core_1_1_repository_1_1_i_repository.html#a4ffad30027056e55bde9db7bb5b3a6f5", null ],
    [ "ReadAllBook", "interface_biblio_dat_core_1_1_repository_1_1_i_repository.html#aa85282d664b5c144fd570fc0cc0c9da2", null ],
    [ "ReadAllCheckout", "interface_biblio_dat_core_1_1_repository_1_1_i_repository.html#a7aae9f243cfa6f74740e678ed6877fa2", null ],
    [ "ReadAllStudent", "interface_biblio_dat_core_1_1_repository_1_1_i_repository.html#a8127bb77fd875708f71b9e0d8529588c", null ],
    [ "UpdateAuthor", "interface_biblio_dat_core_1_1_repository_1_1_i_repository.html#a674fa177fdba621c99877806031e3f72", null ],
    [ "UpdateBook", "interface_biblio_dat_core_1_1_repository_1_1_i_repository.html#a43dc27743775da47eedbbd5339569de1", null ],
    [ "UpdateCheckout", "interface_biblio_dat_core_1_1_repository_1_1_i_repository.html#abd00dfd617f99b9bc6de6c2c40323853", null ],
    [ "UpdateStudent", "interface_biblio_dat_core_1_1_repository_1_1_i_repository.html#a5cfffe91bfa1504b59434ea3c64785cf", null ]
];