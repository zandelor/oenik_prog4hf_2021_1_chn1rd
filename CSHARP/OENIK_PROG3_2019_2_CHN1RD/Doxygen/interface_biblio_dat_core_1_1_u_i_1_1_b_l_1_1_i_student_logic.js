var interface_biblio_dat_core_1_1_u_i_1_1_b_l_1_1_i_student_logic =
[
    [ "AddStudent", "interface_biblio_dat_core_1_1_u_i_1_1_b_l_1_1_i_student_logic.html#ac60e07aaf9ab153cc45af2b6164a8242", null ],
    [ "DelStudent", "interface_biblio_dat_core_1_1_u_i_1_1_b_l_1_1_i_student_logic.html#aeb325e43b24c07a6908c9f24f0684af3", null ],
    [ "GetAllStudentsFromDatabase", "interface_biblio_dat_core_1_1_u_i_1_1_b_l_1_1_i_student_logic.html#a014c47fdd977ea83734df921304f7f2a", null ],
    [ "ModStudent", "interface_biblio_dat_core_1_1_u_i_1_1_b_l_1_1_i_student_logic.html#abb395ef916db07ea98ebdc9c9e267d4a", null ]
];