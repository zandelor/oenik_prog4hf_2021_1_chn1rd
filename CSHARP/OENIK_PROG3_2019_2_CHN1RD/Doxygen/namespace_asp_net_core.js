var namespace_asp_net_core =
[
    [ "Views__ViewImports", "class_asp_net_core_1_1_views_____view_imports.html", "class_asp_net_core_1_1_views_____view_imports" ],
    [ "Views__ViewStart", "class_asp_net_core_1_1_views_____view_start.html", "class_asp_net_core_1_1_views_____view_start" ],
    [ "Views_Home_Index", "class_asp_net_core_1_1_views___home___index.html", "class_asp_net_core_1_1_views___home___index" ],
    [ "Views_Home_Privacy", "class_asp_net_core_1_1_views___home___privacy.html", "class_asp_net_core_1_1_views___home___privacy" ],
    [ "Views_Shared__Layout", "class_asp_net_core_1_1_views___shared_____layout.html", "class_asp_net_core_1_1_views___shared_____layout" ],
    [ "Views_Shared__ValidationScriptsPartial", "class_asp_net_core_1_1_views___shared_____validation_scripts_partial.html", "class_asp_net_core_1_1_views___shared_____validation_scripts_partial" ],
    [ "Views_Shared_Error", "class_asp_net_core_1_1_views___shared___error.html", "class_asp_net_core_1_1_views___shared___error" ],
    [ "Views_Students_StudentsDetails", "class_asp_net_core_1_1_views___students___students_details.html", "class_asp_net_core_1_1_views___students___students_details" ],
    [ "Views_Students_StudentsEdit", "class_asp_net_core_1_1_views___students___students_edit.html", "class_asp_net_core_1_1_views___students___students_edit" ],
    [ "Views_Students_StudentsIndex", "class_asp_net_core_1_1_views___students___students_index.html", "class_asp_net_core_1_1_views___students___students_index" ],
    [ "Views_Students_StudentsList", "class_asp_net_core_1_1_views___students___students_list.html", "class_asp_net_core_1_1_views___students___students_list" ]
];