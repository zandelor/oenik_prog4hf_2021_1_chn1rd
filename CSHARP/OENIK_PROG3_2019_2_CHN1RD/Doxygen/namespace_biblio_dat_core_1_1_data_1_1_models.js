var namespace_biblio_dat_core_1_1_data_1_1_models =
[
    [ "Author", "class_biblio_dat_core_1_1_data_1_1_models_1_1_author.html", "class_biblio_dat_core_1_1_data_1_1_models_1_1_author" ],
    [ "BDBContext", "class_biblio_dat_core_1_1_data_1_1_models_1_1_b_d_b_context.html", "class_biblio_dat_core_1_1_data_1_1_models_1_1_b_d_b_context" ],
    [ "Book", "class_biblio_dat_core_1_1_data_1_1_models_1_1_book.html", "class_biblio_dat_core_1_1_data_1_1_models_1_1_book" ],
    [ "Checkout", "class_biblio_dat_core_1_1_data_1_1_models_1_1_checkout.html", "class_biblio_dat_core_1_1_data_1_1_models_1_1_checkout" ],
    [ "Student", "class_biblio_dat_core_1_1_data_1_1_models_1_1_student.html", "class_biblio_dat_core_1_1_data_1_1_models_1_1_student" ]
];