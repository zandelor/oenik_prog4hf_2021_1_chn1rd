var namespace_biblio_dat_core_1_1_logic =
[
    [ "Tests", "namespace_biblio_dat_core_1_1_logic_1_1_tests.html", "namespace_biblio_dat_core_1_1_logic_1_1_tests" ],
    [ "AverageDaysOfBorrows", "class_biblio_dat_core_1_1_logic_1_1_average_days_of_borrows.html", "class_biblio_dat_core_1_1_logic_1_1_average_days_of_borrows" ],
    [ "GenresAndNumberOfBorrows", "class_biblio_dat_core_1_1_logic_1_1_genres_and_number_of_borrows.html", "class_biblio_dat_core_1_1_logic_1_1_genres_and_number_of_borrows" ],
    [ "IdAlreadyInCollectionException", "class_biblio_dat_core_1_1_logic_1_1_id_already_in_collection_exception.html", "class_biblio_dat_core_1_1_logic_1_1_id_already_in_collection_exception" ],
    [ "IdNotInCollectionException", "class_biblio_dat_core_1_1_logic_1_1_id_not_in_collection_exception.html", "class_biblio_dat_core_1_1_logic_1_1_id_not_in_collection_exception" ],
    [ "ILogic", "interface_biblio_dat_core_1_1_logic_1_1_i_logic.html", "interface_biblio_dat_core_1_1_logic_1_1_i_logic" ],
    [ "LogicClass", "class_biblio_dat_core_1_1_logic_1_1_logic_class.html", "class_biblio_dat_core_1_1_logic_1_1_logic_class" ],
    [ "Person", "class_biblio_dat_core_1_1_logic_1_1_person.html", "class_biblio_dat_core_1_1_logic_1_1_person" ],
    [ "VoucherClass", "class_biblio_dat_core_1_1_logic_1_1_voucher_class.html", "class_biblio_dat_core_1_1_logic_1_1_voucher_class" ]
];