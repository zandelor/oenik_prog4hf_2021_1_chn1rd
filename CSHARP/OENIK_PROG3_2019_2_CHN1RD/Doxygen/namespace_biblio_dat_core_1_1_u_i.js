var namespace_biblio_dat_core_1_1_u_i =
[
    [ "BL", "namespace_biblio_dat_core_1_1_u_i_1_1_b_l.html", "namespace_biblio_dat_core_1_1_u_i_1_1_b_l" ],
    [ "Data", "namespace_biblio_dat_core_1_1_u_i_1_1_data.html", "namespace_biblio_dat_core_1_1_u_i_1_1_data" ],
    [ "UserInterface", "namespace_biblio_dat_core_1_1_u_i_1_1_user_interface.html", "namespace_biblio_dat_core_1_1_u_i_1_1_user_interface" ],
    [ "VM", "namespace_biblio_dat_core_1_1_u_i_1_1_v_m.html", "namespace_biblio_dat_core_1_1_u_i_1_1_v_m" ],
    [ "App", "class_biblio_dat_core_1_1_u_i_1_1_app.html", "class_biblio_dat_core_1_1_u_i_1_1_app" ],
    [ "MainWindow", "class_biblio_dat_core_1_1_u_i_1_1_main_window.html", "class_biblio_dat_core_1_1_u_i_1_1_main_window" ],
    [ "MyIoc", "class_biblio_dat_core_1_1_u_i_1_1_my_ioc.html", "class_biblio_dat_core_1_1_u_i_1_1_my_ioc" ]
];