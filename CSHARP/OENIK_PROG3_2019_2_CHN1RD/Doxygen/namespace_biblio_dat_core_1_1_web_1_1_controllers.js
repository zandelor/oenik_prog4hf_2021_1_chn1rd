var namespace_biblio_dat_core_1_1_web_1_1_controllers =
[
    [ "ApiResult", "class_biblio_dat_core_1_1_web_1_1_controllers_1_1_api_result.html", "class_biblio_dat_core_1_1_web_1_1_controllers_1_1_api_result" ],
    [ "HomeController", "class_biblio_dat_core_1_1_web_1_1_controllers_1_1_home_controller.html", "class_biblio_dat_core_1_1_web_1_1_controllers_1_1_home_controller" ],
    [ "StudentsApiController", "class_biblio_dat_core_1_1_web_1_1_controllers_1_1_students_api_controller.html", "class_biblio_dat_core_1_1_web_1_1_controllers_1_1_students_api_controller" ],
    [ "StudentsController", "class_biblio_dat_core_1_1_web_1_1_controllers_1_1_students_controller.html", "class_biblio_dat_core_1_1_web_1_1_controllers_1_1_students_controller" ]
];