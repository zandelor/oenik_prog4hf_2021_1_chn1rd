var namespace_biblio_dat_core_1_1_web_1_1_models =
[
    [ "ErrorViewModel", "class_biblio_dat_core_1_1_web_1_1_models_1_1_error_view_model.html", "class_biblio_dat_core_1_1_web_1_1_models_1_1_error_view_model" ],
    [ "MapperFactory", "class_biblio_dat_core_1_1_web_1_1_models_1_1_mapper_factory.html", "class_biblio_dat_core_1_1_web_1_1_models_1_1_mapper_factory" ],
    [ "Student", "class_biblio_dat_core_1_1_web_1_1_models_1_1_student.html", "class_biblio_dat_core_1_1_web_1_1_models_1_1_student" ],
    [ "StudentListViewModel", "class_biblio_dat_core_1_1_web_1_1_models_1_1_student_list_view_model.html", "class_biblio_dat_core_1_1_web_1_1_models_1_1_student_list_view_model" ]
];