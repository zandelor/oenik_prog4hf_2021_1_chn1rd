var namespace_biblio_dat_core_1_1_wpf_client =
[
    [ "App", "class_biblio_dat_core_1_1_wpf_client_1_1_app.html", "class_biblio_dat_core_1_1_wpf_client_1_1_app" ],
    [ "Editor", "class_biblio_dat_core_1_1_wpf_client_1_1_editor.html", "class_biblio_dat_core_1_1_wpf_client_1_1_editor" ],
    [ "IMainLogic", "interface_biblio_dat_core_1_1_wpf_client_1_1_i_main_logic.html", "interface_biblio_dat_core_1_1_wpf_client_1_1_i_main_logic" ],
    [ "MainLogic", "class_biblio_dat_core_1_1_wpf_client_1_1_main_logic.html", "class_biblio_dat_core_1_1_wpf_client_1_1_main_logic" ],
    [ "MainVM", "class_biblio_dat_core_1_1_wpf_client_1_1_main_v_m.html", "class_biblio_dat_core_1_1_wpf_client_1_1_main_v_m" ],
    [ "MainWindow", "class_biblio_dat_core_1_1_wpf_client_1_1_main_window.html", "class_biblio_dat_core_1_1_wpf_client_1_1_main_window" ],
    [ "MyIoc", "class_biblio_dat_core_1_1_wpf_client_1_1_my_ioc.html", "class_biblio_dat_core_1_1_wpf_client_1_1_my_ioc" ],
    [ "StudentVM", "class_biblio_dat_core_1_1_wpf_client_1_1_student_v_m.html", "class_biblio_dat_core_1_1_wpf_client_1_1_student_v_m" ]
];