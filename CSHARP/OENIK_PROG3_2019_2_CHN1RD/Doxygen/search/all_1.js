var searchData=
[
  ['bdbcontext_18',['BDBContext',['../class_biblio_dat_core_1_1_data_1_1_models_1_1_b_d_b_context.html#a8d6bb1fd5cce2b3a3e6a64e481992102',1,'BiblioDatCore.Data.Models.BDBContext.BDBContext()'],['../class_biblio_dat_core_1_1_data_1_1_models_1_1_b_d_b_context.html#afade4e38ae371b62acb184c902f477e5',1,'BiblioDatCore.Data.Models.BDBContext.BDBContext(DbContextOptions&lt; BDBContext &gt; options)'],['../class_biblio_dat_core_1_1_data_1_1_models_1_1_b_d_b_context.html',1,'BiblioDatCore.Data.Models.BDBContext']]],
  ['bibliodatcore_19',['BiblioDatCore',['../namespace_biblio_dat_core.html',1,'']]],
  ['bl_20',['BL',['../namespace_biblio_dat_core_1_1_u_i_1_1_b_l.html',1,'BiblioDatCore::UI']]],
  ['book_21',['Book',['../class_biblio_dat_core_1_1_data_1_1_models_1_1_book.html',1,'BiblioDatCore.Data.Models.Book'],['../class_biblio_dat_core_1_1_data_1_1_models_1_1_checkout.html#a6147cfdc1d49f750f37840db6060c9fd',1,'BiblioDatCore.Data.Models.Checkout.Book()'],['../class_biblio_dat_core_1_1_data_1_1_models_1_1_book.html#a2f9f9f320e1bd2b772d557c8b894e585',1,'BiblioDatCore.Data.Models.Book.Book()']]],
  ['bookid_22',['Bookid',['../class_biblio_dat_core_1_1_data_1_1_models_1_1_book.html#af22c9563194775c9693b49e3f9637966',1,'BiblioDatCore.Data.Models.Book.Bookid()'],['../class_biblio_dat_core_1_1_data_1_1_models_1_1_checkout.html#a980fdb42ac8a06b06fe652286c0cbb7f',1,'BiblioDatCore.Data.Models.Checkout.Bookid()']]],
  ['books_23',['Books',['../class_biblio_dat_core_1_1_data_1_1_models_1_1_author.html#af709528282dcad3717a70954126852a4',1,'BiblioDatCore.Data.Models.Author.Books()'],['../class_biblio_dat_core_1_1_data_1_1_models_1_1_b_d_b_context.html#abeec993af3e6db4ade86db4e714551d8',1,'BiblioDatCore.Data.Models.BDBContext.Books()']]],
  ['booksneverborrowed_24',['BooksNeverBorrowed',['../interface_biblio_dat_core_1_1_logic_1_1_i_logic.html#a2c23899734f5c1f234f5ac296edbdd03',1,'BiblioDatCore.Logic.ILogic.BooksNeverBorrowed()'],['../class_biblio_dat_core_1_1_logic_1_1_logic_class.html#ac0bdaa9070664fd1c2411dab19eceef0',1,'BiblioDatCore.Logic.LogicClass.BooksNeverBorrowed()']]],
  ['borrowdate_25',['Borrowdate',['../class_biblio_dat_core_1_1_data_1_1_models_1_1_checkout.html#a5a47407ba9724831a70f65dc1b51eebb',1,'BiblioDatCore::Data::Models::Checkout']]],
  ['borrowsbygenres_26',['BorrowsByGenres',['../interface_biblio_dat_core_1_1_logic_1_1_i_logic.html#af0a184d55804212548149cac58bfa1e3',1,'BiblioDatCore.Logic.ILogic.BorrowsByGenres()'],['../class_biblio_dat_core_1_1_logic_1_1_logic_class.html#a3057680c84174a3ac35776e74df3fcb2',1,'BiblioDatCore.Logic.LogicClass.BorrowsByGenres()']]],
  ['controllers_27',['Controllers',['../namespace_biblio_dat_core_1_1_web_1_1_controllers.html',1,'BiblioDatCore::Web']]],
  ['data_28',['Data',['../namespace_biblio_dat_core_1_1_data.html',1,'BiblioDatCore.Data'],['../namespace_biblio_dat_core_1_1_u_i_1_1_data.html',1,'BiblioDatCore.UI.Data']]],
  ['logic_29',['Logic',['../namespace_biblio_dat_core_1_1_logic.html',1,'BiblioDatCore']]],
  ['models_30',['Models',['../namespace_biblio_dat_core_1_1_data_1_1_models.html',1,'BiblioDatCore.Data.Models'],['../namespace_biblio_dat_core_1_1_web_1_1_models.html',1,'BiblioDatCore.Web.Models']]],
  ['program_31',['Program',['../namespace_biblio_dat_core_1_1_program.html',1,'BiblioDatCore']]],
  ['repository_32',['Repository',['../namespace_biblio_dat_core_1_1_repository.html',1,'BiblioDatCore']]],
  ['tests_33',['Tests',['../namespace_biblio_dat_core_1_1_logic_1_1_tests.html',1,'BiblioDatCore::Logic']]],
  ['ui_34',['UI',['../namespace_biblio_dat_core_1_1_u_i.html',1,'BiblioDatCore']]],
  ['userinterface_35',['UserInterface',['../namespace_biblio_dat_core_1_1_u_i_1_1_user_interface.html',1,'BiblioDatCore::UI']]],
  ['vm_36',['VM',['../namespace_biblio_dat_core_1_1_u_i_1_1_v_m.html',1,'BiblioDatCore::UI']]],
  ['web_37',['Web',['../namespace_biblio_dat_core_1_1_web.html',1,'BiblioDatCore']]],
  ['wpfclient_38',['WpfClient',['../namespace_biblio_dat_core_1_1_wpf_client.html',1,'BiblioDatCore']]]
];
