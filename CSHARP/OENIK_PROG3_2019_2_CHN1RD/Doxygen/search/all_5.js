var searchData=
[
  ['firstname_88',['FirstName',['../class_biblio_dat_core_1_1_u_i_1_1_data_1_1_student_w_p_f.html#ac9e5effe88e790745437fc6d7d9c7580',1,'BiblioDatCore.UI.Data.StudentWPF.FirstName()'],['../class_biblio_dat_core_1_1_web_1_1_models_1_1_student.html#ad5020cca496192d0889b535d3e5a604f',1,'BiblioDatCore.Web.Models.Student.FirstName()'],['../class_biblio_dat_core_1_1_wpf_client_1_1_student_v_m.html#a561a84c55d2fc5b2ab49c3eed71345e0',1,'BiblioDatCore.WpfClient.StudentVM.FirstName()']]],
  ['firstname_89',['Firstname',['../class_biblio_dat_core_1_1_data_1_1_models_1_1_author.html#a259fafe3d8f2462f149ab9e4aa34768f',1,'BiblioDatCore.Data.Models.Author.Firstname()'],['../class_biblio_dat_core_1_1_data_1_1_models_1_1_student.html#aa5255f6ed282103cc324240cb7ed824d',1,'BiblioDatCore.Data.Models.Student.Firstname()']]],
  ['five_90',['Five',['../namespace_biblio_dat_core_1_1_u_i_1_1_data.html#a85c49e1ce826c06cd5bd02159be31a83ae5d9de39f7ca1ba2637e5640af3ae8aa',1,'BiblioDatCore::UI::Data']]],
  ['four_91',['Four',['../namespace_biblio_dat_core_1_1_u_i_1_1_data.html#a85c49e1ce826c06cd5bd02159be31a83a981b8fcee42e1e726a67a2b9a98ea6e9',1,'BiblioDatCore::UI::Data']]],
  ['fourteen_92',['Fourteen',['../namespace_biblio_dat_core_1_1_u_i_1_1_data.html#a85c49e1ce826c06cd5bd02159be31a83aa59cd6b51b232dee123c6b1dd2c7e80b',1,'BiblioDatCore::UI::Data']]]
];
