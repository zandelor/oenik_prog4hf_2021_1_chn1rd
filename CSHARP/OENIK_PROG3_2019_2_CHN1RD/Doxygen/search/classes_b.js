var searchData=
[
  ['startup_261',['Startup',['../class_biblio_dat_core_1_1_web_1_1_startup.html',1,'BiblioDatCore::Web']]],
  ['student_262',['Student',['../class_biblio_dat_core_1_1_data_1_1_models_1_1_student.html',1,'BiblioDatCore.Data.Models.Student'],['../class_biblio_dat_core_1_1_web_1_1_models_1_1_student.html',1,'BiblioDatCore.Web.Models.Student']]],
  ['studentlistviewmodel_263',['StudentListViewModel',['../class_biblio_dat_core_1_1_web_1_1_models_1_1_student_list_view_model.html',1,'BiblioDatCore::Web::Models']]],
  ['studentlogicwpf_264',['StudentLogicWPF',['../class_biblio_dat_core_1_1_u_i_1_1_b_l_1_1_student_logic_w_p_f.html',1,'BiblioDatCore::UI::BL']]],
  ['studentsapicontroller_265',['StudentsApiController',['../class_biblio_dat_core_1_1_web_1_1_controllers_1_1_students_api_controller.html',1,'BiblioDatCore::Web::Controllers']]],
  ['studentscontroller_266',['StudentsController',['../class_biblio_dat_core_1_1_web_1_1_controllers_1_1_students_controller.html',1,'BiblioDatCore::Web::Controllers']]],
  ['studentvm_267',['StudentVM',['../class_biblio_dat_core_1_1_wpf_client_1_1_student_v_m.html',1,'BiblioDatCore::WpfClient']]],
  ['studentwpf_268',['StudentWPF',['../class_biblio_dat_core_1_1_u_i_1_1_data_1_1_student_w_p_f.html',1,'BiblioDatCore::UI::Data']]]
];
