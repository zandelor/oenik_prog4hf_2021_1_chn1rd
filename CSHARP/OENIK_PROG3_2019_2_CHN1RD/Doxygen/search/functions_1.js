var searchData=
[
  ['bdbcontext_305',['BDBContext',['../class_biblio_dat_core_1_1_data_1_1_models_1_1_b_d_b_context.html#a8d6bb1fd5cce2b3a3e6a64e481992102',1,'BiblioDatCore.Data.Models.BDBContext.BDBContext()'],['../class_biblio_dat_core_1_1_data_1_1_models_1_1_b_d_b_context.html#afade4e38ae371b62acb184c902f477e5',1,'BiblioDatCore.Data.Models.BDBContext.BDBContext(DbContextOptions&lt; BDBContext &gt; options)']]],
  ['book_306',['Book',['../class_biblio_dat_core_1_1_data_1_1_models_1_1_book.html#a2f9f9f320e1bd2b772d557c8b894e585',1,'BiblioDatCore::Data::Models::Book']]],
  ['booksneverborrowed_307',['BooksNeverBorrowed',['../interface_biblio_dat_core_1_1_logic_1_1_i_logic.html#a2c23899734f5c1f234f5ac296edbdd03',1,'BiblioDatCore.Logic.ILogic.BooksNeverBorrowed()'],['../class_biblio_dat_core_1_1_logic_1_1_logic_class.html#ac0bdaa9070664fd1c2411dab19eceef0',1,'BiblioDatCore.Logic.LogicClass.BooksNeverBorrowed()']]],
  ['borrowsbygenres_308',['BorrowsByGenres',['../interface_biblio_dat_core_1_1_logic_1_1_i_logic.html#af0a184d55804212548149cac58bfa1e3',1,'BiblioDatCore.Logic.ILogic.BorrowsByGenres()'],['../class_biblio_dat_core_1_1_logic_1_1_logic_class.html#a3057680c84174a3ac35776e74df3fcb2',1,'BiblioDatCore.Logic.LogicClass.BorrowsByGenres()']]]
];
