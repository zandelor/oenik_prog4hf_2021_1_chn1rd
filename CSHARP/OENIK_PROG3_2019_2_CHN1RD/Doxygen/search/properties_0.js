var searchData=
[
  ['addcmd_425',['AddCmd',['../class_biblio_dat_core_1_1_wpf_client_1_1_main_v_m.html#a36468aa80915f1858b1776a64b987664',1,'BiblioDatCore::WpfClient::MainVM']]],
  ['addcommand_426',['AddCommand',['../class_biblio_dat_core_1_1_u_i_1_1_v_m_1_1_main_view_model.html#a6dc277aa2b89a80b67278a3235dc8d55',1,'BiblioDatCore::UI::VM::MainViewModel']]],
  ['allstudents_427',['AllStudents',['../class_biblio_dat_core_1_1_wpf_client_1_1_main_v_m.html#ab99dc5ece909aff2ffa22339164e1a2f',1,'BiblioDatCore::WpfClient::MainVM']]],
  ['author_428',['Author',['../class_biblio_dat_core_1_1_data_1_1_models_1_1_book.html#a56d79a355d817b8978185b01811ee7c0',1,'BiblioDatCore::Data::Models::Book']]],
  ['authorid_429',['Authorid',['../class_biblio_dat_core_1_1_data_1_1_models_1_1_author.html#ad05b13b76e25da419aa842b176cf63ca',1,'BiblioDatCore.Data.Models.Author.Authorid()'],['../class_biblio_dat_core_1_1_data_1_1_models_1_1_book.html#ab34029c16bde7fd95801d335b293fae0',1,'BiblioDatCore.Data.Models.Book.Authorid()']]],
  ['authorname_430',['AUTHORNAME',['../class_biblio_dat_core_1_1_logic_1_1_average_days_of_borrows.html#ae00fbddea882cab26477db88b470011c',1,'BiblioDatCore::Logic::AverageDaysOfBorrows']]],
  ['authors_431',['Authors',['../class_biblio_dat_core_1_1_data_1_1_models_1_1_b_d_b_context.html#a9fa7d9b1b07aebc821699c6e192146a0',1,'BiblioDatCore::Data::Models::BDBContext']]],
  ['average_432',['AVERAGE',['../class_biblio_dat_core_1_1_logic_1_1_average_days_of_borrows.html#a6d835a841a833af198a1999060375c4f',1,'BiblioDatCore::Logic::AverageDaysOfBorrows']]]
];
