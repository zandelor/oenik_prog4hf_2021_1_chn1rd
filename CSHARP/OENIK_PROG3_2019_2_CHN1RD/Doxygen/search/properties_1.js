var searchData=
[
  ['book_433',['Book',['../class_biblio_dat_core_1_1_data_1_1_models_1_1_checkout.html#a6147cfdc1d49f750f37840db6060c9fd',1,'BiblioDatCore::Data::Models::Checkout']]],
  ['bookid_434',['Bookid',['../class_biblio_dat_core_1_1_data_1_1_models_1_1_book.html#af22c9563194775c9693b49e3f9637966',1,'BiblioDatCore.Data.Models.Book.Bookid()'],['../class_biblio_dat_core_1_1_data_1_1_models_1_1_checkout.html#a980fdb42ac8a06b06fe652286c0cbb7f',1,'BiblioDatCore.Data.Models.Checkout.Bookid()']]],
  ['books_435',['Books',['../class_biblio_dat_core_1_1_data_1_1_models_1_1_author.html#af709528282dcad3717a70954126852a4',1,'BiblioDatCore.Data.Models.Author.Books()'],['../class_biblio_dat_core_1_1_data_1_1_models_1_1_b_d_b_context.html#abeec993af3e6db4ade86db4e714551d8',1,'BiblioDatCore.Data.Models.BDBContext.Books()']]],
  ['borrowdate_436',['Borrowdate',['../class_biblio_dat_core_1_1_data_1_1_models_1_1_checkout.html#a5a47407ba9724831a70f65dc1b51eebb',1,'BiblioDatCore::Data::Models::Checkout']]]
];
