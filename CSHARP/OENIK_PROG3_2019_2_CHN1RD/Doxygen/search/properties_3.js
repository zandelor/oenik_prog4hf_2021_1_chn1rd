var searchData=
[
  ['dateofbirth_443',['Dateofbirth',['../class_biblio_dat_core_1_1_data_1_1_models_1_1_author.html#a5d9e1d615918d9cd58cc1730d25bfed7',1,'BiblioDatCore.Data.Models.Author.Dateofbirth()'],['../class_biblio_dat_core_1_1_data_1_1_models_1_1_student.html#ad0e35f17cce515728a5b83225b4102b3',1,'BiblioDatCore.Data.Models.Student.Dateofbirth()']]],
  ['dateofbirth_444',['DateOfBirth',['../class_biblio_dat_core_1_1_u_i_1_1_data_1_1_student_w_p_f.html#a2406bed9d45093bb3a209913dc7e97d1',1,'BiblioDatCore.UI.Data.StudentWPF.DateOfBirth()'],['../class_biblio_dat_core_1_1_web_1_1_models_1_1_student.html#a5a134950df9ce7214d7174432d133713',1,'BiblioDatCore.Web.Models.Student.DateOfBirth()'],['../class_biblio_dat_core_1_1_wpf_client_1_1_student_v_m.html#a78d31d76b1efcad47b69b74aa666709a',1,'BiblioDatCore.WpfClient.StudentVM.DateOfBirth()']]],
  ['dateofdeath_445',['Dateofdeath',['../class_biblio_dat_core_1_1_data_1_1_models_1_1_author.html#ae5793b0571a5263942f62f3546601318',1,'BiblioDatCore::Data::Models::Author']]],
  ['delcmd_446',['DelCmd',['../class_biblio_dat_core_1_1_wpf_client_1_1_main_v_m.html#a962c8fd07d835abd1029bf5e7eb05e77',1,'BiblioDatCore::WpfClient::MainVM']]],
  ['deletecommand_447',['DeleteCommand',['../class_biblio_dat_core_1_1_u_i_1_1_v_m_1_1_main_view_model.html#ab6897e36e40acaed355105f26ca83d20',1,'BiblioDatCore::UI::VM::MainViewModel']]]
];
