var searchData=
[
  ['lastname_458',['LastName',['../class_biblio_dat_core_1_1_u_i_1_1_data_1_1_student_w_p_f.html#a8ea108125ccd63dd6bcf634eb7d250f2',1,'BiblioDatCore.UI.Data.StudentWPF.LastName()'],['../class_biblio_dat_core_1_1_web_1_1_models_1_1_student.html#ac40e7d7c8806f373ee6546b808ae4741',1,'BiblioDatCore.Web.Models.Student.LastName()'],['../class_biblio_dat_core_1_1_wpf_client_1_1_student_v_m.html#a7fd87f93efcefcc827562fd81d9755fe',1,'BiblioDatCore.WpfClient.StudentVM.LastName()']]],
  ['lastname_459',['Lastname',['../class_biblio_dat_core_1_1_data_1_1_models_1_1_author.html#af4852170d42aaa066faf1cd60fb4ffe1',1,'BiblioDatCore.Data.Models.Author.Lastname()'],['../class_biblio_dat_core_1_1_data_1_1_models_1_1_student.html#a4aeed299dd7af02d02a651bacae025b4',1,'BiblioDatCore.Data.Models.Student.Lastname()']]],
  ['listofstudents_460',['ListOfStudents',['../class_biblio_dat_core_1_1_web_1_1_models_1_1_student_list_view_model.html#a4bcccb6841f6cd402db951e1e4f83978',1,'BiblioDatCore::Web::Models::StudentListViewModel']]],
  ['loadcmd_461',['LoadCmd',['../class_biblio_dat_core_1_1_wpf_client_1_1_main_v_m.html#ae4e13852061a5cd81812ae4beb0a7c73',1,'BiblioDatCore::WpfClient::MainVM']]]
];
