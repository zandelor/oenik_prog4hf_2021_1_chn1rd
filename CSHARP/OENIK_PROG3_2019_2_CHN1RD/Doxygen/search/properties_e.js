var searchData=
[
  ['selectedstudent_472',['SelectedStudent',['../class_biblio_dat_core_1_1_wpf_client_1_1_main_v_m.html#a20ca127affd372d2d02cce57138a39bc',1,'BiblioDatCore::WpfClient::MainVM']]],
  ['showrequestid_473',['ShowRequestId',['../class_biblio_dat_core_1_1_web_1_1_models_1_1_error_view_model.html#a949667797ca38f0fb9d9edda94718a38',1,'BiblioDatCore::Web::Models::ErrorViewModel']]],
  ['student_474',['Student',['../class_biblio_dat_core_1_1_data_1_1_models_1_1_checkout.html#a73a1764f78ad639a4c830509735d9561',1,'BiblioDatCore.Data.Models.Checkout.Student()'],['../class_biblio_dat_core_1_1_u_i_1_1_user_interface_1_1_editor_window.html#a4adcf73fdf3eadfb8f32d78e17dea223',1,'BiblioDatCore.UI.UserInterface.EditorWindow.Student()'],['../class_biblio_dat_core_1_1_u_i_1_1_v_m_1_1_editor_view_model.html#a54871521d55e18862a4e717c11017a8f',1,'BiblioDatCore.UI.VM.EditorViewModel.Student()']]],
  ['studentid_475',['Studentid',['../class_biblio_dat_core_1_1_data_1_1_models_1_1_checkout.html#aaa78f782126abf5367ffcc05d920cc93',1,'BiblioDatCore.Data.Models.Checkout.Studentid()'],['../class_biblio_dat_core_1_1_data_1_1_models_1_1_student.html#aa1a321bb37059c913cb45bfadf3098e3',1,'BiblioDatCore.Data.Models.Student.Studentid()']]],
  ['students_476',['Students',['../class_biblio_dat_core_1_1_data_1_1_models_1_1_b_d_b_context.html#a52566431d3041c76527ca65b34793990',1,'BiblioDatCore.Data.Models.BDBContext.Students()'],['../class_biblio_dat_core_1_1_u_i_1_1_v_m_1_1_main_view_model.html#ad2bb4705b8dc8e68eba0e9454fcb4b64',1,'BiblioDatCore.UI.VM.MainViewModel.Students()']]],
  ['studentselected_477',['StudentSelected',['../class_biblio_dat_core_1_1_u_i_1_1_v_m_1_1_main_view_model.html#a224f6bc894256a7ab9736267e135d88b',1,'BiblioDatCore::UI::VM::MainViewModel']]]
];
