var annotated_dup =
[
    [ "AspNetCore", "namespace_asp_net_core.html", [
      [ "Views__ViewImports", "class_asp_net_core_1_1_views_____view_imports.html", "class_asp_net_core_1_1_views_____view_imports" ],
      [ "Views__ViewStart", "class_asp_net_core_1_1_views_____view_start.html", "class_asp_net_core_1_1_views_____view_start" ],
      [ "Views_Home_Index", "class_asp_net_core_1_1_views___home___index.html", "class_asp_net_core_1_1_views___home___index" ],
      [ "Views_Home_Privacy", "class_asp_net_core_1_1_views___home___privacy.html", "class_asp_net_core_1_1_views___home___privacy" ],
      [ "Views_Shared__Layout", "class_asp_net_core_1_1_views___shared_____layout.html", "class_asp_net_core_1_1_views___shared_____layout" ],
      [ "Views_Shared__ValidationScriptsPartial", "class_asp_net_core_1_1_views___shared_____validation_scripts_partial.html", "class_asp_net_core_1_1_views___shared_____validation_scripts_partial" ],
      [ "Views_Shared_Error", "class_asp_net_core_1_1_views___shared___error.html", "class_asp_net_core_1_1_views___shared___error" ],
      [ "Views_Students_StudentsDetails", "class_asp_net_core_1_1_views___students___students_details.html", "class_asp_net_core_1_1_views___students___students_details" ],
      [ "Views_Students_StudentsEdit", "class_asp_net_core_1_1_views___students___students_edit.html", "class_asp_net_core_1_1_views___students___students_edit" ],
      [ "Views_Students_StudentsIndex", "class_asp_net_core_1_1_views___students___students_index.html", "class_asp_net_core_1_1_views___students___students_index" ],
      [ "Views_Students_StudentsList", "class_asp_net_core_1_1_views___students___students_list.html", "class_asp_net_core_1_1_views___students___students_list" ]
    ] ],
    [ "BiblioDatCore", "namespace_biblio_dat_core.html", [
      [ "Data", "namespace_biblio_dat_core_1_1_data.html", [
        [ "Models", "namespace_biblio_dat_core_1_1_data_1_1_models.html", [
          [ "Author", "class_biblio_dat_core_1_1_data_1_1_models_1_1_author.html", "class_biblio_dat_core_1_1_data_1_1_models_1_1_author" ],
          [ "BDBContext", "class_biblio_dat_core_1_1_data_1_1_models_1_1_b_d_b_context.html", "class_biblio_dat_core_1_1_data_1_1_models_1_1_b_d_b_context" ],
          [ "Book", "class_biblio_dat_core_1_1_data_1_1_models_1_1_book.html", "class_biblio_dat_core_1_1_data_1_1_models_1_1_book" ],
          [ "Checkout", "class_biblio_dat_core_1_1_data_1_1_models_1_1_checkout.html", "class_biblio_dat_core_1_1_data_1_1_models_1_1_checkout" ],
          [ "Student", "class_biblio_dat_core_1_1_data_1_1_models_1_1_student.html", "class_biblio_dat_core_1_1_data_1_1_models_1_1_student" ]
        ] ]
      ] ],
      [ "Logic", "namespace_biblio_dat_core_1_1_logic.html", [
        [ "Tests", "namespace_biblio_dat_core_1_1_logic_1_1_tests.html", [
          [ "LogicTest", "class_biblio_dat_core_1_1_logic_1_1_tests_1_1_logic_test.html", "class_biblio_dat_core_1_1_logic_1_1_tests_1_1_logic_test" ]
        ] ],
        [ "AverageDaysOfBorrows", "class_biblio_dat_core_1_1_logic_1_1_average_days_of_borrows.html", "class_biblio_dat_core_1_1_logic_1_1_average_days_of_borrows" ],
        [ "GenresAndNumberOfBorrows", "class_biblio_dat_core_1_1_logic_1_1_genres_and_number_of_borrows.html", "class_biblio_dat_core_1_1_logic_1_1_genres_and_number_of_borrows" ],
        [ "IdAlreadyInCollectionException", "class_biblio_dat_core_1_1_logic_1_1_id_already_in_collection_exception.html", "class_biblio_dat_core_1_1_logic_1_1_id_already_in_collection_exception" ],
        [ "IdNotInCollectionException", "class_biblio_dat_core_1_1_logic_1_1_id_not_in_collection_exception.html", "class_biblio_dat_core_1_1_logic_1_1_id_not_in_collection_exception" ],
        [ "ILogic", "interface_biblio_dat_core_1_1_logic_1_1_i_logic.html", "interface_biblio_dat_core_1_1_logic_1_1_i_logic" ],
        [ "LogicClass", "class_biblio_dat_core_1_1_logic_1_1_logic_class.html", "class_biblio_dat_core_1_1_logic_1_1_logic_class" ],
        [ "Person", "class_biblio_dat_core_1_1_logic_1_1_person.html", "class_biblio_dat_core_1_1_logic_1_1_person" ],
        [ "VoucherClass", "class_biblio_dat_core_1_1_logic_1_1_voucher_class.html", "class_biblio_dat_core_1_1_logic_1_1_voucher_class" ]
      ] ],
      [ "Program", "namespace_biblio_dat_core_1_1_program.html", [
        [ "Menu", "class_biblio_dat_core_1_1_program_1_1_menu.html", "class_biblio_dat_core_1_1_program_1_1_menu" ],
        [ "Program", "class_biblio_dat_core_1_1_program_1_1_program.html", null ]
      ] ],
      [ "Repository", "namespace_biblio_dat_core_1_1_repository.html", [
        [ "IRepository", "interface_biblio_dat_core_1_1_repository_1_1_i_repository.html", "interface_biblio_dat_core_1_1_repository_1_1_i_repository" ],
        [ "RepoClass", "class_biblio_dat_core_1_1_repository_1_1_repo_class.html", "class_biblio_dat_core_1_1_repository_1_1_repo_class" ]
      ] ],
      [ "UI", "namespace_biblio_dat_core_1_1_u_i.html", [
        [ "BL", "namespace_biblio_dat_core_1_1_u_i_1_1_b_l.html", [
          [ "IEditorService", "interface_biblio_dat_core_1_1_u_i_1_1_b_l_1_1_i_editor_service.html", "interface_biblio_dat_core_1_1_u_i_1_1_b_l_1_1_i_editor_service" ],
          [ "IStudentLogic", "interface_biblio_dat_core_1_1_u_i_1_1_b_l_1_1_i_student_logic.html", "interface_biblio_dat_core_1_1_u_i_1_1_b_l_1_1_i_student_logic" ],
          [ "StudentLogicWPF", "class_biblio_dat_core_1_1_u_i_1_1_b_l_1_1_student_logic_w_p_f.html", "class_biblio_dat_core_1_1_u_i_1_1_b_l_1_1_student_logic_w_p_f" ]
        ] ],
        [ "Data", "namespace_biblio_dat_core_1_1_u_i_1_1_data.html", [
          [ "StudentWPF", "class_biblio_dat_core_1_1_u_i_1_1_data_1_1_student_w_p_f.html", "class_biblio_dat_core_1_1_u_i_1_1_data_1_1_student_w_p_f" ]
        ] ],
        [ "UserInterface", "namespace_biblio_dat_core_1_1_u_i_1_1_user_interface.html", [
          [ "EditorWindow", "class_biblio_dat_core_1_1_u_i_1_1_user_interface_1_1_editor_window.html", "class_biblio_dat_core_1_1_u_i_1_1_user_interface_1_1_editor_window" ],
          [ "EditorServiceViaWindow", "class_biblio_dat_core_1_1_u_i_1_1_user_interface_1_1_editor_service_via_window.html", "class_biblio_dat_core_1_1_u_i_1_1_user_interface_1_1_editor_service_via_window" ]
        ] ],
        [ "VM", "namespace_biblio_dat_core_1_1_u_i_1_1_v_m.html", [
          [ "EditorViewModel", "class_biblio_dat_core_1_1_u_i_1_1_v_m_1_1_editor_view_model.html", "class_biblio_dat_core_1_1_u_i_1_1_v_m_1_1_editor_view_model" ],
          [ "MainViewModel", "class_biblio_dat_core_1_1_u_i_1_1_v_m_1_1_main_view_model.html", "class_biblio_dat_core_1_1_u_i_1_1_v_m_1_1_main_view_model" ]
        ] ],
        [ "App", "class_biblio_dat_core_1_1_u_i_1_1_app.html", "class_biblio_dat_core_1_1_u_i_1_1_app" ],
        [ "MainWindow", "class_biblio_dat_core_1_1_u_i_1_1_main_window.html", "class_biblio_dat_core_1_1_u_i_1_1_main_window" ],
        [ "MyIoc", "class_biblio_dat_core_1_1_u_i_1_1_my_ioc.html", "class_biblio_dat_core_1_1_u_i_1_1_my_ioc" ]
      ] ],
      [ "Web", "namespace_biblio_dat_core_1_1_web.html", [
        [ "Controllers", "namespace_biblio_dat_core_1_1_web_1_1_controllers.html", [
          [ "ApiResult", "class_biblio_dat_core_1_1_web_1_1_controllers_1_1_api_result.html", "class_biblio_dat_core_1_1_web_1_1_controllers_1_1_api_result" ],
          [ "HomeController", "class_biblio_dat_core_1_1_web_1_1_controllers_1_1_home_controller.html", "class_biblio_dat_core_1_1_web_1_1_controllers_1_1_home_controller" ],
          [ "StudentsApiController", "class_biblio_dat_core_1_1_web_1_1_controllers_1_1_students_api_controller.html", "class_biblio_dat_core_1_1_web_1_1_controllers_1_1_students_api_controller" ],
          [ "StudentsController", "class_biblio_dat_core_1_1_web_1_1_controllers_1_1_students_controller.html", "class_biblio_dat_core_1_1_web_1_1_controllers_1_1_students_controller" ]
        ] ],
        [ "Models", "namespace_biblio_dat_core_1_1_web_1_1_models.html", [
          [ "ErrorViewModel", "class_biblio_dat_core_1_1_web_1_1_models_1_1_error_view_model.html", "class_biblio_dat_core_1_1_web_1_1_models_1_1_error_view_model" ],
          [ "MapperFactory", "class_biblio_dat_core_1_1_web_1_1_models_1_1_mapper_factory.html", "class_biblio_dat_core_1_1_web_1_1_models_1_1_mapper_factory" ],
          [ "Student", "class_biblio_dat_core_1_1_web_1_1_models_1_1_student.html", "class_biblio_dat_core_1_1_web_1_1_models_1_1_student" ],
          [ "StudentListViewModel", "class_biblio_dat_core_1_1_web_1_1_models_1_1_student_list_view_model.html", "class_biblio_dat_core_1_1_web_1_1_models_1_1_student_list_view_model" ]
        ] ],
        [ "Program", "class_biblio_dat_core_1_1_web_1_1_program.html", "class_biblio_dat_core_1_1_web_1_1_program" ],
        [ "Startup", "class_biblio_dat_core_1_1_web_1_1_startup.html", "class_biblio_dat_core_1_1_web_1_1_startup" ]
      ] ],
      [ "WpfClient", "namespace_biblio_dat_core_1_1_wpf_client.html", [
        [ "App", "class_biblio_dat_core_1_1_wpf_client_1_1_app.html", "class_biblio_dat_core_1_1_wpf_client_1_1_app" ],
        [ "Editor", "class_biblio_dat_core_1_1_wpf_client_1_1_editor.html", "class_biblio_dat_core_1_1_wpf_client_1_1_editor" ],
        [ "IMainLogic", "interface_biblio_dat_core_1_1_wpf_client_1_1_i_main_logic.html", "interface_biblio_dat_core_1_1_wpf_client_1_1_i_main_logic" ],
        [ "MainLogic", "class_biblio_dat_core_1_1_wpf_client_1_1_main_logic.html", "class_biblio_dat_core_1_1_wpf_client_1_1_main_logic" ],
        [ "MainVM", "class_biblio_dat_core_1_1_wpf_client_1_1_main_v_m.html", "class_biblio_dat_core_1_1_wpf_client_1_1_main_v_m" ],
        [ "MainWindow", "class_biblio_dat_core_1_1_wpf_client_1_1_main_window.html", "class_biblio_dat_core_1_1_wpf_client_1_1_main_window" ],
        [ "MyIoc", "class_biblio_dat_core_1_1_wpf_client_1_1_my_ioc.html", "class_biblio_dat_core_1_1_wpf_client_1_1_my_ioc" ],
        [ "StudentVM", "class_biblio_dat_core_1_1_wpf_client_1_1_student_v_m.html", "class_biblio_dat_core_1_1_wpf_client_1_1_student_v_m" ]
      ] ]
    ] ],
    [ "XamlGeneratedNamespace", "namespace_xaml_generated_namespace.html", [
      [ "GeneratedInternalTypeHelper", "class_xaml_generated_namespace_1_1_generated_internal_type_helper.html", "class_xaml_generated_namespace_1_1_generated_internal_type_helper" ]
    ] ]
];