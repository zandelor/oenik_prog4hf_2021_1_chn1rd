var class_biblio_dat_core_1_1_data_1_1_models_1_1_b_d_b_context =
[
    [ "BDBContext", "class_biblio_dat_core_1_1_data_1_1_models_1_1_b_d_b_context.html#a8d6bb1fd5cce2b3a3e6a64e481992102", null ],
    [ "BDBContext", "class_biblio_dat_core_1_1_data_1_1_models_1_1_b_d_b_context.html#afade4e38ae371b62acb184c902f477e5", null ],
    [ "OnConfiguring", "class_biblio_dat_core_1_1_data_1_1_models_1_1_b_d_b_context.html#a1303cc8ec7db4547a6731a0067b5a52a", null ],
    [ "OnModelCreating", "class_biblio_dat_core_1_1_data_1_1_models_1_1_b_d_b_context.html#ad7b665e31ed3da4762178bc786a6e20e", null ],
    [ "Authors", "class_biblio_dat_core_1_1_data_1_1_models_1_1_b_d_b_context.html#a9fa7d9b1b07aebc821699c6e192146a0", null ],
    [ "Books", "class_biblio_dat_core_1_1_data_1_1_models_1_1_b_d_b_context.html#abeec993af3e6db4ade86db4e714551d8", null ],
    [ "Checkouts", "class_biblio_dat_core_1_1_data_1_1_models_1_1_b_d_b_context.html#aff56b9592a724db69a62bd4a658959c6", null ],
    [ "Students", "class_biblio_dat_core_1_1_data_1_1_models_1_1_b_d_b_context.html#a52566431d3041c76527ca65b34793990", null ]
];