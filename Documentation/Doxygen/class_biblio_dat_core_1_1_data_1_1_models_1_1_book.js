var class_biblio_dat_core_1_1_data_1_1_models_1_1_book =
[
    [ "Book", "class_biblio_dat_core_1_1_data_1_1_models_1_1_book.html#a2f9f9f320e1bd2b772d557c8b894e585", null ],
    [ "Author", "class_biblio_dat_core_1_1_data_1_1_models_1_1_book.html#a56d79a355d817b8978185b01811ee7c0", null ],
    [ "Authorid", "class_biblio_dat_core_1_1_data_1_1_models_1_1_book.html#ab34029c16bde7fd95801d335b293fae0", null ],
    [ "Bookid", "class_biblio_dat_core_1_1_data_1_1_models_1_1_book.html#af22c9563194775c9693b49e3f9637966", null ],
    [ "Checkouts", "class_biblio_dat_core_1_1_data_1_1_models_1_1_book.html#ab39636f6fc7ab8dede5817a77aff5899", null ],
    [ "Edition", "class_biblio_dat_core_1_1_data_1_1_models_1_1_book.html#a6dcda2006684d20d038d61afd9279732", null ],
    [ "Genre", "class_biblio_dat_core_1_1_data_1_1_models_1_1_book.html#a86ea8f3a477407870a8787860434a917", null ],
    [ "Pages", "class_biblio_dat_core_1_1_data_1_1_models_1_1_book.html#af943b9cde72874aa92b4bc02b9f922f9", null ],
    [ "Releasedate", "class_biblio_dat_core_1_1_data_1_1_models_1_1_book.html#aecfd2cecf3ddb053ebfd1a7b86c8ffdc", null ],
    [ "Title", "class_biblio_dat_core_1_1_data_1_1_models_1_1_book.html#aba4742ef891102f2cd24f8295de6dde0", null ]
];