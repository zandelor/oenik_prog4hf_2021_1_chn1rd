var class_biblio_dat_core_1_1_data_1_1_models_1_1_student =
[
    [ "Student", "class_biblio_dat_core_1_1_data_1_1_models_1_1_student.html#a38f5319f3248d6158ae58e055979f0f3", null ],
    [ "Checkouts", "class_biblio_dat_core_1_1_data_1_1_models_1_1_student.html#a976e81e6040ac7a503727b6164e38c51", null ],
    [ "Classyear", "class_biblio_dat_core_1_1_data_1_1_models_1_1_student.html#aa0475514d6b7984bff08d206cf8f6f38", null ],
    [ "Dateofbirth", "class_biblio_dat_core_1_1_data_1_1_models_1_1_student.html#ad0e35f17cce515728a5b83225b4102b3", null ],
    [ "Firstname", "class_biblio_dat_core_1_1_data_1_1_models_1_1_student.html#aa5255f6ed282103cc324240cb7ed824d", null ],
    [ "Gender", "class_biblio_dat_core_1_1_data_1_1_models_1_1_student.html#a7f7a70ed9407d94d50d70cc208c1da3c", null ],
    [ "Lastname", "class_biblio_dat_core_1_1_data_1_1_models_1_1_student.html#a4aeed299dd7af02d02a651bacae025b4", null ],
    [ "Studentid", "class_biblio_dat_core_1_1_data_1_1_models_1_1_student.html#aa1a321bb37059c913cb45bfadf3098e3", null ]
];