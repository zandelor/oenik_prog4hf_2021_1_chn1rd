var class_biblio_dat_core_1_1_repository_1_1_repo_class =
[
    [ "RepoClass", "class_biblio_dat_core_1_1_repository_1_1_repo_class.html#acb547f3a390259ec149c3437100039cf", null ],
    [ "CreateAuthor", "class_biblio_dat_core_1_1_repository_1_1_repo_class.html#a0e0e43ee3799cfcf2655fa0de6baedbb", null ],
    [ "CreateBook", "class_biblio_dat_core_1_1_repository_1_1_repo_class.html#ab4698336194f2e2659f2239aabc5ffb2", null ],
    [ "CreateCheckout", "class_biblio_dat_core_1_1_repository_1_1_repo_class.html#a9910ee62d69625534f75c2d06c0b6816", null ],
    [ "CreateStudent", "class_biblio_dat_core_1_1_repository_1_1_repo_class.html#a8822e36ef6fd9f032728fba69e475b2f", null ],
    [ "DeleteAuthor", "class_biblio_dat_core_1_1_repository_1_1_repo_class.html#a1bd0cf2371c1510c937a9fe662633dd3", null ],
    [ "DeleteBook", "class_biblio_dat_core_1_1_repository_1_1_repo_class.html#ad0c74866afe489d289a2ea9bdd5967fa", null ],
    [ "DeleteCheckout", "class_biblio_dat_core_1_1_repository_1_1_repo_class.html#a7897271e5d27bece2c0181878977614d", null ],
    [ "DeleteStudent", "class_biblio_dat_core_1_1_repository_1_1_repo_class.html#a7aef1304958bcb494afd53897f21215e", null ],
    [ "GetOneAuthor", "class_biblio_dat_core_1_1_repository_1_1_repo_class.html#a3c8da3800c9dd9531a5d2e2df771d845", null ],
    [ "GetOneBook", "class_biblio_dat_core_1_1_repository_1_1_repo_class.html#aa4d8cb879d94a3bd4a89bed557e0e777", null ],
    [ "GetOneCheckout", "class_biblio_dat_core_1_1_repository_1_1_repo_class.html#ab5b91e0f1c370b06f03b4b98602b9633", null ],
    [ "GetOneStudent", "class_biblio_dat_core_1_1_repository_1_1_repo_class.html#a33cf02855f4772629917fcf28267e416", null ],
    [ "ReadAllAuthor", "class_biblio_dat_core_1_1_repository_1_1_repo_class.html#a597e351279ce9acfa59b9655237abcc7", null ],
    [ "ReadAllBook", "class_biblio_dat_core_1_1_repository_1_1_repo_class.html#a5dd86b1bbde0afd9994316a15b6f0854", null ],
    [ "ReadAllCheckout", "class_biblio_dat_core_1_1_repository_1_1_repo_class.html#ab80e2ec2e8d9267a138776870c8bc20a", null ],
    [ "ReadAllStudent", "class_biblio_dat_core_1_1_repository_1_1_repo_class.html#a9260916386b3fb36ef3461bf8c58a04f", null ],
    [ "UpdateAuthor", "class_biblio_dat_core_1_1_repository_1_1_repo_class.html#a12ba417e080b87018c176fa3d477c1b6", null ],
    [ "UpdateBook", "class_biblio_dat_core_1_1_repository_1_1_repo_class.html#a3fbf5886f0dfbde24fea071226ca45fd", null ],
    [ "UpdateCheckout", "class_biblio_dat_core_1_1_repository_1_1_repo_class.html#aa7d07f307cc853812ad199b13db06153", null ],
    [ "UpdateStudent", "class_biblio_dat_core_1_1_repository_1_1_repo_class.html#a1932eb222d8270c111327c1e2b54c1e5", null ]
];