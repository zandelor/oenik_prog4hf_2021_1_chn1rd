var class_biblio_dat_core_1_1_u_i_1_1_b_l_1_1_student_logic_w_p_f =
[
    [ "StudentLogicWPF", "class_biblio_dat_core_1_1_u_i_1_1_b_l_1_1_student_logic_w_p_f.html#a078b6e04e8b843a21c622e36c0c7f751", null ],
    [ "AddStudent", "class_biblio_dat_core_1_1_u_i_1_1_b_l_1_1_student_logic_w_p_f.html#ac2aa035f3f7b152be3893e64a9b924fe", null ],
    [ "DelStudent", "class_biblio_dat_core_1_1_u_i_1_1_b_l_1_1_student_logic_w_p_f.html#a87653e9816fc646116667d838eed8923", null ],
    [ "GetAllStudentsFromDatabase", "class_biblio_dat_core_1_1_u_i_1_1_b_l_1_1_student_logic_w_p_f.html#ac13111c9da674ec84bf897e1ff31f7cd", null ],
    [ "ModStudent", "class_biblio_dat_core_1_1_u_i_1_1_b_l_1_1_student_logic_w_p_f.html#a3ebe6172a336a86bedd4107c5729a015", null ]
];