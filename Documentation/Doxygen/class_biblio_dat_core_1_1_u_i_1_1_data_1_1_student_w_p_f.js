var class_biblio_dat_core_1_1_u_i_1_1_data_1_1_student_w_p_f =
[
    [ "StudentWPF", "class_biblio_dat_core_1_1_u_i_1_1_data_1_1_student_w_p_f.html#a855cc83a1ef436ea8cccd44b786b19b6", null ],
    [ "StudentWPF", "class_biblio_dat_core_1_1_u_i_1_1_data_1_1_student_w_p_f.html#a06e75cafb7b1c976f1c37ff5e4e7f6bb", null ],
    [ "ConvertToEntity", "class_biblio_dat_core_1_1_u_i_1_1_data_1_1_student_w_p_f.html#ab57ec57a063e147a772ddc5877ea1a8d", null ],
    [ "CopyFrom", "class_biblio_dat_core_1_1_u_i_1_1_data_1_1_student_w_p_f.html#affe96164939ea89f8ffa65df64090a69", null ],
    [ "CopyFrom", "class_biblio_dat_core_1_1_u_i_1_1_data_1_1_student_w_p_f.html#a7f5a506716bea0104930af143e0a9e3d", null ],
    [ "ToString", "class_biblio_dat_core_1_1_u_i_1_1_data_1_1_student_w_p_f.html#ab5adef7e03283ad615d1053df55430e4", null ],
    [ "ClassYear", "class_biblio_dat_core_1_1_u_i_1_1_data_1_1_student_w_p_f.html#a14bdd2613a358dac66fd530f87fd8ddf", null ],
    [ "DateOfBirth", "class_biblio_dat_core_1_1_u_i_1_1_data_1_1_student_w_p_f.html#a2406bed9d45093bb3a209913dc7e97d1", null ],
    [ "FirstName", "class_biblio_dat_core_1_1_u_i_1_1_data_1_1_student_w_p_f.html#ac9e5effe88e790745437fc6d7d9c7580", null ],
    [ "Gender", "class_biblio_dat_core_1_1_u_i_1_1_data_1_1_student_w_p_f.html#ab4077aff533930f357d005d7137283b1", null ],
    [ "Id", "class_biblio_dat_core_1_1_u_i_1_1_data_1_1_student_w_p_f.html#ab7875228c1d6abc8c4ad518aa742a2fb", null ],
    [ "LastName", "class_biblio_dat_core_1_1_u_i_1_1_data_1_1_student_w_p_f.html#a8ea108125ccd63dd6bcf634eb7d250f2", null ]
];