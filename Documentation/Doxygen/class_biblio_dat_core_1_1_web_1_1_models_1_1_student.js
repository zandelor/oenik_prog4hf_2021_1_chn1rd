var class_biblio_dat_core_1_1_web_1_1_models_1_1_student =
[
    [ "ClassYear", "class_biblio_dat_core_1_1_web_1_1_models_1_1_student.html#a07510408fdb17379972f14ffd466531b", null ],
    [ "DateOfBirth", "class_biblio_dat_core_1_1_web_1_1_models_1_1_student.html#a5a134950df9ce7214d7174432d133713", null ],
    [ "FirstName", "class_biblio_dat_core_1_1_web_1_1_models_1_1_student.html#ad5020cca496192d0889b535d3e5a604f", null ],
    [ "Gender", "class_biblio_dat_core_1_1_web_1_1_models_1_1_student.html#af1384b607a16b52191b19e482bf09424", null ],
    [ "Id", "class_biblio_dat_core_1_1_web_1_1_models_1_1_student.html#a858b9f512d5967e6dd06e49d94ee2aad", null ],
    [ "LastName", "class_biblio_dat_core_1_1_web_1_1_models_1_1_student.html#ac40e7d7c8806f373ee6546b808ae4741", null ]
];