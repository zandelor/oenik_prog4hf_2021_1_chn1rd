var hierarchy =
[
    [ "BiblioDatCore.Web.Controllers.ApiResult", "class_biblio_dat_core_1_1_web_1_1_controllers_1_1_api_result.html", null ],
    [ "Application", null, [
      [ "BiblioDatCore.UI.App", "class_biblio_dat_core_1_1_u_i_1_1_app.html", null ]
    ] ],
    [ "System.Windows.Application", null, [
      [ "BiblioDatCore.UI.App", "class_biblio_dat_core_1_1_u_i_1_1_app.html", null ],
      [ "BiblioDatCore.UI.App", "class_biblio_dat_core_1_1_u_i_1_1_app.html", null ],
      [ "BiblioDatCore.WpfClient.App", "class_biblio_dat_core_1_1_wpf_client_1_1_app.html", null ],
      [ "BiblioDatCore.WpfClient.App", "class_biblio_dat_core_1_1_wpf_client_1_1_app.html", null ],
      [ "BiblioDatCore.WpfClient.App", "class_biblio_dat_core_1_1_wpf_client_1_1_app.html", null ]
    ] ],
    [ "BiblioDatCore.Data.Models.Author", "class_biblio_dat_core_1_1_data_1_1_models_1_1_author.html", null ],
    [ "BiblioDatCore.Logic.AverageDaysOfBorrows", "class_biblio_dat_core_1_1_logic_1_1_average_days_of_borrows.html", null ],
    [ "BiblioDatCore.Data.Models.Book", "class_biblio_dat_core_1_1_data_1_1_models_1_1_book.html", null ],
    [ "BiblioDatCore.Data.Models.Checkout", "class_biblio_dat_core_1_1_data_1_1_models_1_1_checkout.html", null ],
    [ "Controller", null, [
      [ "BiblioDatCore.Web.Controllers.HomeController", "class_biblio_dat_core_1_1_web_1_1_controllers_1_1_home_controller.html", null ],
      [ "BiblioDatCore.Web.Controllers.StudentsController", "class_biblio_dat_core_1_1_web_1_1_controllers_1_1_students_controller.html", null ]
    ] ],
    [ "ControllerBase", null, [
      [ "BiblioDatCore.Web.Controllers.StudentsApiController", "class_biblio_dat_core_1_1_web_1_1_controllers_1_1_students_api_controller.html", null ]
    ] ],
    [ "DbContext", null, [
      [ "BiblioDatCore.Data.Models.BDBContext", "class_biblio_dat_core_1_1_data_1_1_models_1_1_b_d_b_context.html", null ]
    ] ],
    [ "BiblioDatCore.Web.Models.ErrorViewModel", "class_biblio_dat_core_1_1_web_1_1_models_1_1_error_view_model.html", null ],
    [ "Exception", null, [
      [ "BiblioDatCore.Logic.IdAlreadyInCollectionException", "class_biblio_dat_core_1_1_logic_1_1_id_already_in_collection_exception.html", null ],
      [ "BiblioDatCore.Logic.IdNotInCollectionException", "class_biblio_dat_core_1_1_logic_1_1_id_not_in_collection_exception.html", null ]
    ] ],
    [ "BiblioDatCore.Logic.GenresAndNumberOfBorrows", "class_biblio_dat_core_1_1_logic_1_1_genres_and_number_of_borrows.html", null ],
    [ "System.Windows.Markup.IComponentConnector", null, [
      [ "BiblioDatCore.UI.MainWindow", "class_biblio_dat_core_1_1_u_i_1_1_main_window.html", null ],
      [ "BiblioDatCore.UI.MainWindow", "class_biblio_dat_core_1_1_u_i_1_1_main_window.html", null ],
      [ "BiblioDatCore.UI.UserInterface.EditorWindow", "class_biblio_dat_core_1_1_u_i_1_1_user_interface_1_1_editor_window.html", null ],
      [ "BiblioDatCore.UI.UserInterface.EditorWindow", "class_biblio_dat_core_1_1_u_i_1_1_user_interface_1_1_editor_window.html", null ],
      [ "BiblioDatCore.WpfClient.Editor", "class_biblio_dat_core_1_1_wpf_client_1_1_editor.html", null ],
      [ "BiblioDatCore.WpfClient.Editor", "class_biblio_dat_core_1_1_wpf_client_1_1_editor.html", null ],
      [ "BiblioDatCore.WpfClient.MainWindow", "class_biblio_dat_core_1_1_wpf_client_1_1_main_window.html", null ],
      [ "BiblioDatCore.WpfClient.MainWindow", "class_biblio_dat_core_1_1_wpf_client_1_1_main_window.html", null ]
    ] ],
    [ "BiblioDatCore.UI.BL.IEditorService", "interface_biblio_dat_core_1_1_u_i_1_1_b_l_1_1_i_editor_service.html", [
      [ "BiblioDatCore.UI.UserInterface.EditorServiceViaWindow", "class_biblio_dat_core_1_1_u_i_1_1_user_interface_1_1_editor_service_via_window.html", null ]
    ] ],
    [ "BiblioDatCore.Logic.ILogic", "interface_biblio_dat_core_1_1_logic_1_1_i_logic.html", [
      [ "BiblioDatCore.Logic.LogicClass", "class_biblio_dat_core_1_1_logic_1_1_logic_class.html", null ]
    ] ],
    [ "BiblioDatCore.WpfClient.IMainLogic", "interface_biblio_dat_core_1_1_wpf_client_1_1_i_main_logic.html", [
      [ "BiblioDatCore.WpfClient.MainLogic", "class_biblio_dat_core_1_1_wpf_client_1_1_main_logic.html", null ]
    ] ],
    [ "System.Windows.Markup.InternalTypeHelper", null, [
      [ "XamlGeneratedNamespace.GeneratedInternalTypeHelper", "class_xaml_generated_namespace_1_1_generated_internal_type_helper.html", null ],
      [ "XamlGeneratedNamespace.GeneratedInternalTypeHelper", "class_xaml_generated_namespace_1_1_generated_internal_type_helper.html", null ],
      [ "XamlGeneratedNamespace.GeneratedInternalTypeHelper", "class_xaml_generated_namespace_1_1_generated_internal_type_helper.html", null ]
    ] ],
    [ "BiblioDatCore.Repository.IRepository", "interface_biblio_dat_core_1_1_repository_1_1_i_repository.html", [
      [ "BiblioDatCore.Repository.RepoClass", "class_biblio_dat_core_1_1_repository_1_1_repo_class.html", null ]
    ] ],
    [ "IServiceLocator", null, [
      [ "BiblioDatCore.UI.MyIoc", "class_biblio_dat_core_1_1_u_i_1_1_my_ioc.html", null ],
      [ "BiblioDatCore.WpfClient.MyIoc", "class_biblio_dat_core_1_1_wpf_client_1_1_my_ioc.html", null ]
    ] ],
    [ "BiblioDatCore.UI.BL.IStudentLogic", "interface_biblio_dat_core_1_1_u_i_1_1_b_l_1_1_i_student_logic.html", [
      [ "BiblioDatCore.UI.BL.StudentLogicWPF", "class_biblio_dat_core_1_1_u_i_1_1_b_l_1_1_student_logic_w_p_f.html", null ]
    ] ],
    [ "BiblioDatCore.Logic.Tests.LogicTest", "class_biblio_dat_core_1_1_logic_1_1_tests_1_1_logic_test.html", null ],
    [ "BiblioDatCore.Web.Models.MapperFactory", "class_biblio_dat_core_1_1_web_1_1_models_1_1_mapper_factory.html", null ],
    [ "BiblioDatCore.Program.Menu", "class_biblio_dat_core_1_1_program_1_1_menu.html", null ],
    [ "ObservableObject", null, [
      [ "BiblioDatCore.UI.Data.StudentWPF", "class_biblio_dat_core_1_1_u_i_1_1_data_1_1_student_w_p_f.html", null ],
      [ "BiblioDatCore.WpfClient.StudentVM", "class_biblio_dat_core_1_1_wpf_client_1_1_student_v_m.html", null ]
    ] ],
    [ "BiblioDatCore.Logic.Person", "class_biblio_dat_core_1_1_logic_1_1_person.html", null ],
    [ "BiblioDatCore.Program.Program", "class_biblio_dat_core_1_1_program_1_1_program.html", null ],
    [ "BiblioDatCore.Web.Program", "class_biblio_dat_core_1_1_web_1_1_program.html", null ],
    [ "global.MicrosoftAspNetCore.Mvc.Razor.RazorPage", null, [
      [ "AspNetCore.Views_Home_Index", "class_asp_net_core_1_1_views___home___index.html", null ],
      [ "AspNetCore.Views_Home_Privacy", "class_asp_net_core_1_1_views___home___privacy.html", null ],
      [ "AspNetCore.Views_Shared_Error", "class_asp_net_core_1_1_views___shared___error.html", null ],
      [ "AspNetCore.Views_Shared__Layout", "class_asp_net_core_1_1_views___shared_____layout.html", null ],
      [ "AspNetCore.Views_Shared__ValidationScriptsPartial", "class_asp_net_core_1_1_views___shared_____validation_scripts_partial.html", null ],
      [ "AspNetCore.Views_Students_StudentsDetails", "class_asp_net_core_1_1_views___students___students_details.html", null ],
      [ "AspNetCore.Views_Students_StudentsEdit", "class_asp_net_core_1_1_views___students___students_edit.html", null ],
      [ "AspNetCore.Views_Students_StudentsIndex", "class_asp_net_core_1_1_views___students___students_index.html", null ],
      [ "AspNetCore.Views__ViewImports", "class_asp_net_core_1_1_views_____view_imports.html", null ],
      [ "AspNetCore.Views__ViewStart", "class_asp_net_core_1_1_views_____view_start.html", null ]
    ] ],
    [ "global.MicrosoftAspNetCore.Mvc.Razor.RazorPage< IEnumerable< BiblioDatCore.Web.Models.Student >>", null, [
      [ "AspNetCore.Views_Students_StudentsList", "class_asp_net_core_1_1_views___students___students_list.html", null ]
    ] ],
    [ "SimpleIoc", null, [
      [ "BiblioDatCore.UI.MyIoc", "class_biblio_dat_core_1_1_u_i_1_1_my_ioc.html", null ],
      [ "BiblioDatCore.WpfClient.MyIoc", "class_biblio_dat_core_1_1_wpf_client_1_1_my_ioc.html", null ]
    ] ],
    [ "BiblioDatCore.Web.Startup", "class_biblio_dat_core_1_1_web_1_1_startup.html", null ],
    [ "BiblioDatCore.Data.Models.Student", "class_biblio_dat_core_1_1_data_1_1_models_1_1_student.html", null ],
    [ "BiblioDatCore.Web.Models.Student", "class_biblio_dat_core_1_1_web_1_1_models_1_1_student.html", null ],
    [ "BiblioDatCore.Web.Models.StudentListViewModel", "class_biblio_dat_core_1_1_web_1_1_models_1_1_student_list_view_model.html", null ],
    [ "ViewModelBase", null, [
      [ "BiblioDatCore.UI.VM.EditorViewModel", "class_biblio_dat_core_1_1_u_i_1_1_v_m_1_1_editor_view_model.html", null ],
      [ "BiblioDatCore.UI.VM.MainViewModel", "class_biblio_dat_core_1_1_u_i_1_1_v_m_1_1_main_view_model.html", null ],
      [ "BiblioDatCore.WpfClient.MainVM", "class_biblio_dat_core_1_1_wpf_client_1_1_main_v_m.html", null ]
    ] ],
    [ "BiblioDatCore.Logic.VoucherClass", "class_biblio_dat_core_1_1_logic_1_1_voucher_class.html", null ],
    [ "System.Windows.Window", null, [
      [ "BiblioDatCore.UI.MainWindow", "class_biblio_dat_core_1_1_u_i_1_1_main_window.html", null ],
      [ "BiblioDatCore.UI.MainWindow", "class_biblio_dat_core_1_1_u_i_1_1_main_window.html", null ],
      [ "BiblioDatCore.UI.UserInterface.EditorWindow", "class_biblio_dat_core_1_1_u_i_1_1_user_interface_1_1_editor_window.html", null ],
      [ "BiblioDatCore.UI.UserInterface.EditorWindow", "class_biblio_dat_core_1_1_u_i_1_1_user_interface_1_1_editor_window.html", null ],
      [ "BiblioDatCore.UI.UserInterface.EditorWindow", "class_biblio_dat_core_1_1_u_i_1_1_user_interface_1_1_editor_window.html", null ],
      [ "BiblioDatCore.WpfClient.Editor", "class_biblio_dat_core_1_1_wpf_client_1_1_editor.html", null ],
      [ "BiblioDatCore.WpfClient.Editor", "class_biblio_dat_core_1_1_wpf_client_1_1_editor.html", null ],
      [ "BiblioDatCore.WpfClient.Editor", "class_biblio_dat_core_1_1_wpf_client_1_1_editor.html", null ],
      [ "BiblioDatCore.WpfClient.MainWindow", "class_biblio_dat_core_1_1_wpf_client_1_1_main_window.html", null ],
      [ "BiblioDatCore.WpfClient.MainWindow", "class_biblio_dat_core_1_1_wpf_client_1_1_main_window.html", null ],
      [ "BiblioDatCore.WpfClient.MainWindow", "class_biblio_dat_core_1_1_wpf_client_1_1_main_window.html", null ]
    ] ],
    [ "Window", null, [
      [ "BiblioDatCore.UI.MainWindow", "class_biblio_dat_core_1_1_u_i_1_1_main_window.html", null ]
    ] ]
];