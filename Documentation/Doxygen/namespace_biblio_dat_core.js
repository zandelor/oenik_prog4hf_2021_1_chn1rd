var namespace_biblio_dat_core =
[
    [ "Data", "namespace_biblio_dat_core_1_1_data.html", "namespace_biblio_dat_core_1_1_data" ],
    [ "Logic", "namespace_biblio_dat_core_1_1_logic.html", "namespace_biblio_dat_core_1_1_logic" ],
    [ "Program", "namespace_biblio_dat_core_1_1_program.html", "namespace_biblio_dat_core_1_1_program" ],
    [ "Repository", "namespace_biblio_dat_core_1_1_repository.html", "namespace_biblio_dat_core_1_1_repository" ],
    [ "UI", "namespace_biblio_dat_core_1_1_u_i.html", "namespace_biblio_dat_core_1_1_u_i" ],
    [ "Web", "namespace_biblio_dat_core_1_1_web.html", "namespace_biblio_dat_core_1_1_web" ],
    [ "WpfClient", "namespace_biblio_dat_core_1_1_wpf_client.html", "namespace_biblio_dat_core_1_1_wpf_client" ]
];