var namespace_biblio_dat_core_1_1_u_i_1_1_data =
[
    [ "StudentWPF", "class_biblio_dat_core_1_1_u_i_1_1_data_1_1_student_w_p_f.html", "class_biblio_dat_core_1_1_u_i_1_1_data_1_1_student_w_p_f" ],
    [ "ClassYear", "namespace_biblio_dat_core_1_1_u_i_1_1_data.html#a85c49e1ce826c06cd5bd02159be31a83", [
      [ "None", "namespace_biblio_dat_core_1_1_u_i_1_1_data.html#a85c49e1ce826c06cd5bd02159be31a83a6adf97f83acf6453d4a6a4b1070f3754", null ],
      [ "One", "namespace_biblio_dat_core_1_1_u_i_1_1_data.html#a85c49e1ce826c06cd5bd02159be31a83a06c2cea18679d64399783748fa367bdd", null ],
      [ "Two", "namespace_biblio_dat_core_1_1_u_i_1_1_data.html#a85c49e1ce826c06cd5bd02159be31a83aaada29daee1d64ed0fe907043855cb7e", null ],
      [ "Three", "namespace_biblio_dat_core_1_1_u_i_1_1_data.html#a85c49e1ce826c06cd5bd02159be31a83aca8a2087e5557e317599344687a57391", null ],
      [ "Four", "namespace_biblio_dat_core_1_1_u_i_1_1_data.html#a85c49e1ce826c06cd5bd02159be31a83a981b8fcee42e1e726a67a2b9a98ea6e9", null ],
      [ "Five", "namespace_biblio_dat_core_1_1_u_i_1_1_data.html#a85c49e1ce826c06cd5bd02159be31a83ae5d9de39f7ca1ba2637e5640af3ae8aa", null ],
      [ "Six", "namespace_biblio_dat_core_1_1_u_i_1_1_data.html#a85c49e1ce826c06cd5bd02159be31a83ae6fbc0b9673f8c86726688d7607fc8f5", null ],
      [ "Seven", "namespace_biblio_dat_core_1_1_u_i_1_1_data.html#a85c49e1ce826c06cd5bd02159be31a83a12e67aac3e7f9227cc35f8f047d7dc74", null ],
      [ "Eight", "namespace_biblio_dat_core_1_1_u_i_1_1_data.html#a85c49e1ce826c06cd5bd02159be31a83abaca0ca6729684fd54206793ae4b5bd5", null ],
      [ "Nine", "namespace_biblio_dat_core_1_1_u_i_1_1_data.html#a85c49e1ce826c06cd5bd02159be31a83a24db11216549ee55172c33cf3def2f3f", null ],
      [ "Ten", "namespace_biblio_dat_core_1_1_u_i_1_1_data.html#a85c49e1ce826c06cd5bd02159be31a83aa185c3c138dca5ef46afc33288a67d1f", null ],
      [ "Eleven", "namespace_biblio_dat_core_1_1_u_i_1_1_data.html#a85c49e1ce826c06cd5bd02159be31a83ac709c631ff12d168727d4fd5488a04d9", null ],
      [ "Twelve", "namespace_biblio_dat_core_1_1_u_i_1_1_data.html#a85c49e1ce826c06cd5bd02159be31a83a1b187571eeebe3f241cb64bb1f943322", null ],
      [ "Thirteen", "namespace_biblio_dat_core_1_1_u_i_1_1_data.html#a85c49e1ce826c06cd5bd02159be31a83ac9c15d36d259b4aa1382b2970a590c89", null ],
      [ "Fourteen", "namespace_biblio_dat_core_1_1_u_i_1_1_data.html#a85c49e1ce826c06cd5bd02159be31a83aa59cd6b51b232dee123c6b1dd2c7e80b", null ]
    ] ]
];