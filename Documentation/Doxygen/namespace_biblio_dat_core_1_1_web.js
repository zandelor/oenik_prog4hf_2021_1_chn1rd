var namespace_biblio_dat_core_1_1_web =
[
    [ "Controllers", "namespace_biblio_dat_core_1_1_web_1_1_controllers.html", "namespace_biblio_dat_core_1_1_web_1_1_controllers" ],
    [ "Models", "namespace_biblio_dat_core_1_1_web_1_1_models.html", "namespace_biblio_dat_core_1_1_web_1_1_models" ],
    [ "Program", "class_biblio_dat_core_1_1_web_1_1_program.html", "class_biblio_dat_core_1_1_web_1_1_program" ],
    [ "Startup", "class_biblio_dat_core_1_1_web_1_1_startup.html", "class_biblio_dat_core_1_1_web_1_1_startup" ]
];