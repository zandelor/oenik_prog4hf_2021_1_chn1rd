var searchData=
[
  ['idalreadyincollectionexception_242',['IdAlreadyInCollectionException',['../class_biblio_dat_core_1_1_logic_1_1_id_already_in_collection_exception.html',1,'BiblioDatCore::Logic']]],
  ['idnotincollectionexception_243',['IdNotInCollectionException',['../class_biblio_dat_core_1_1_logic_1_1_id_not_in_collection_exception.html',1,'BiblioDatCore::Logic']]],
  ['ieditorservice_244',['IEditorService',['../interface_biblio_dat_core_1_1_u_i_1_1_b_l_1_1_i_editor_service.html',1,'BiblioDatCore::UI::BL']]],
  ['ilogic_245',['ILogic',['../interface_biblio_dat_core_1_1_logic_1_1_i_logic.html',1,'BiblioDatCore::Logic']]],
  ['imainlogic_246',['IMainLogic',['../interface_biblio_dat_core_1_1_wpf_client_1_1_i_main_logic.html',1,'BiblioDatCore::WpfClient']]],
  ['irepository_247',['IRepository',['../interface_biblio_dat_core_1_1_repository_1_1_i_repository.html',1,'BiblioDatCore::Repository']]],
  ['istudentlogic_248',['IStudentLogic',['../interface_biblio_dat_core_1_1_u_i_1_1_b_l_1_1_i_student_logic.html',1,'BiblioDatCore::UI::BL']]]
];
