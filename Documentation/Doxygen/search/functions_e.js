var searchData=
[
  ['testavgdayofborrowsbyauthors_388',['TestAvgDayOfBorrowsByAuthors',['../class_biblio_dat_core_1_1_logic_1_1_tests_1_1_logic_test.html#acc9c483add131b493a171a6288f921ee',1,'BiblioDatCore::Logic::Tests::LogicTest']]],
  ['testbooksneverborrowed_389',['TestBooksNeverBorrowed',['../class_biblio_dat_core_1_1_logic_1_1_tests_1_1_logic_test.html#a3847e8b93b02fd977f67eabd6b42b31e',1,'BiblioDatCore::Logic::Tests::LogicTest']]],
  ['testborrowsbygenres_390',['TestBorrowsByGenres',['../class_biblio_dat_core_1_1_logic_1_1_tests_1_1_logic_test.html#a19798c4000f297e825465bcd7b87419a',1,'BiblioDatCore::Logic::Tests::LogicTest']]],
  ['testchangeauthorwithnull_391',['TestChangeAuthorWithNull',['../class_biblio_dat_core_1_1_logic_1_1_tests_1_1_logic_test.html#afe512e5f6e6735469338850cc36562e9',1,'BiblioDatCore::Logic::Tests::LogicTest']]],
  ['testchangebookthrowsidalreadyincollectionexception_392',['TestChangeBookThrowsIdAlreadyInCollectionException',['../class_biblio_dat_core_1_1_logic_1_1_tests_1_1_logic_test.html#a407d79e8db49d225faa03257c3968a19',1,'BiblioDatCore::Logic::Tests::LogicTest']]],
  ['testchangecheckout_393',['TestChangeCheckout',['../class_biblio_dat_core_1_1_logic_1_1_tests_1_1_logic_test.html#acaa3032c04ae550c6d4cc3bda1ad1bc4',1,'BiblioDatCore::Logic::Tests::LogicTest']]],
  ['testchangecheckoutthrowsidnotincollectionexception_394',['TestChangeCheckoutThrowsIdNotInCollectionException',['../class_biblio_dat_core_1_1_logic_1_1_tests_1_1_logic_test.html#a09993e4ab31cfdde1a7dbf467943fcfa',1,'BiblioDatCore::Logic::Tests::LogicTest']]],
  ['testgetoneauthor_395',['TestGetOneAuthor',['../class_biblio_dat_core_1_1_logic_1_1_tests_1_1_logic_test.html#a6ad4815c7baa24f9a0bb23fc385fd93e',1,'BiblioDatCore::Logic::Tests::LogicTest']]],
  ['testgetonebookwithexception_396',['TestGetOneBookWithException',['../class_biblio_dat_core_1_1_logic_1_1_tests_1_1_logic_test.html#a8bae6078b40cf750f853758aebd41df0',1,'BiblioDatCore::Logic::Tests::LogicTest']]],
  ['testinsertauthorthrowsidalreadyincollectionexception_397',['TestInsertAuthorThrowsIdAlreadyInCollectionException',['../class_biblio_dat_core_1_1_logic_1_1_tests_1_1_logic_test.html#a021432992393ee677257f7bc02a1e850',1,'BiblioDatCore::Logic::Tests::LogicTest']]],
  ['testinsertstudent_398',['TestInsertStudent',['../class_biblio_dat_core_1_1_logic_1_1_tests_1_1_logic_test.html#a1ee7b8b6de133653c7bb0278ae7ce55d',1,'BiblioDatCore::Logic::Tests::LogicTest']]],
  ['testlistallbook_399',['TestListAllBook',['../class_biblio_dat_core_1_1_logic_1_1_tests_1_1_logic_test.html#add2ec996e3f25813f8d2e5cda7a46e11',1,'BiblioDatCore::Logic::Tests::LogicTest']]],
  ['testremoveauthor_400',['TestRemoveAuthor',['../class_biblio_dat_core_1_1_logic_1_1_tests_1_1_logic_test.html#a54f6470b89635c04c8405479291c1ff2',1,'BiblioDatCore::Logic::Tests::LogicTest']]],
  ['testremovecheckout_401',['TestRemoveCheckout',['../class_biblio_dat_core_1_1_logic_1_1_tests_1_1_logic_test.html#ac383ddd68754553693fe3144578a520b',1,'BiblioDatCore::Logic::Tests::LogicTest']]],
  ['tostring_402',['ToString',['../class_biblio_dat_core_1_1_u_i_1_1_data_1_1_student_w_p_f.html#ab5adef7e03283ad615d1053df55430e4',1,'BiblioDatCore::UI::Data::StudentWPF']]]
];
