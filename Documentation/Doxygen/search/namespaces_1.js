var searchData=
[
  ['bibliodatcore_282',['BiblioDatCore',['../namespace_biblio_dat_core.html',1,'']]],
  ['bl_283',['BL',['../namespace_biblio_dat_core_1_1_u_i_1_1_b_l.html',1,'BiblioDatCore::UI']]],
  ['controllers_284',['Controllers',['../namespace_biblio_dat_core_1_1_web_1_1_controllers.html',1,'BiblioDatCore::Web']]],
  ['data_285',['Data',['../namespace_biblio_dat_core_1_1_data.html',1,'BiblioDatCore.Data'],['../namespace_biblio_dat_core_1_1_u_i_1_1_data.html',1,'BiblioDatCore.UI.Data']]],
  ['logic_286',['Logic',['../namespace_biblio_dat_core_1_1_logic.html',1,'BiblioDatCore']]],
  ['models_287',['Models',['../namespace_biblio_dat_core_1_1_data_1_1_models.html',1,'BiblioDatCore.Data.Models'],['../namespace_biblio_dat_core_1_1_web_1_1_models.html',1,'BiblioDatCore.Web.Models']]],
  ['program_288',['Program',['../namespace_biblio_dat_core_1_1_program.html',1,'BiblioDatCore']]],
  ['repository_289',['Repository',['../namespace_biblio_dat_core_1_1_repository.html',1,'BiblioDatCore']]],
  ['tests_290',['Tests',['../namespace_biblio_dat_core_1_1_logic_1_1_tests.html',1,'BiblioDatCore::Logic']]],
  ['ui_291',['UI',['../namespace_biblio_dat_core_1_1_u_i.html',1,'BiblioDatCore']]],
  ['userinterface_292',['UserInterface',['../namespace_biblio_dat_core_1_1_u_i_1_1_user_interface.html',1,'BiblioDatCore::UI']]],
  ['vm_293',['VM',['../namespace_biblio_dat_core_1_1_u_i_1_1_v_m.html',1,'BiblioDatCore::UI']]],
  ['web_294',['Web',['../namespace_biblio_dat_core_1_1_web.html',1,'BiblioDatCore']]],
  ['wpfclient_295',['WpfClient',['../namespace_biblio_dat_core_1_1_wpf_client.html',1,'BiblioDatCore']]]
];
