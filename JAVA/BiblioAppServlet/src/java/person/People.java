package person;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class People implements Serializable {
    
    @XmlElement
    String bookTitle = "";
    @XmlElement
    int voucherValue = 0;
    @XmlElement
    List<Person> person;
    
    Random r = new Random();
    

    public People() {
        
    }
    
    public People(Boolean param) {
        person = new ArrayList<Person>();
        person.add(new Person("Barnabe Harrild", "bharrild0@chron.com", "male"));
        person.add(new Person("Nona Eric", "nericj@liveinternet.ru", "female"));
        person.add(new Person("Towny Cremin", "tcreminn@webnode.com", "male"));
        person.add(new Person("Helena Soars", "hsoarsc@lovefur.net", "female"));
        person.add(new Person("Laverna Bigby", "lbigby9@oaic.gov.au", "female"));
        person.add(new Person("Dag Marcum", "dmarcuml@shareasale.com", "female"));
        person.add(new Person("Portie Atheis", "patheism@plala.or.jp", "male"));
        person.add(new Person("Louie L'Episcopi", "llepiscopir@woothemes.fr", "male"));
        person.add(new Person("Gina Hollingdale", "ghollingdalew@ifeng.com", "female"));
        person.add(new Person("Xi Zhang", "woaibeijin@panda.cn", "male"));
    }
    

    public List<Person> getPeople() {
        return person;
    }
    
    public void setVoucherValue(int voucherValue) {
        this.voucherValue = voucherValue;
    }
    
    public void setBookTitle(String bookTitle) {
        this.bookTitle = bookTitle;
    }
            
            
    public void keepNRandomPeople (int n) {
        
        System.out.println();
        while (person.size() > n) {
            person.remove(r.nextInt(person.size()));
            Collections.shuffle(person);
        }
        
    }
}
